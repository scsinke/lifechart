﻿using LifeChartApp.Model;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LifeChartApp.Requests
{
    public class SaveLifeChartRequest
    {
        [JsonProperty(PropertyName = "lifeCharts")]
        public List<LifeChartContainer> LifeCharts { get; set; }
    }
}