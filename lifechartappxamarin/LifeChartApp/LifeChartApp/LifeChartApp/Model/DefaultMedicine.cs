﻿using SQLite;

namespace LifeChartApp.Model
{
    public class DefaultMedicine
    {
        [AutoIncrement, PrimaryKey]
        public int MedicineId { get; set; }

        public string MedicineName { get; set; }
        public int DoseInMg { get; set; }
        public int AmountPerDay { get; set; }
        public bool Toggled { get; set; }
    }
}