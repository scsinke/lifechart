﻿using LifeChartApp.Interfaces;
using LifeChartApp.Pages.LifeChartText;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace LifeChartApp.Model
{
    public class LCTextOverviewController : ILCTextOverviewController
    {
        private ContentView mainContentView;
        public ContentPage CurrentPage { get; private set; }
        private List<CompleteLifeChart> allLifeCharts = new List<CompleteLifeChart>();
        private List<CompleteLifeChart> lifeChartsToSend = new List<CompleteLifeChart>();
        private CompleteLifeChart selectedLifeChart;

        private const int YEAR_OVERVIEW_INDEX = 0;
        private const int MONTH_OVERVIEW_INDEX = 1;
        private const int DAY_OVERVIEW_INDEX = 2;
        private const int TEXT_OVERVIEW_INDEX = 3;
        private const int MULTIPLE_YEARS_START_INDEX = 0;
        private const int SINGLE_YEAR_START_INDEX = 1;

        public int PageIndex { get; private set; }
        public int Year { get; private set; }
        public int Month { get; private set; }
        public List<int> Years { get; private set; }

        public LCTextOverviewController(ContentView placeHolder, List<CompleteLifeChart> completeLifeCharts)
        {
            Years = new List<int>();
            mainContentView = placeHolder;
            allLifeCharts = completeLifeCharts;
            lifeChartsToSend = allLifeCharts;
            PageIndex = 0;
        }

        public void LoadFirstPage()
        {
            CheckFirstPage();
            LoadIndexedPage();
        }

        private void CheckFirstPage()
        {
            if (!FirstLifeChartWasInAPreviousYear())
            {
                PageIndex = SINGLE_YEAR_START_INDEX;
                SetYears(true);
            }
            else
            {
                PageIndex = MULTIPLE_YEARS_START_INDEX;
                SetYears(false);
            }
        }

        private void SetYears(bool singleYear)
        {
            if (allLifeCharts.Count == 0)
                return;

            if (singleYear)
                Years.Add(allLifeCharts[0].LifeChart.Date.Year);
            else
            {
                foreach (CompleteLifeChart clc in allLifeCharts)
                {
                    if (!Years.Any(year => year == clc.LifeChart.Date.Year))
                        Years.Add(clc.LifeChart.Date.Year);
                }
            }
        }

        private bool FirstLifeChartWasInAPreviousYear()
        {
            if (allLifeCharts.Count > 0)
            {
                if (!GetFirstLifeChartDate().Year.Equals(DateTime.Now.Year))
                    return true;
            }
            Year = DateTime.Now.Year;
            return false;
        }

        private DateTime GetFirstLifeChartDate()
        {
            DateTime firstDate = DateTime.Now;

            return allLifeCharts.OrderBy(life => life.LifeChart.Date).FirstOrDefault().LifeChart.Date;
        }

        private void UpdateDates(int item)
        {
            switch (PageIndex)
            {
                case YEAR_OVERVIEW_INDEX:
                    Year = item;
                    lifeChartsToSend = allLifeCharts.Where(lc => lc.LifeChart.Date.Year == Year).ToList();
                    break;

                case MONTH_OVERVIEW_INDEX:
                    Month = item;
                    lifeChartsToSend = allLifeCharts.Where(lc => lc.LifeChart.Date.Year == Year && lc.LifeChart.Date.Month == Month).ToList();
                    break;

                case DAY_OVERVIEW_INDEX:
                    SetLifeChart(item);
                    break;
            }
        }

        private void SetLifeChart(int item)
        {
            string dateTimeString = item.ToString() + "/" + Month.ToString() + "/" + Year.ToString();
            DateTime date = Convert.ToDateTime(dateTimeString);
            selectedLifeChart = lifeChartsToSend.Where(lc => lc.LifeChart.Date.ToShortDateString().Equals(date.ToShortDateString())).First();
        }

        public void LoadNextPage(int item)
        {
            UpdateDates(item);
            PageIndex++;
            LoadIndexedPage();
        }

        public void LoadPreviousPage()
        {
            PageIndex--;
            LoadIndexedPage();
        }

        public void LoadDayOverViewPage(List<CompleteLifeChart> completeLifeCharts)
        {
            lifeChartsToSend = completeLifeCharts;

            PageIndex = DAY_OVERVIEW_INDEX;
            LoadIndexedPage();
        }

        private void LoadIndexedPage()
        {
            CheckPage();
            LoadPage();
        }

        private void CheckPage()
        {
            switch (PageIndex)
            {
                case YEAR_OVERVIEW_INDEX:
                    CurrentPage = new LCYearOverview(this, Years);
                    break;

                case MONTH_OVERVIEW_INDEX:
                    CurrentPage = new LCMonthOverview(this, allLifeCharts, Year);
                    break;

                case DAY_OVERVIEW_INDEX:
                    CurrentPage = new LCDayOverview(this, lifeChartsToSend);
                    break;

                case TEXT_OVERVIEW_INDEX:
                    CurrentPage = new LCTextOverview(this, selectedLifeChart);
                    break;
            }
        }

        private void LoadPage() => mainContentView.Content = CurrentPage.Content;
    }
}
