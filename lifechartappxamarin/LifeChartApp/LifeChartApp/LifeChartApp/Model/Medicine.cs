﻿using Newtonsoft.Json;
using SQLite;

namespace LifeChartApp.Model
{
    public class Medicine
    {
        [AutoIncrement, PrimaryKey]
        public int MedicineId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string MedicineName { get; set; }

        [JsonProperty(PropertyName = "doseInMg")]
        public int DoseInMg { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public int AmountPerDay { get; set; }

        //Foreign key
        public int LifeChartId { get; set; }
    }
}