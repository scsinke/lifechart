﻿using SQLite;

namespace LifeChartApp.Model
{
    public class Person
    {
        [AutoIncrement, PrimaryKey]
        public int PersonId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Sex { get; set; }
        public float Weight { get; set; }
    }
}