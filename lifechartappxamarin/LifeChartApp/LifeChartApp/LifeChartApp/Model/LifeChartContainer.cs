﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LifeChartApp.Model
{
    public class LifeChartContainer
    {
        [JsonProperty(PropertyName = "lifeChartId")]
        public int LifeChartId { get; set; }

        [JsonProperty(PropertyName = "weight")]
        public float Weight { get; set; }

        [JsonProperty(PropertyName = "sleep")]
        public float Sleep { get; set; }

        [JsonProperty(PropertyName = "mood")]
        public string Mood { get; set; }

        [JsonProperty(PropertyName = "unrestrainedMania")]
        public bool UnrestrainedMania { get; set; }

        [JsonProperty(PropertyName = "severityOfEpisode")]
        public string SeverityOfEpisode { get; set; }

        [JsonProperty(PropertyName = "complaints")]
        public string Complaints { get; set; }

        [JsonProperty(PropertyName = "moodRating")]
        public int MoodRating { get; set; }

        [JsonProperty(PropertyName = "moodSwings")]
        public int MoodSwings { get; set; }

        [JsonProperty(PropertyName = "menstruated")]
        public bool Menstruated { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "medicines")]
        public List<Medicine> Medicines { get; set; }

        [JsonProperty(PropertyName = "lifeEvents")]
        public List<LifeEvent> LifeEvents { get; set; }
    }
}