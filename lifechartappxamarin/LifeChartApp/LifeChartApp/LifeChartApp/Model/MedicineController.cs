﻿using LifeChartApp.Dao;
using LifeChartApp.Interfaces;
using System.Collections.Generic;

namespace LifeChartApp.Model
{
    public class MedicineController
    {

        private static MedicineController instance;
        private readonly IMedicineDao db = MedicineDao.Instance;

        private List<Medicine> currentMedicineList;
        private List<DefaultMedicine> currentDefaultMedicineList;

        private MedicineController(){
            if (currentMedicineList == null)
                currentMedicineList = new List<Medicine>();
            if (currentDefaultMedicineList == null)
                currentDefaultMedicineList = new List<DefaultMedicine>();
        }

        public static MedicineController GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicineController();
            }
            return instance;
        }

        public List<DefaultMedicine> GetAllDefaultMeds()
        {
            return db.GetAllDefaultMedicines() ?? new List<DefaultMedicine>();
        }

        public List<DefaultMedicine> GetCurrentDefaultMeds()
        {
            return currentDefaultMedicineList;
        }

        public void SetCurrentMedicineList(List<Medicine> list)
        {
            currentMedicineList = list;
        }

        public List<Medicine> GetCurrentMedicineList()
        {
            return currentMedicineList ?? new List<Medicine>();
        }

        public void SetCurrentDefaultMedicineList(List<DefaultMedicine> medList)
        {
            currentDefaultMedicineList = medList;
        }

        public List<DefaultMedicine> GetCurrentDefaultMedicineList()
        {
            return currentDefaultMedicineList ?? new List<DefaultMedicine>();
        }

        public List<Medicine> GetAllTakenMeds()
        {
            List<Medicine> combinedList = new List<Medicine>();

            List<Medicine> defList = ConvertTakenDefaultMeds();
            combinedList.AddRange(defList);
            combinedList.AddRange(currentMedicineList);

            return combinedList;
        }

        public void UpdateDefaultMedicineListToDb()
        {
            foreach (DefaultMedicine defMed in currentDefaultMedicineList)
            {
                db.UpdateDefaultMedicine(defMed);
            }
        }

        private List<Medicine> ConvertTakenDefaultMeds()
        {
            List<Medicine> medList = new List<Medicine>();

            foreach(DefaultMedicine defMed in currentDefaultMedicineList)
            {
                if (defMed.Toggled)
                {
                    Medicine medicine = new Medicine()
                    {
                        AmountPerDay = defMed.AmountPerDay,
                        DoseInMg = defMed.DoseInMg,
                        MedicineName = defMed.MedicineName
                    };
                    medList.Add(medicine);
                }
            }

            return medList;
        }
    }
}