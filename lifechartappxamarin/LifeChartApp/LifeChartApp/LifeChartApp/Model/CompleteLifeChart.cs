﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeChartApp.Model
{
    public class CompleteLifeChart
    {
        public LifeChart LifeChart { get; set; }
        public List<Medicine> Medicines { get; set; }
        public List<LifeEvent> LifeEvents { get; set; }
    }
}