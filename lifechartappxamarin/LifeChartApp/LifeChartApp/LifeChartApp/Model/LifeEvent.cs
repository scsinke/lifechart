﻿using Newtonsoft.Json;
using SQLite;

namespace LifeChartApp.Model
{
    public class LifeEvent
    {
        [AutoIncrement, PrimaryKey]
        public int EventId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Event { get; set; }

        [JsonProperty(PropertyName = "moodInfluence")]
        public int MoodInfluence { get; set; }

        //Foreign key
        public int LifeChartId { get; set; }
    }
}