﻿using LifeChartApp.Dao;
using System;
using System.Collections.Generic;

namespace LifeChartApp.Model
{
   public class LifeChartController
    {
        private static LifeChartController instance;

        private readonly LifeChart lifeChart;
        private Person person;
        private readonly IMedicineDao medicineDao;

        private LifeChartController() {
            lifeChart = new LifeChart();
            medicineDao = MedicineDao.Instance;
        }

        public static LifeChartController GetInstance()
        {
            if (instance == null)
                instance = new LifeChartController();

            return instance;
        }

        public void SetPerson(Person person) => this.person = person;

        public LifeChart GetLifeChart() => lifeChart;

        public void SetWeight(float weight) => lifeChart.Weight = weight;

        public void SetSleep(float sleep) => lifeChart.Sleep = sleep;

        public void SetMood(string mood) => lifeChart.Mood = mood;

        public void SetUnrestrainedMania(bool unrestrainedMania) => lifeChart.UnrestrainedMania = unrestrainedMania;

        public void SetSeverityOfEpisode(string severityOfEpisode) => lifeChart.SeverityOfEpisode = severityOfEpisode;

        public void SetComplaints(string complaints) => lifeChart.Complaints = complaints;

        public void SetMoodRating(int moodRating) => lifeChart.MoodRating = moodRating;

        public void SetMoodSwings(int moodSwings) => lifeChart.MoodSwings = moodSwings;

        public void SetMenstruated(bool menstruated) => lifeChart.Menstruated = menstruated;

        public void SetDate(DateTime date)=> lifeChart.Date = date;

        public float GetWeight() => lifeChart.Weight;

        public float GetSleep() => lifeChart.Sleep;

        public bool GetUnrestrainedMania() => lifeChart.UnrestrainedMania;

        public Person GetPerson() => person;
 
        public string GetMood() => lifeChart.Mood;

        public string GetComplaints() => lifeChart.Complaints;

        public string GetSeverityOfEpisode() => lifeChart.SeverityOfEpisode;

        public int GetMoodSwings() => lifeChart.MoodSwings;

        public int GetMoodRating() => lifeChart.MoodRating;

        public bool GetMenstruation() => lifeChart.Menstruated;

        public List<DefaultMedicine> GetDefaultMedicines() => medicineDao.GetAllDefaultMedicines();
    }
}