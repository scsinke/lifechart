﻿using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Interfaces;
using LifeChartApp.Pages;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LifeChartApp.Model
{
    public class LCPageController : ILCPageController
    {
        private readonly ILifeChartDao lifeChartDao;
        private MainPage mainPage;

        public IPersonDao PersonDao { private get; set; }
        public int CurrentPageIndex { get; private set; }
        public ContentView MainContentView { get; private set; }
        public List<ContentPage> Pages { get; private set; }
         
        public LCPageController(MainPage mainPage, ContentView placeHolder, ILifeChartDao lifeChartDao)
        {
            MainContentView = placeHolder;
            this.mainPage = mainPage;
            this.lifeChartDao = lifeChartDao;
            PersonDao = Dao.PersonDao.Instance;

            InitPages();
        }

        private void InitPages()
        {
            CurrentPageIndex = -1;
            Pages = new List<ContentPage>
            {
                new MedicinePage(this),
                new WeightPage(this),
                new SleepPage(this),
                new MoodPage(this),
                new EpisodePage(this),
                new ComplaintsPage(this),
                new Mood2Page(this),
                new LifeEventPage(this),
                new MenstruationPage(this)
            };
        }
        
        public void NextPage()
        {
            if (Pages.Count > CurrentPageIndex)
                CurrentPageIndex++;
            LoadIndexedPage();
        }

        public void PreviousPage()
        {
            if (CurrentPageIndex >= 0)
                CurrentPageIndex--;
            LoadIndexedPage();
        }

        public void EndPage()
        {
            LoadPage(new LCSavePage(lifeChartDao, this));
        }

        public void LeaveAddItemPage()
        {
            LoadIndexedPage();
        }

        public void LoadDefaultMedicinePage(DefaultMedicine med)
        {
            LoadPage(new DefaultMedicinePageDetail(this, med));
        }

        public void LoadAddMedicinePage(Medicine med)
        {
            LoadPage(new MedicinePageDetail(this, med));
        }

        public void LoadAddMedicineToListPage()
        {
            LoadPage(new AddMedicineToListPage(this));
        }

        public void LoadAddLifeEventPage()
        {
            LoadPage(new AddLifeEventPage(this));
        }

        public void LoadPageWithIndex(int index)
        {
            CurrentPageIndex = index;
            LoadIndexedPage();
        }

        public void LoadLifeEventDetails(LifeEvent lifeEvent)
        {
            LoadPage(new LifeEventPageDetail(this, lifeEvent));
        }

        private void LoadIndexedPage()
        {
            IStrings stringsNL = StringsNL.Instance;
            string sex = PersonDao.GetPerson(1).Sex;
            string male = SexType.MALE.ToString();
            int genderCorrectionValue = 0;

            if (sex == male)
                genderCorrectionValue = -1;

            mainPage.SetTitleInsideLifeChart(CurrentPageIndex + 1, Pages.Count + genderCorrectionValue);
            LoadPage(Pages[CurrentPageIndex]);
        }

        private void LoadPage(ContentPage page)
        {
            MainContentView.Content = page.Content;
        }
    }
}