﻿using SQLite;
using System;

namespace LifeChartApp.Model
{
    public class LifeChart
    {
        [AutoIncrement, PrimaryKey]
        public int LifeChartId { get; set; }

        public float Weight { get; set; }
        public float Sleep { get; set; }
        public string Mood { get; set; }
        public bool UnrestrainedMania { get; set; }
        public string SeverityOfEpisode { get; set; }
        public string Complaints { get; set; }
        public int MoodRating { get; set; }
        public int MoodSwings { get; set; }
        public bool Menstruated { get; set; }
        public DateTime Date { get; set; }

        public bool Uploaded { get; set; }

        public int PersonId { get; set; }
    }
}