﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeChartApp.Model
{
    public class LifeEventController
    {
        private static LifeEventController instance;
        private List<LifeEvent> lifeEvents;

        private LifeEventController()
        {
            if (lifeEvents == null)
                lifeEvents = new List<LifeEvent>();
        }

        public static LifeEventController GetInstance()
        {
            if (instance == null)
                instance = new LifeEventController();

            return instance;
        }

        public void SetLifeEvents(List<LifeEvent> lifeEvents)
        {
            this.lifeEvents = lifeEvents;
        }

        public List<LifeEvent> GetLifeEvents()
        {
            return lifeEvents;
        }

        public void AddLifeEvent(LifeEvent lifeEvent)
        {
            lifeEvents.Add(lifeEvent);
        }

        public void DeleteLifeEvent(LifeEvent lifeEvent)
        {
            lifeEvents.Remove(lifeEvent);
        }
    }
}
