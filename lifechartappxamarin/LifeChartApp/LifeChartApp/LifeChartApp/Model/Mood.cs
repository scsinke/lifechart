﻿using System;
namespace LifeChartApp
{
    public class Mood
    {
        public String Title { get; set; }
        public String Value { get; set; }
        public String Description { get; set;}
        public Xamarin.Forms.Color Color { get; set; }
    }
}
