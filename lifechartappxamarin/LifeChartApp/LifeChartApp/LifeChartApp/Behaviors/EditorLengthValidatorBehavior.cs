﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace LifeChartApp.Behaviors
{
    /// <summary>
    /// Behavior that restricts the length of an entry
    /// </summary>
    public class EditorLengthValidatorBehavior : Behavior<Editor>
    {
        public int MaxLength { get; set; }

        protected override void OnAttachedTo(Editor bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += OnEditorTextChanged;
        }

        protected override void OnDetachingFrom(Editor bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged += OnEditorTextChanged;
        }

        private void OnEditorTextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = (Editor)sender;

            if (entry.Text.Length <= this.MaxLength)
                return;
            entry.TextChanged -= OnEditorTextChanged;
            entry.Text = e.OldTextValue;
            entry.TextChanged += OnEditorTextChanged;
        }
    }
}
