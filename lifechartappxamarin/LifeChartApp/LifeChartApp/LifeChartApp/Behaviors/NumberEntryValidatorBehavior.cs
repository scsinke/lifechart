﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace LifeChartApp.Behaviors
{
    public class NumberEntryValidatorBehavior : Behavior<Entry>
    {

        public bool IsFloat { get; set; }

        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += HandleTextChanged;
        }
  
        void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            string currentValue = e.NewTextValue;

            if (!string.IsNullOrEmpty(currentValue))
            {
                if (!IsFloat)
                    currentValue = currentValue.Replace(".", string.Empty);
                currentValue = currentValue.Replace("-", string.Empty);
                currentValue = currentValue.Replace(",", string.Empty);
                currentValue = currentValue.Replace("+", string.Empty);
            }

            ((Entry)sender).Text = currentValue;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
        }
    }
}