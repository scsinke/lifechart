﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace LifeChartApp.Behaviors
{
    /// <summary>
    /// Behavior that restricts the length of an entry
    /// </summary>
    public class EntryValueValidatorBehavior : Behavior<Entry>
    {
        public int MaxValue { get; set; }

        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += OnEntryTextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged += OnEntryTextChanged;
        }

        private void OnEntryTextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;

            if (MaxValue > 0)
            {
                int.TryParse(entry.Text, out int value);
                if (value <= MaxValue)
                    return;
                entry.TextChanged -= OnEntryTextChanged;
                entry.Text = e.OldTextValue;
                entry.TextChanged += OnEntryTextChanged;
            }
        }
    }
}
