﻿using LifeChartApp.Dao;
using LifeChartApp.Pages;
using LifeChartApp.Pages.Login;
using System;
using System.Linq;
using Xamarin.Auth;
using Xamarin.Forms;

namespace LifeChartApp
{
	public partial class App : Application
	{
        internal static readonly string AppName = "LifeChartApp";

        public App ()
		{
			InitializeComponent();

            OpenFirstDefaultPage();
        }

        private void OpenFirstDefaultPage()
        {
            var account = AccountStore.Create().FindAccountsForService(AppName).FirstOrDefault();

            if (!UserHasRegistered())
            {
                MainPage = new NavigationPage(new RegistrationPage());
                return;
            }

            if (account != null)
            {
                MainPage = new NavigationPage(new LoginPage());
            }
            else
            {
                MainPage = new NavigationPage(new FirstLoginPage());
            }
        }

        private Boolean UserHasRegistered()
        {
            IPersonDao personDao = PersonDao.Instance;
            if (personDao.GetPerson(1) == null)
                return false;
            return true;
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
