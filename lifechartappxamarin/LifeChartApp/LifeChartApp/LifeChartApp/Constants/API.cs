﻿using System;

namespace LifeChartApp.Constants
{
    public class Api
    {
        public const string URL_API = "http://192.168.1.40:8080/";
        
        public readonly Uri URI_SAVE_LIFECHART;

        public Api()
        {
            URI_SAVE_LIFECHART = new Uri(URL_API + "lifechart");
        }
    }
}
