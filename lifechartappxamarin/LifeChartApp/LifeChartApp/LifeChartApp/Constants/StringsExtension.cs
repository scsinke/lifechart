﻿using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Constants
{
    [ContentProperty("Text")]
    public class StringsExtension : IMarkupExtension
    {
        public string Text { get; set; }

        public string StringFormat { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return "";

            try
            {
                IStrings strings = StringsNL.Instance;
                var translation = strings.GetType().GetRuntimeProperty(Text).GetValue(strings, null);

                if (translation != null)
                {
                    if (StringFormat != null)
                        return string.Format(StringFormat, translation);
                    return translation;
                }
                return Text;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
