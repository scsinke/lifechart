﻿namespace LifeChartApp.Constants
{
    public interface IStrings
    {
        /** Page Titles **/
        string AppTitle { get; }
        string AddLifeEvents { get; }
        string Event { get; }
        string AddMedics { get; }
        string OtherComplaints { get; }
        string WelcomeToLifeChartApp { get; }
        string LifeEvents { get; }
        string Graphs { get; }
        string Overview { get; }
        string Menstruation { get; }
        string Mood2 { get; }
        string Sleep { get; }
        string Weight { get; }
        string EditLife { get; }
        string Login { get; }
        string Register { get; }

        /** Labels **/
        string WriteDownInfluenceMood { get; }
        string NameMedicine { get; }
        string MedicineList { get; }
        string LifeEventList { get; }
        string Unit { get; }
        string Dose { get; }
        string IntakePerDay { get; }
        string WriteDownComplaints { get; }
        string SeverityEpisode { get; }
        string DetermineInfluenceOnEpisode { get; }
        string WriteDownLifeEventInfluence { get; }
        string Medics { get; }
        string Extra { get; }
        string Default { get; }
        string HaveYouMenstruated { get; }
        string YourMood { get; }
        string Mania { get; }
        string UnpleasantManiaDescription { get; }
        string Mood { get; }
        string MoodDescription { get; }
        string ExtremelyDepressive { get; }
        string ExtremelyActive { get; }
        string MoodChangesDescription { get; }
        string AmountOfMoodChanges { get; }
        string HoursSleepDescription { get; }
        string NapsDontCount { get; }
        string SleepInHours { get; }
        string WhatsYourWeight { get; }
        string FillingInWeightNotRequired { get; }
        string WeightInKg { get; }
        string ThanksForFillingInLC { get; }
        string PressSaveToSaveLC { get; }
        string FirstName { get; }
        string LastName { get; }
        string Sex { get; }
        string EditLifeEvent { get; }
        string Male { get; }
        string Female { get; }

        string Neutral { get; }
        string Depressive { get; }
        string LightMania { get; }

        string Positive { get; }
        string Negative { get; }

        string ConfigureYourPincode { get; }
        string PincodeOnlyFiveNumbers { get; }
        string PincodeNotMatching { get; }
        string Pincode { get; }
        string PincodeAgain { get; }

        string SleepMoodGraphTitle { get; }
        string SleepMoodGraphXAxis { get; }
        string SleepMoodGraphYAxis { get; }

        string LifeChartDetails { get; }

        /** Units **/
        string Hours { get; }
        string Kilogram { get; }
        string Miligram { get; }

        /** Descriptions **/
        string RegistrationDescription { get; }

        /** Alert messages **/
        string LifeChartAlreadyFilledIn { get; }
        string LifeChartFilledInToday { get; }
        string WatchOut { get; }
        string EmptyManiaMessage { get; }
        string LifeChartNotCompleted { get; }
        string DoseOrIntakeCannotBeZero { get; }
        string DuplicateMedFound { get; }
        string DeleteMedMessage { get; }
        string FailedToAdd { get; }
        string DoubleLifeEvent { get; }
        string WantToDeleteLifeEvent { get; }
        string UploadSuccess { get; }
        string LifeChartUploadedMessage { get; }
        string UploadFailed { get; }
        string LifeChartNotUploadMessage { get; }
        string LifeChartSaveSuccess { get; }
        string LifeChartSaved { get; }
        string NoLifeChartsToUpload { get; }
        string NoLifeChartsNeedToUpload { get; }
        string NotEnoughLifeCharts { get; }

        /** Buttons **/
        string Yes { get; }
        string No { get; }
        string Cancel { get; }
        string Save { get; }
        string Next { get; }
        string Back { get; }
        string Ok { get; }
        string Edit { get; }
        string Delete { get; }
        string PersonalInformation { get; }
        string ScanQRCode { get; }

        string Home { get; }
        string LifeChart { get; }
        string Profile { get; }

        /** Mood **/
        string MoodSerious { get; }
        string MoodSeriousDescription { get; }

        string MoodModeratelyHigh { get; }
        string MoodModeratelyHighDescription { get; }

        string MoodModeratelyLow { get; }
        string MoodModeratelyLowDescription { get; }

        string MoodLight { get; }
        string MoodLightDescription { get; }

        string MoodStable { get; }
        string MoodStableDescription { get; }

        string MoodLightDepressive { get; }
        string MoodLightDepressiveDescription { get; }

        string MoodModeratelyLowDepressive { get; }
        string MoodModeratelyLowDepressiveDescription { get; }

        string MoodModeratelyHighDepressive { get; }
        string MoodModeratelyHighDepressiveDescription { get; }

        string MoodSeriouslyDepressive { get; }
        string MoodSeriouslyDepressiveDescription { get; }

        string MoodDepressive { get; }
        string MoodDepressiveDescription { get; }
    }
}