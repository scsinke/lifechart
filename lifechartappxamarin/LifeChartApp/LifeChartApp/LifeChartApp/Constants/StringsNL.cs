﻿using System.Globalization;

namespace LifeChartApp.Constants
{
    public class StringsNL : IStrings
    {
        public static readonly StringsNL Instance = new StringsNL();

        private StringsNL()
        {
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("nl-NL");
        }

        /** Page Titles **/
        public string AppTitle => "Life Chart";
        public string AddLifeEvents => "Levensgebeurtenis toevoegen";
        public string Event => "Gebeurtenis";
        public string AddMedics => "Medicatie toevoegen";
        public string OtherComplaints => "Andere psychische klachten of verschijnselen";
        public string WelcomeToLifeChartApp => "Welkom bij de Life Chart app!";
        public string LifeEvents => "Levensgebeurtenissen";
        public string Graphs => "Grafieken";
        public string Overview => "Overzicht";
        public string Menstruation => "Menstruatie";
        public string Mood2 => "Stemming #2";
        public string Sleep => "Slaap";
        public string Weight => "Gewicht";
        public string EditMedic => "Medicatie wijzigen";
        public string EditLife => "Levensgebeurtenis wijzigen";
        public string Login => "Inloggen";
        public string Register => "Registreren";

        /** Labels **/
        public string WriteDownInfluenceMood => "Wat was de invloed hiervan op uw stemming?";
        public string NameMedicine => "Naam medicijn";
        public string MedicineList => "Lijst van medicijnen";
        public string LifeEventList => "Lijst van levensgebeurtenissen";
        public string Unit => "Eenheid";
        public string Dose => "Dosering (in mg):";
        public string IntakePerDay => "Inname per dag:";
        public string WriteDownComplaints => "Heeft u naast uw klachten van manie of depressie andere psychiatrische klachten? Geef hieronder een omschrijving.";
        public string SeverityEpisode => "Ernst van de episode";
        public string DetermineInfluenceOnEpisode => "Welke invloed heeft uw stemming gehad op uw sociaal of beroepsmatig functioneren en op uw omgang met anderen, thuis op uw werk of op school?";
        public string WriteDownLifeEventInfluence => "Welke belangrijke gebeurtenissen hebben invloed gehad op uw stemming?";
        public string WhatHappend => "Wat is er gebeurd?";
        public string Medics => "Medicatie";
        public string Extra => "Extra";
        public string Default => "Standaard";
        public string HaveYouMenstruated => "Heeft u vandaag gemenstrueerd?";
        public string YourMood => "Uw stemming";
        public string Mania => "Manie";
        public string UnpleasantManiaDescription => "Als uw stemming ondanks de manie onplezierig is, vink dan het vakje aan:";
        public string Mood => "Stemming";
        public string MoodDescription => "Hoe was uw stemming vandaag?";
        public string ExtremelyDepressive => "Uiterst depressief";
        public string ExtremelyActive => "Uiterst actief";
        public string MoodChangesDescription => "Hoeveel plotselinge, duidelijk grote stemmingswisselingen zijn vandaag voorgekomen?";
        public string AmountOfMoodChanges => "Aantal stemmingsveranderingen";
        public string HoursSleepDescription => "Hoeveel uren heeft u geslapen?";
        public string NapsDontCount => "Dutjes tellen niet mee";
        public string SleepInHours => "Slaap in uren";
        public string WhatsYourWeight => "Wat is uw gewicht?";
        public string FillingInWeightNotRequired => "U hoeft niet elke dag uw gewicht in te voeren.";
        public string WeightInKg => "Gewicht in KG";
        public string ThanksForFillingInLC => "U bent bijna klaar";
        public string PressSaveToSaveLC => "Druk op de opslaan knop om uw LifeChart gegevens op te slaan. Daarna is uw LifeChart compleet voor vandaag.";
        public string DefaultMeds => "Standaard medicatie";
        public string FirstName => "Voornaam";
        public string LastName => "Achternaam";
        public string Sex => "Geslacht";
        public string EditLifeEvent => "Levensgebeurtenis wijzigen";
        public string Male => "Man";
        public string Female => "Vrouw";

        public string Neutral => "Neutraal";
        public string Depressive => "Depresief";
        public string LightMania => "Lichte manie";

        public string Positive => "Positief";
        public string Negative => "Negatief";

        public string ConfigureYourPincode => "Stel hier uw pincode in";
        public string PincodeOnlyFiveNumbers => "U moet een 5-cijferige pincode invoeren";
        public string PincodeNotMatching => "Pincode komt niet overeen";
        public string Pincode => "Pincode";
        public string PincodeAgain => "Pincode nogmaals";

        public string SleepMoodGraphTitle => "Invloed van uren slaap op stemming";
        public string SleepMoodGraphXAxis => "Datum";
        public string SleepMoodGraphYAxis => "Stemming/Slaap";

        public string LifeChartDetails => "LifeChart Details";

        /** Units **/
        public string Hours => "uren";
        public string Kilogram => "kg";
        public string Miligram => "mg";

        /** Descriptions **/
        public string RegistrationDescription => "Voor een betere werking van onze applicatie vragen wij u om de volgende gegevens in te vullen. Het geslacht moet ingevuld worden voor het gebruik van de app. De overige gegevens dienen voor personalisatie of betere werking van de applicatie. Deze gegevens kunnen later nog veranderd worden.";

        //** Alert messages **/
        public string LifeChartAlreadyFilledIn => "De LifeChart is al compleet";
        public string LifeChartFilledInToday => "U kunt uw gegevens één keer per dag invullen. De gegevens van vandaag kunt u wel aanpassen via uw overzicht.";
        public string FailedToAdd => "Toevoegen misluk!";
        public string DoubleLifeEvent => "Dubbele levensgebeurtenis gevonden!";
        public string WatchOut => "Let op!";
        public string WantToDeleteLifeEvent => "Wilt u deze levensgebeurtenis verwijderen?";
        public string EmptyManiaMessage => "U bent vergeten uw stemming in te vullen";
        public string LifeChartNotCompleted => "UW LifeChart is niet compleet ingevuld";
        public string DeleteMedMessage => "Wilt u dit medicijn verwijderen?";
        public string DoseOrIntakeCannotBeZero => "Dosering of inname mag niet 0 zijn.";
        public string DuplicateMedFound => "Dubbele medicijn gevonden!";
        public string UploadSuccess => "Upload succesvol";
        public string LifeChartUploadedMessage => "Het uploaden van de LifeCharts is voltooid.";
        public string UploadFailed => "Upload mislukt";
        public string LifeChartNotUploadMessage => "Het uploaden van de LifeCharts is mislukt.";
        public string LifeChartSaveSuccess => "LifeChart opgeslagen!";
        public string LifeChartSaved => "De LifeChart is succesvol opgeslagen en toegevoegd aan uw overzicht.";
        public string NoLifeChartsToUpload => "Geen LifeCharts gevonden";
        public string NoLifeChartsNeedToUpload => "Er zijn geen LifeCharts meer die moeten worden geüpload.";
        public string NotEnoughLifeCharts => "Er zijn niet genoeg LifeCharts ingevuld om de grafiek weer te geven.";

        /** Buttons **/
        public string Yes => "Ja";
        public string No => "Nee";
        public string Cancel => "Annuleren";
        public string Save => "Opslaan";
        public string Confirm => "Bevestigen";
        public string Next => "Volgende";
        public string Back => "Terug";
        public string Ok => "Oké";
        public string Edit => "Wijzigen";
        public string Delete => "Verwijderen";
        public string PersonalInformation => "Persoonsgegevens";
        public string ScanQRCode => "Scan QR-code";

        public string Home => "Home";
        public string LifeChart => "LifeChart";
        public string Profile => "Profiel";

        /** Mood **/
        public string MoodSerious => "Ernstig";
        public string MoodSeriousDescription => "Ernstige beperkingen of opgenomen";

        public string MoodModeratelyHigh => "Matig hoog";
        public string MoodModeratelyHighDescription => "GROTE moeite met doelgerichte activiteiten";

        public string MoodModeratelyLow => "Matig laag";
        public string MoodModeratelyLowDescription => "ENIGE moeite met doelgerichte activiteiten";

        public string MoodLight => "Licht";
        public string MoodLightDescription => "Energiek en productief met WEINIG of geen beperkingen";

        public string MoodStable => "Stabiel";
        public string MoodStableDescription => "";

        public string MoodLightDepressive => "Licht";
        public string MoodLightDepressiveDescription => "WEINIG of geen beperkingen";

        public string MoodModeratelyLowDepressive => "Matig laag";
        public string MoodModeratelyLowDepressiveDescription => "Functioneren met ENIGE moeite";

        public string MoodModeratelyHighDepressive => "Matig hoog";
        public string MoodModeratelyHighDepressiveDescription => "Functioneren met GROTE moeite";

        public string MoodSeriouslyDepressive => "Ernstig";
        public string MoodSeriouslyDepressiveDescription => "Ernstige beperkingen of opgenomen";

        public string MoodDepressive => "Depressie";
        public string MoodDepressiveDescription => "";

        /** LifeChart Data **/
        public string DateText => "Datum: ";
        public string UploadedText => "Geüpload: ";
        public string MenstruatedText => "Gemenstrueerd: ";
        public string MoodSwingsText => "Stemmingswisselingen: ";
        public string MoodRatingText => "Stemmings score: ";
        public string ComplaintsText => "Klachten: ";
        public string SeverityOfEpisodeText => "Ernst van episode: ";
        public string UnrestrainedManiaText => "Ontstemde manie: ";
        public string MoodText => "Stemming: ";
        public string SleepText => "Uren slaap: ";
        public string WeightText => "Gewicht: ";
        public string DateListText => "Datum";
    }
}