﻿            using System;

namespace LifeChartApp.Constants
{
    public enum SexType
    {
        FEMALE = 0,
        MALE = 1
    }

    public static class SexTypeHelper
    {
        public static string GetDescription(SexType e)
        {
            IStrings stringsNL = StringsNL.Instance;

            switch (e)
            {
                case SexType.MALE:
                    return stringsNL.Male;

                case SexType.FEMALE:
                    return stringsNL.Female;

                default:
                    throw new ArgumentException($"Unknown SexType '{e}'");
            }
        }
    }
}