﻿namespace LifeChartApp.Utils
{
    enum UploadResponseType
    {
        SUCCESS,
        FAILED,
        NO_CONTENT
    }
}