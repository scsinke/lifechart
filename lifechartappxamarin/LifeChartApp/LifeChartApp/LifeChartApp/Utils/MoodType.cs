﻿using LifeChartApp.Constants;
using System;

namespace LifeChartApp.Utils
{
    public enum MoodType
    {
        NONE = -1,
        SERIOUS = 9,
        MODERATELY_HIGH = 8,
        MODERATELY_LOW = 7,
        LIGHT = 6,
        STABLE = 5,
        LIGHT_DEPRESSIVE = 4,
        MODERATELY_LOW_DEPRESSIVE = 3,
        MODERATELTY_HIGH_DEPRESSIVE = 2,
        SERIOUSLY_DEPRESSIVE = 1,
        DEPRESSIVE = 0
    }

    public static class MoodTypeHelper
    {
        public static string GetDescription(MoodType e)
        {
            IStrings stringsNL = StringsNL.Instance;

            switch (e)
            {
                case MoodType.NONE:
                    return null;
                case MoodType.SERIOUS:
                    return stringsNL.MoodSerious;
                case MoodType.MODERATELY_HIGH:
                    return stringsNL.MoodModeratelyHigh;
                case MoodType.MODERATELY_LOW:
                    return stringsNL.MoodModeratelyLow;
                case MoodType.LIGHT:
                    return stringsNL.MoodLight;
                case MoodType.STABLE:
                    return stringsNL.MoodStable;
                case MoodType.LIGHT_DEPRESSIVE:
                    return stringsNL.MoodLightDepressive;
                case MoodType.MODERATELY_LOW_DEPRESSIVE:
                    return stringsNL.MoodModeratelyLowDepressive;
                case MoodType.MODERATELTY_HIGH_DEPRESSIVE:
                    return stringsNL.MoodModeratelyHighDepressive;
                case MoodType.SERIOUSLY_DEPRESSIVE:
                    return stringsNL.MoodSeriouslyDepressive;
                case MoodType.DEPRESSIVE:
                    return stringsNL.MoodDepressive;

                default:
                    throw new ArgumentException($"Unknown MoodType '{e}'");
            }
        }
    }
}