﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace LifeChartApp.Utils
{
    public class JsonContent : StringContent
    {
        public readonly object Request;

        public JsonContent(object request) : base(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")
        {
            Request = request;
        }
    }
}