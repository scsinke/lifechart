﻿using System.Collections.Generic;
using LifeChartApp.Model;

namespace LifeChartApp.Interfaces
{
    public interface ILCTextOverviewController
    {
        /// <summary>
        /// Loads the first page which is year or month page. Depends on if there's more than one year.
        /// </summary>
        void LoadFirstPage();

        /// <summary>
        /// Loads the previous page
        /// </summary>
        void LoadPreviousPage();

        /// <summary>
        /// Loads the next page
        /// </summary>
        /// <param name="item"></param>
        void LoadNextPage(int item);
        void LoadDayOverViewPage(List<CompleteLifeChart> completeLifeCharts);
    }
}
