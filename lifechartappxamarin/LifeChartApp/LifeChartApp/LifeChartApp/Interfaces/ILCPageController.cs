﻿using LifeChartApp.Model;

namespace LifeChartApp.Interfaces
{
    public interface ILCPageController
    {
        /// <summary>
        /// Loads the previous page
        /// </summary>
        void PreviousPage();

        /// <summary>
        /// Loads the next page
        /// </summary>
        void NextPage();

        /// <summary>
        /// Loads the final page
        /// </summary>
        void EndPage();

        /// <summary>
        /// Loads the default medicine page
        /// </summary>
        /// <param name="med"></param>
        void LoadDefaultMedicinePage(DefaultMedicine med);


        /// <summary>
        /// Loads the add medicine page
        /// </summary>
        void LoadAddMedicinePage(Medicine med);

        /// <summary>
        /// Loads add extra medicine page
        /// </summary>
        void LoadAddMedicineToListPage();

        /// <summary>
        /// Loads the add life event page
        /// </summary>
        void LoadAddLifeEventPage();

        /// <summary>
        /// Loads the edit life event page
        /// </summary>
        /// <param name="lifeEvent"></param>
        void LoadLifeEventDetails(LifeEvent lifeEvent);

        /// <summary>
        /// Leaves the edit or add item page
        /// </summary>
        void LeaveAddItemPage();
    }
}
