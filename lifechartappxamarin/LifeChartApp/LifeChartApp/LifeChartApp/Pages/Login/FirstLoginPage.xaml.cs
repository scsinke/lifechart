﻿using LifeChartApp.Constants;
using System;
using Xamarin.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FirstLoginPage : ContentPage
	{
        private readonly int pincodeLength = 5;

		public FirstLoginPage ()
		{
			InitializeComponent ();
            pincodeEntry.TextChanged += TextChanged;
            pincodeCheckEntry.TextChanged += TextChanged;

            saveButton.Clicked += SaveButton_ClickedAsync;
        }


        private void TextChanged(object sender, EventArgs e)
        {
            bool enabled = CheckPincodeValidation();
            saveButton.IsEnabled = enabled;
        }

        private bool CheckMatchingPincode()
        {
            if (pincodeEntry.Text == pincodeCheckEntry.Text)
                return true;

            return false;
        }

        private bool CheckPincodeValidation()
        {
            bool success = false;

            if(!string.IsNullOrEmpty(pincodeEntry.Text) && !string.IsNullOrEmpty(pincodeCheckEntry.Text) && 
                pincodeEntry.Text.Length == pincodeLength && pincodeCheckEntry.Text.Length == pincodeLength)
                success = true;

            return success;
        }

        private async void SaveButton_ClickedAsync(object sender, EventArgs e)
        {
            IStrings stringsNL = StringsNL.Instance;

            if(CheckMatchingPincode())
            {
                //Open main login page
                SaveCredentials(pincodeEntry.Text);

                await Navigation.PushModalAsync(new NavigationPage(new MainPage()));
            }
            else
            {
                await DisplayAlert(stringsNL.WatchOut, stringsNL.PincodeNotMatching, stringsNL.Ok);
            }
        }

        private void SaveCredentials(string pincode)
        {
            if (!string.IsNullOrWhiteSpace(pincode))
            {
                Account account = new Account();

                account.Properties.Add("Pincode", pincode);

                AccountStore.Create().Save(account, App.AppName);
            }
        }

    }
}