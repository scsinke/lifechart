﻿using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages.Login
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationPage : ContentPage
	{
		public RegistrationPage ()
		{
			InitializeComponent();
            ConvertSexTypeEnum();

            sexPicker.SelectedIndexChanged += OnSexPickerSelectedIndexChanged;

            saveButton.Clicked += OnSaveButtonPressed;
		}

        private void OnSexPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            if (sexPicker.SelectedItem != null)
                saveButton.IsEnabled = true;
        }
        
        private async void OnSaveButtonPressed(object sender, EventArgs e)
        {
            IPersonDao personDao = PersonDao.Instance;
            float weightInKg = GetWeightFloat(weight.Text);
            SexType sexType = (SexType)sexPicker.SelectedIndex;
            Person person = new Person()
            {
                FirstName = firstname.Text,
                LastName = lastname.Text,
                Weight = weightInKg,
                Sex = sexType.ToString()
            };
            personDao.InsertPerson(person);

            await Navigation.PushModalAsync(new NavigationPage(new FirstLoginPage()));
        }

        private float GetWeightFloat(string input)
        {
            float weight;
            if (input.Contains("."))
            {
                string[] weightWithoutDecimal = input.Split('.');
                string stringAfterDecimal = weightWithoutDecimal[1];
                if (!string.IsNullOrEmpty(stringAfterDecimal))
                {
                    double numberAfterDecimal = double.Parse(stringAfterDecimal) * Math.Pow(0.1F, stringAfterDecimal.Length);
                    weight = float.Parse(weightWithoutDecimal[0]) + float.Parse(numberAfterDecimal.ToString());
                }
                else
                    float.TryParse(input, out weight);
            }
            else
                float.TryParse(input, out weight);
            return weight;
        }

        private void ConvertSexTypeEnum()
        {
            var sexTypes = Enum.GetValues(typeof(SexType));

            sexPicker.BatchBegin();
            foreach (Enum value in sexTypes)
            {
                sexPicker.Items.Add(SexTypeHelper.GetDescription((SexType)value));
            }
            sexPicker.BatchCommit();
        }
    }
}