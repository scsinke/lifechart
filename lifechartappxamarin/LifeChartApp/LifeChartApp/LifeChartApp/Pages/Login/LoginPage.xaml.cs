﻿using LifeChartApp.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        private readonly int pincodeLength = 5;

		public LoginPage ()
		{
			InitializeComponent ();
           
            pincodeEntry.TextChanged += PincodeEntry_TextChangedAsync;
        }

        private async void PincodeEntry_TextChangedAsync(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length == pincodeLength)
            {
                if (CheckLogin(e.NewTextValue))
                {
                    pincodeEntry.Unfocus();

                    await Navigation.PushModalAsync(new NavigationPage(new MainPage()));
                }
                else
                {
                    IStrings stringsNL = StringsNL.Instance;
                    pincodeEntry.Text = "";

                    await DisplayAlert(stringsNL.WatchOut, stringsNL.PincodeNotMatching, stringsNL.Ok);
                }
            }
        }

        private bool CheckLogin(string pincode)
        {
            var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();

            if(account != null)
            {
                KeyValuePair<string, string> accountPincode = account.Properties.FirstOrDefault(kvp => kvp.Key == "Pincode");

                if (accountPincode.Value == pincode)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

    }
}