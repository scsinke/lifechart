﻿using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using LifeChartApp.Pages;
using LifeChartApp.Pages.Graphs;
using System;
using Xamarin.Forms;

namespace LifeChartApp
{
    public partial class MainPage : ContentPage
    {
        private IPersonDao personDao;
        private IMedicineDao medicineDao;
        private ILifeChartDao lifeChartDao;
        public int CurrentLCPageIndex { private get; set; }

        private readonly Color selectedColor = Color.FromHex("#448AFF");
        private readonly Color normalColor = Color.FromHex("#616161");

        public MainPage()
		{
			InitializeComponent();
            InitStartPage();
            personDao = PersonDao.Instance;
            medicineDao = MedicineDao.Instance;
            lifeChartDao = LifeChartDao.Instance;
        }

        private void InitStartPage()
        {
            var page = new HomePage();
            placeHolder.Content = page.Content;
            TabHomeText.TextColor = selectedColor;
        }

        private void ResetColors()
        {
            TabHomeText.TextColor = normalColor;
            TabGraphsText.TextColor = normalColor;
            TabOverviewText.TextColor = normalColor;
            TabLifeChartText.TextColor = normalColor;
            TabProfileText.TextColor = normalColor;
        }

        private void HomeItemTapped(object sender, EventArgs a)
        {
            SetTitleOutOfLifeChart();
            ResetColors();
            TabHomeText.TextColor = selectedColor;
            var page = new HomePage();
            placeHolder.Content = page.Content;
        }

        private void GraphsItemTapped(object sender, EventArgs a)
        {
            SetTitleOutOfLifeChart();
            IStrings stringsNL = StringsNL.Instance;
            const int MINIMUM_LIFECHARTS = 2;

            if (lifeChartDao.GetAllCompleteLifeCharts().Count >= MINIMUM_LIFECHARTS)
            {
                ResetColors();
                TabGraphsText.TextColor = selectedColor;
                var page = new MoodSleepGraphPage(this, placeHolder, lifeChartDao);
                placeHolder.Content = page.Content;
            }
            else
            {
                DisplayAlert(stringsNL.WatchOut, stringsNL.NotEnoughLifeCharts, stringsNL.Ok);
            }
        }

        private void OverviewItemTapped(object sender, EventArgs a)
        {
            ILCTextOverviewController lcTextOverviewController = new LCTextOverviewController(placeHolder, lifeChartDao.GetAllCompleteLifeCharts());
            OverViewTapped();
            lcTextOverviewController.LoadFirstPage();
        }

        public void OverViewTapped()
        {
            SetTitleOutOfLifeChart();
            ResetColors();
            TabOverviewText.TextColor = selectedColor;
        }

        private void LifeChartItemTapped(object sender, EventArgs a)
        {
            LoadFirstLifeChartPage();
        }

        private void ProfileItemTapped(object sender, EventArgs a)
        {
            SetTitleOutOfLifeChart();
            ResetColors();
            TabProfileText.TextColor = selectedColor;

            var page = new ProfilePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }

        private void LoadFirstLifeChartPage()
        {
            IStrings stringsNL = StringsNL.Instance;
            ILCPageController LCPageController = new LCPageController(this, placeHolder, lifeChartDao);

            LifeChart lifechart = lifeChartDao.GetLatestLifeChart();

            if (lifechart == null || lifechart.Date.Date < DateTimeOffset.Now.Date)
            {
                ResetColors();
                TabLifeChartText.TextColor = selectedColor;

                LifeChartController lifeChartController = LifeChartController.GetInstance();
                lifeChartController.SetPerson(personDao.GetPerson(1));

                LCPageController.NextPage();
            }
            else
            {
                DisplayAlert(stringsNL.LifeChartAlreadyFilledIn, stringsNL.LifeChartFilledInToday, stringsNL.Ok);
            }
        }

        private void SetTitleOutOfLifeChart()
        {
            IStrings stringsNL = StringsNL.Instance;
            Title = stringsNL.AppTitle;
        }

        public void SetTitleInsideLifeChart(int currentIndex, int maxIndex)
        {
            IStrings stringsNL = StringsNL.Instance;
            SetTitleOutOfLifeChart();

            string stringToAdd = " - " + currentIndex.ToString() + " / " + maxIndex.ToString();
            Title = Title + stringToAdd;
        }

        //Disable app from closing by pressing the back button
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}