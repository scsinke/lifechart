﻿using LifeChartApp.Dao;
using LifeChartApp.Model;
using LifeChartApp.Pages;
using LifeChartApp.Pages.LifeChartText;
using LifeChartApp.Interfaces;
using LifeChartApp.Pages.Graphs.Util;
using LifeChartApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages.Graphs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoodSleepGraphPage : ContentPage
	{
        private MainPage mainPage;
        private ContentView placeHolder;
        private MoodViewModel moodViewModel;
        private SleepViewModel sleepViewModel;
        private ILifeChartDao lifeChartDao;

        private List<CompleteLifeChart> completeLifeChartList;

        public MoodSleepGraphPage(MainPage mainPage, ContentView placeHolder, ILifeChartDao lifeChartDao)
        {
            InitializeComponent();

            this.mainPage = mainPage;
            this.placeHolder = placeHolder;
            this.lifeChartDao = lifeChartDao;
            moodViewModel = new MoodViewModel();
            sleepViewModel = new SleepViewModel();

            completeLifeChartList = lifeChartDao.GetCompleteLifeChartsBetweenDates(DateTime.Now.AddDays(-30), DateTime.Now);

            initGraphData();

            stepSeries.ItemsSource = moodViewModel.Data;
            lineSeries.ItemsSource = sleepViewModel.Data;
            graphContent.Content = chart;

            detailButton.Clicked += OnDetailButtonPressed;
        }

        private void OnDetailButtonPressed(object sender, EventArgs e)
        {
            List<CompleteLifeChart> clc = lifeChartDao.GetAllCompleteLifeCharts();
            ILCTextOverviewController lcTextOverviewController = new LCTextOverviewController(placeHolder, clc);

            mainPage.OverViewTapped();

            if (clc.Count > 0)
            {
                lcTextOverviewController.LoadFirstPage();
            }
            else
            {
                lcTextOverviewController.LoadDayOverViewPage(completeLifeChartList);
            }
        }

        private void initGraphData()
        {
            foreach (CompleteLifeChart clc in completeLifeChartList)
            {
                initMoodData(clc.LifeChart);
                initSleepData(clc.LifeChart);
            }
        }

        private void initMoodData(LifeChart lifeChart)
        {
            MoodType moodType = (MoodType)Enum.Parse(typeof(MoodType), lifeChart.Mood);

            string shortDate = lifeChart.Date.ToShortDateString();

            MoodData moodData = new MoodData()
            {
                Date = Convert.ToDateTime(shortDate),
                Mood = (int)moodType
            };
            moodViewModel.AddMoodData(moodData);
        }

        private void initSleepData(LifeChart lifeChart)
        {
            string shortDate = lifeChart.Date.ToShortDateString();

            SleepData sleepData = new SleepData()
            {
                Date = Convert.ToDateTime(shortDate),
                Sleep = lifeChart.Sleep
            };
            sleepViewModel.AddSleepData(sleepData);
        }
	}
}