﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeChartApp.Pages.Graphs.Util
{
    public class MoodViewModel
    {
        public List<MoodData> Data { get; }
        
        public MoodViewModel()
        {
            Data = new List<MoodData>();
        }

        public void AddMoodData(MoodData moodData)
        {
            Data.Add(moodData);
        }
    }
}
