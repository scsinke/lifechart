﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeChartApp.Pages.Graphs.Util
{
    public class MoodData
    {
        public DateTime Date { get; set; }
        
        public int Mood { get; set; }
    }
}
