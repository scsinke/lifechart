﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeChartApp.Pages.Graphs.Util
{
    public class SleepData
    {
        public DateTime Date { get; set; }

        public double Sleep { get; set; }
    }
}
