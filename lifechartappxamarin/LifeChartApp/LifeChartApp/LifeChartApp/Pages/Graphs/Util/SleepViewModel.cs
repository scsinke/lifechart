﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeChartApp.Pages.Graphs.Util
{
    public class SleepViewModel
    {
        public List<SleepData> Data { get; }

        public SleepViewModel()
        {
            Data = new List<SleepData>();
        }

        public void AddSleepData(SleepData sleepData)
        {
            Data.Add(sleepData);
        }
    }
}
