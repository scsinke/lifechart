﻿using LifeChartApp.Model;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MedicinePage : ContentPage
    {
        private LifeChartController lifeChartController;
        private MedicineController mcInstance;
        private ILCPageController LCPageController;

        public MedicinePage(ILCPageController LCPageController)
        {
            InitializeComponent();
            this.LCPageController = LCPageController;

            PageSetup();

            nextButton.Clicked += LoadNextPage;
            AddMedicinesList.ItemTapped += AddMedicinesList_ItemTapped;
            StoredMedicinesList.ItemTapped += StoredMedicinesList_ItemTapped;
        }

        private void StoredMedicinesList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            DefaultMedicine med = (DefaultMedicine)e.Item;
            LCPageController.LoadDefaultMedicinePage(med);
        }

        private void AddMedicinesList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Medicine med = (Medicine)e.Item;
            LCPageController.LoadAddMedicinePage(med);
        }

        public void AddMedicineToList(object sender, EventArgs e)
        {
            LCPageController.LoadAddMedicineToListPage();
        }

        private void PageSetup()
        {
            mcInstance = MedicineController.GetInstance();
            lifeChartController = LifeChartController.GetInstance();

            StoredMedicinesList.ItemsSource = null;
            StoredMedicinesList.ItemsSource = lifeChartController.GetDefaultMedicines();

            AddMedicinesList.ItemsSource = null;
            AddMedicinesList.ItemsSource = mcInstance.GetCurrentMedicineList();
        }

        private void DefaultMedToggled(object sender, ToggledEventArgs e)
        {
            if ((sender as Switch).BindingContext is DefaultMedicine newMed)
            {
                List<DefaultMedicine> defMedList = mcInstance.GetCurrentDefaultMeds();
                DefaultMedicine updateMed = defMedList.FirstOrDefault(med => med.MedicineId == newMed.MedicineId);

                defMedList.Remove(updateMed);
                defMedList.Add(newMed);

                mcInstance.SetCurrentDefaultMedicineList(defMedList);
                mcInstance.UpdateDefaultMedicineListToDb();
            }
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }
    }
}
