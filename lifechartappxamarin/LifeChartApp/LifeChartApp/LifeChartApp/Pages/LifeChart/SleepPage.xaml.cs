﻿using LifeChartApp.Model;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SleepPage : ContentPage
    {
        private LifeChartController lifeChartController;
        private ILCPageController LCPageController;

        public SleepPage(ILCPageController LCPageController)
        {
            InitializeComponent();
            this.LCPageController = LCPageController;

            lifeChartController = LifeChartController.GetInstance();

            sleepEntry.TextChanged += SleepEntry_TextChanged;

            nextButton.Clicked += LoadNextPage;
            backButton.Clicked += LoadPreviousPage;

            float sleep = lifeChartController.GetSleep();
            if (sleep >= 0)
                ChangeSleep(sleep);
        }

        private void ChangeSleep(float hours)
        {
            sleepEntry.Text = hours.ToString();
        }

        private void SleepEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            float.TryParse(e.NewTextValue, out float sleep);

            if (sleep > 0)
            {
                lifeChartController.SetSleep(sleep);
            }
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }
    }
}