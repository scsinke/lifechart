﻿using LifeChartApp.Model;
using System;
using Xamarin.Forms;
using LifeChartApp.Interfaces;
using LifeChartApp.Dao;

namespace LifeChartApp.Pages
{
    public partial class ComplaintsPage : ContentPage
    {
        private LifeChartController lifeChartController;
        private ILCPageController LCPageController;

        public ComplaintsPage(ILCPageController LCPageController)
        {
            InitializeComponent();

            lifeChartController = LifeChartController.GetInstance();

            this.LCPageController = LCPageController;

            complaintsText.TextChanged += ComplaintsTextChanged;

            nextButton.Clicked += LoadNextPage;
            backButton.Clicked += LoadPreviousPage;

            string complaints = lifeChartController.GetComplaints();
            if (!String.IsNullOrEmpty(complaints))
                complaintsText.Text = complaints;
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }

        private void ComplaintsTextChanged(object sender, TextChangedEventArgs e)
        {
            lifeChartController.SetComplaints(e.NewTextValue);
        }
    }
}