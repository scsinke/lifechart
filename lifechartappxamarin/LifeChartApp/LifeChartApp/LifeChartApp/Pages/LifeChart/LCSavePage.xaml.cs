﻿using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LCSavePage : ContentPage
	{
        private ILCPageController LCPageController;
        private ILifeChartDao lifeChartDao;
        private LifeChartController lifeChartController;
        private LifeEventController lifeEventController;
        private MedicineController medicineController;

        public LCSavePage(ILifeChartDao lifeChartDao, ILCPageController LCPageController)
		{
			InitializeComponent();
            this.LCPageController = LCPageController;
            this.lifeChartDao = lifeChartDao;

            lifeChartController = LifeChartController.GetInstance();
            lifeEventController = LifeEventController.GetInstance();
            medicineController = MedicineController.GetInstance();

            backButton.Clicked += OnBackButtonClicked;
            saveButton.Clicked += OnSaveButtonClicked;
        }

        private void OnBackButtonClicked(object sender, EventArgs e)
        {
            LoadPreviousPage();
        }

        private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            IStrings stringsNL = StringsNL.Instance;

            List<string> emptyFields = new List<string>();
            if (lifeChartController.GetMood() == null)
                emptyFields.Add(stringsNL.EmptyManiaMessage);

            if (emptyFields.Count > 0)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (string str in emptyFields)
                {
                    stringBuilder.Append("* " + str + Environment.NewLine);
                }

                DisplayAlert(stringsNL.LifeChartNotCompleted, stringBuilder.ToString(), stringsNL.Ok);

                return;
            }

            lifeChartController.SetDate(DateTime.Now);

            CompleteLifeChart completeLifeChart = new CompleteLifeChart()
            {
                LifeChart = lifeChartController.GetLifeChart(),
                Medicines = medicineController.GetAllTakenMeds(),
                LifeEvents = lifeEventController.GetLifeEvents()
            };

            lifeChartDao.InsertCompleteLifeChart(completeLifeChart, lifeChartController.GetPerson().PersonId);

            saveButton.IsEnabled = false;
            backButton.IsEnabled = false;

            DisplayAlert(stringsNL.LifeChartSaveSuccess, stringsNL.LifeChartSaved, stringsNL.Ok);
        }

        private void LoadPreviousPage()
        {
            LCPageController.LeaveAddItemPage();
        }
    }
}