﻿using System;

using Xamarin.Forms;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;

namespace LifeChartApp.Pages
{
    public partial class EpisodePage : ContentPage
    {
        private LifeChartController lifeChartController;
        private ILCPageController LCPageController;

        public EpisodePage(ILCPageController LCPageController)
        {
            InitializeComponent();
            this.LCPageController = LCPageController;

            lifeChartController = LifeChartController.GetInstance();

            episodeText.TextChanged += EpisodeTextChanged;

            nextButton.Clicked += LoadNextPage;
            backButton.Clicked += LoadPreviousPage;

            string episode = lifeChartController.GetSeverityOfEpisode();
            if (!String.IsNullOrEmpty(episode))
                episodeText.Text = episode;
        }

        private void EpisodeTextChanged(object sender, TextChangedEventArgs e)
        {
            lifeChartController.SetSeverityOfEpisode(e.NewTextValue);
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }
    }
}