﻿using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;
using LifeChartApp.Constants;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddLifeEventPage : ContentPage
	{
        private readonly double stepValue = 1.0;
        private readonly int maxSliderValue = 4;
        private readonly int minSliderValue = -4;
        private readonly int standardSliderValue = 0;
        private LifeEventController lifeEventController;

        private ILCPageController LCPageController;

        public AddLifeEventPage (ILCPageController LCPageController)
		{
			InitializeComponent ();
            lifeEventController = LifeEventController.GetInstance();
            this.LCPageController = LCPageController;

            influenceSlider.Maximum = maxSliderValue;
            influenceSlider.Minimum = minSliderValue;
            influenceSlider.Value = standardSliderValue;
            influenceSlider.ValueChanged += OnSliderValueChanged;

            lifeEventNameEntry.TextChanged += LifeEventNameEntry_TextChanged;

            saveButton.Clicked += OnSaveButtonClicked;
            cancelButton.Clicked += OnCancelButtonClicked;
        }

        private void LifeEventNameEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.IsNullOrEmpty(lifeEventNameEntry.Text))
                saveButton.IsEnabled = false;
            else
                saveButton.IsEnabled = true;
        }

        private void OnSliderValueChanged(object sender, ValueChangedEventArgs e)
        {
            var newStep = Math.Round(e.NewValue / stepValue);
            influenceSlider.Value = newStep * stepValue;
        }

        private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            String lifeEventName = lifeEventNameEntry.Text;
            int influence = (int)influenceSlider.Value;
            if (!String.IsNullOrEmpty(lifeEventName))
            {
                LifeEvent lifeEvent = new LifeEvent()
                {
                    Event = lifeEventName,
                    MoodInfluence = influence
                };

                if (SaveLifeEventList(lifeEvent))
                {
                    LCPageController.LeaveAddItemPage();
                }
            }
        }

        private void OnCancelButtonClicked(object sender, EventArgs e)
        {
            LCPageController.LeaveAddItemPage();
        }

        private bool SaveLifeEventList(LifeEvent lifeEvent)
        {
            IStrings stringsNL = StringsNL.Instance;
            List<LifeEvent> lifeEventList = lifeEventController.GetLifeEvents();

            LifeEvent matchingLifeEvent = lifeEventList.FirstOrDefault(findLifeEvent => findLifeEvent.Event == lifeEvent.Event);

            if (matchingLifeEvent == null)
            {
                lifeEvent.EventId = lifeEventList.Count;
                lifeEventList.Add(lifeEvent);

                lifeEventController.SetLifeEvents(lifeEventList);

                return true;
            }
            else
            {
                DisplayAlert(stringsNL.FailedToAdd, stringsNL.DoubleLifeEvent, stringsNL.Ok);
                return false;
            }
        }
    }
}