﻿using LifeChartApp.Model;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System;
using LifeChartApp.Interfaces;
using LifeChartApp.Constants;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LifeEventPageDetail : ContentPage
	{
        private readonly double stepValue = 1.0;
        private readonly int maxSliderValue = 4;
        private readonly int minSliderValue = -4;
        private LifeEvent _lifeEvent;
        private int lifeEventIndex;

        private LifeEventController lifeEventController;

        private ILCPageController LCPageController;

        public LifeEventPageDetail (ILCPageController LCPageController, LifeEvent lifeEvent)
        {
            InitializeComponent();
            this.LCPageController = LCPageController;
            _lifeEvent = lifeEvent;
            lifeEventController = LifeEventController.GetInstance();

            LifeEventSetup();

            editButton.Clicked += EditButton_Clicked;
            deleteButton.Clicked += DeleteButton_ClickedAsync;
            influenceSlider.Maximum = maxSliderValue;
            influenceSlider.Minimum = minSliderValue;
            influenceSlider.Value = _lifeEvent.MoodInfluence;

            BindValuesTolabels();
        }

        private void LifeEventSetup()
        {
            List<LifeEvent> lifeEventList = lifeEventController.GetLifeEvents();
            lifeEventIndex = lifeEventList.FindIndex(le => le.EventId == _lifeEvent.EventId);

            if (lifeEventIndex < 0)
                lifeEventIndex++;
        }

        private void BindValuesTolabels()
        {
            if (!String.IsNullOrEmpty(_lifeEvent.Event))
            {
                lifeEventNameEntry.Text = _lifeEvent.Event;
            }

            lifeEventNameEntry.TextChanged += LifeEventNameEntry_TextChanged;
            influenceSlider.ValueChanged += InfluenceSlider_ValueChanged;
        }

        private void LifeEventNameEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            _lifeEvent.Event = e.NewTextValue;
        }

        private void InfluenceSlider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var newStep = Math.Round(e.NewValue / stepValue);
            influenceSlider.Value = newStep * stepValue;
            CheckInputFields();
            _lifeEvent.MoodInfluence = (int)e.NewValue;
        }

        private async void DeleteButton_ClickedAsync(object sender, EventArgs e)
        {
            IStrings stringsNL = StringsNL.Instance;
            var result = await DisplayAlert(stringsNL.WatchOut, stringsNL.WantToDeleteLifeEvent, stringsNL.Yes, stringsNL.No);

            if (result)
            {
                RemoveLifeEvent();
                GoBackToLifeEventPage();
            }
        }

        private void EditButton_Clicked(object sender, EventArgs e)
        {
            if (EditLifeEventList())
                GoBackToLifeEventPage();
        }

        private void RemoveLifeEvent()
        {
            List<LifeEvent> eventList = lifeEventController.GetLifeEvents();
            LifeEvent matchingEvent = eventList.FirstOrDefault(life => life.Event == _lifeEvent.Event && life.EventId == _lifeEvent.EventId);

            if (matchingEvent != null)
            {
                eventList.Remove(matchingEvent);
            }

            lifeEventController.SetLifeEvents(eventList);
        }

        private void CheckInputFields()
        {
            if (!String.IsNullOrEmpty(lifeEventNameEntry.Text))
                editButton.IsEnabled = true;
            else
                editButton.IsEnabled = false;
        }

        private void GoBackToLifeEventPage()
        {
            LCPageController.LeaveAddItemPage();
        }

        private bool EditLifeEventList()
        {
            IStrings stringsNL = StringsNL.Instance;
            List<LifeEvent> lifeEventList = lifeEventController.GetLifeEvents();
            lifeEventList[lifeEventIndex] = _lifeEvent;

            int matchingLifeEvents = lifeEventList.Count(le => le.Event == _lifeEvent.Event);

            if (matchingLifeEvents <= 1)
            {
                lifeEventController.SetLifeEvents(lifeEventList);
                return true;
            }
            else
            {
                DisplayAlert(stringsNL.WatchOut, stringsNL.DoubleLifeEvent, stringsNL.Ok);
                return false;
            }
        }
    }
}