﻿using LifeChartApp.Model;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;
using System.Globalization;
using System.Linq;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeightPage : ContentPage
    {
        private readonly float amountToChangeWeight = 0.1F;
        private LifeChartController lifeChartController;

        private ILCPageController LCPageController;

        public WeightPage(ILCPageController LCPageController)
        {
            InitializeComponent();
            this.LCPageController = LCPageController;
            lifeChartController = LifeChartController.GetInstance();
            Person person = lifeChartController.GetPerson();

            if (lifeChartController.GetWeight() > 0)
            {
                weightEntry.Text = lifeChartController.GetWeight().ToString(new CultureInfo("en-US"));
            }
            else
            {
                if (person != null)
                {
                    weightEntry.Text = lifeChartController.GetPerson().Weight.ToString(new CultureInfo("en-US"));
                    float weight = GetWeightFloat(weightEntry.Text);

                    if(weight > 0)
                        lifeChartController.SetWeight(weight);
                }
            }

            weightEntry.TextChanged += WeightChanged;
            nextButton.Clicked += LoadNextPage;
            backButton.Clicked += LoadPreviousPage;
        }

        private void WeightChanged(object sender, TextChangedEventArgs e)
        {
            float weight = GetWeightFloat(weightEntry.Text);

            if (weight > 0)
                lifeChartController.SetWeight(weight);
        }

        private void AddToWeight(object sender, EventArgs e)
        {
            float weight = GetWeightFloat(weightEntry.Text);
                float newWeight = weight + amountToChangeWeight;
                weightEntry.Text = (newWeight).ToString(new CultureInfo("en-US"));
        }

        private void RemoveFromWeight(object sender, EventArgs e)
        {
            float weight = GetWeightFloat(weightEntry.Text);
            float newWeight = weight - amountToChangeWeight;
                if (newWeight < 0)
                    weightEntry.Text = "0";
                else
                    weightEntry.Text = (newWeight).ToString(new CultureInfo("en-US"));
        }

        private float GetWeightFloat(string input)
        {
            float weight;
            if (input.Contains("."))
            {
                string[] weightWithoutDecimal = input.Split('.');
                string stringAfterDecimal = weightWithoutDecimal[1];
                if (!string.IsNullOrEmpty(stringAfterDecimal))
                {
                    double numberAfterDecimal = double.Parse(stringAfterDecimal) * Math.Pow(0.1F, stringAfterDecimal.Length);
                    weight = float.Parse(weightWithoutDecimal[0]) + float.Parse(numberAfterDecimal.ToString());
                }
                else
                    float.TryParse(input, out weight);
            }
            else
                float.TryParse(input, out weight);
            return weight;
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }
    }
}