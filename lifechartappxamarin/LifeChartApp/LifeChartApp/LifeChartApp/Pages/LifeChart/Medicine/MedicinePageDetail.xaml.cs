﻿using LifeChartApp.Model;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System;
using LifeChartApp.Interfaces;
using LifeChartApp.Constants;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MedicinePageDetail : ContentPage
	{
        private Medicine _med;
        private String oldMedName;
        private MedicineController medicineController;
        private int medIndex;

        ILCPageController LCPageController;

        public MedicinePageDetail (ILCPageController LCPageController, Medicine med)
		{
			InitializeComponent ();
            this.LCPageController = LCPageController;
            _med = med;

            medicineController = MedicineController.GetInstance();

            MedListSetup();

            editButton.Clicked += EditButton_Clicked;
            deleteButton.Clicked += DeleteButton_ClickedAsync;

            BindValuesTolabels();
		}

        private void MedListSetup()
        {
            List<DefaultMedicine> medicineList = medicineController.GetAllDefaultMeds();
            medIndex = medicineList.FindIndex(m => m.MedicineId == _med.MedicineId);
            if (medIndex < 0)
                medIndex = medicineList.Count;
        }

        private void BindValuesTolabels()
        {
            if (!String.IsNullOrEmpty(_med.MedicineName))
            {
                MedicineName.Text = _med.MedicineName;
                oldMedName = _med.MedicineName;
            }

            Dose.Text = _med.DoseInMg.ToString();
            Amount.Text = _med.AmountPerDay.ToString();

            MedicineName.TextChanged += MedicineName_TextChanged;
            Dose.TextChanged += Dose_TextChanged;
            Amount.TextChanged += Amount_TextChanged;
        }

        private void Amount_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            if (!String.IsNullOrEmpty(e.NewTextValue))
            {
                _med.AmountPerDay = Int32.Parse(e.NewTextValue);
            }
        }

        private void Dose_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            if (!String.IsNullOrEmpty(e.NewTextValue))
            {
                _med.DoseInMg = Int32.Parse(e.NewTextValue);
            }
        }

        private void MedicineName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            _med.MedicineName = e.NewTextValue;
        }

        private void GoBackToMedicinePage()
        {
            LCPageController.LeaveAddItemPage();
        }

        private void CheckInputFields()
        {
            if (!String.IsNullOrEmpty(MedicineName.Text) && !String.IsNullOrEmpty(Dose.Text)
                && !String.IsNullOrEmpty(Amount.Text))
                editButton.IsEnabled = true;
            else
                editButton.IsEnabled = false;
        }

        private async void DeleteButton_ClickedAsync(object sender, EventArgs e)
        {
            IStrings stringsNL = StringsNL.Instance;

            var result = await DisplayAlert(stringsNL.WatchOut, stringsNL.DeleteMedMessage, stringsNL.Yes, stringsNL.No);

            if (result)
            {
                RemoveMedicine();
                GoBackToMedicinePage();
            }
        }

        private void RemoveMedicine()
        {
            List<Medicine> medlist = medicineController.GetCurrentMedicineList();
            Medicine matchingMed = medlist.FirstOrDefault(med => med.MedicineName == _med.MedicineName);

            if (matchingMed != null)
            {
                medlist.Remove(matchingMed);
            }

            medicineController.SetCurrentMedicineList(medlist);
        }

        private void EditButton_Clicked(object sender, EventArgs e)
        {
            if (EditMedicine())
                GoBackToMedicinePage();
        }

        private bool EditMedicine()
        {
            List<Medicine> medList = medicineController.GetCurrentMedicineList();
            medList[medIndex] = _med;

            int matchingMedicines = medList.Count(med => med.DoseInMg == _med.DoseInMg && med.MedicineName == _med.MedicineName);

            if (matchingMedicines <= 1)
            {
                medicineController.SetCurrentMedicineList(medList);
                return true;
            }
            else
            {
                IStrings stringsNL = StringsNL.Instance;
                DisplayAlert(stringsNL.WatchOut, stringsNL.DuplicateMedFound, stringsNL.Ok);

                return false;
            }
        }
    }
}