﻿using LifeChartApp.Model;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;
using System.Linq;
using LifeChartApp.Constants;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddMedicineToListPage : ContentPage
	{
        private ILCPageController LCPageController;
        private MedicineController mcInstance;


		public AddMedicineToListPage (ILCPageController LCPageController)
		{
			InitializeComponent();
            mcInstance = MedicineController.GetInstance();
            this.LCPageController = LCPageController;
            cancelButton.Clicked += CancelButton_Clicked;
            saveButton.Clicked += SaveButton_Clicked;

            MedicineName.TextChanged += MedicineName_TextChanged;
            Dose.TextChanged += Dose_TextChanged;
            Amount.TextChanged += Amount_TextChanged;
        }

        private void Amount_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
        }

        private void Dose_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
        }

        private void MedicineName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
        }

        private void CheckInputFields()
        {
            if (!String.IsNullOrEmpty(MedicineName.Text) && !String.IsNullOrEmpty(Dose.Text)
                && !String.IsNullOrEmpty(Amount.Text))
                saveButton.IsEnabled = true;
            else
                saveButton.IsEnabled = false;
        }

        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            String medName = MedicineName.Text;
            int medDose = int.Parse(Dose.Text);
            int medAmount = int.Parse(Amount.Text);

            Medicine med = new Medicine()
            {
                DoseInMg = medDose,
                AmountPerDay = medAmount,
                MedicineName = medName
            };

            if (SaveMedList(med))
            {
                LCPageController.LeaveAddItemPage();
            }
        }

        private bool SaveMedList(Medicine med)
        {
            List<Medicine> medList = mcInstance.GetCurrentMedicineList();

            Medicine matchingMed = medList.FirstOrDefault(findMed => findMed.MedicineName == med.MedicineName && findMed.DoseInMg == med.DoseInMg);

            if (matchingMed == null)
            {
                medList.Add(med);

                mcInstance.SetCurrentMedicineList(medList);

                return true;
            }
            else
            {
                IStrings stringsNL = StringsNL.Instance;

                DisplayAlert(stringsNL.FailedToAdd, stringsNL.DuplicateMedFound, stringsNL.Ok);
                return false;
            }
        }

        private void CancelButton_Clicked(object sender, EventArgs e)
        {
            LCPageController.LeaveAddItemPage();
        }
    }
}