﻿using LifeChartApp.Model;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;
using System.Collections.Generic;
using System.Linq;
using LifeChartApp.Constants;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LifeEventPage : ContentPage
	{
        private LifeChartController lifeChartController;
        private LifeEventController lifeEventController;
        private ILCPageController LCPageController;

		public LifeEventPage (ILCPageController LCPageController)
		{
			InitializeComponent ();
            lifeChartController = LifeChartController.GetInstance();
            lifeEventController = LifeEventController.GetInstance();
            this.LCPageController = LCPageController;

            LifeEventList.ItemTapped += LifeEventList_ItemTapped;
            LifeEventList.ItemsSource = null;
            LifeEventList.ItemsSource = lifeEventController.GetLifeEvents();
            PageSetup();
        }
    
        private void PageSetup()
        {
            nextButton.Clicked += OnNextButtonClicked;
            backButton.Clicked += OnBackButtonClicked;
        }

        private void LifeEventList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            LifeEvent lifeEvent = (LifeEvent)e.Item;
            LCPageController.LoadLifeEventDetails(lifeEvent);
        }

        private void OnNextButtonClicked(object sender, EventArgs e)
        {
            Person person = lifeChartController.GetPerson();
            if (person.Sex.Equals(SexType.MALE.ToString()))
            {
                LCPageController.EndPage();
            }
            else if (person.Sex.Equals(SexType.FEMALE.ToString()))
            {
                LCPageController.NextPage();
            }           
        }

        private void OnBackButtonClicked(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }

        private void AddNewLifeEvent(object sender, EventArgs e)
        {
            LCPageController.LoadAddLifeEventPage();
        }
    }
}