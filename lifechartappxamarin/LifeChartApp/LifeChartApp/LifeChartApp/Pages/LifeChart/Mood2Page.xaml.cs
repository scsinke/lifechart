﻿using LifeChartApp.Model;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;
using LifeChartApp.Constants;
using LifeChartApp.Utils;
using System.Collections.Generic;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Mood2Page : ContentPage
    {
        Boolean unpleasant = false;

        private LifeChartController lifeChartController;
        private ILCPageController LCPageController;

        public Mood2Page(ILCPageController LCPageController)
        {
            InitializeComponent();

            StemmingListSetup();

            lifeChartController = LifeChartController.GetInstance();
            this.LCPageController = LCPageController;

            unpleasantToggle.Toggled += UnpleasantToggleTrigger;

            nextButton.Clicked += LoadNextPage;
            backButton.Clicked += LoadPreviousPage;

            unpleasantToggle.IsToggled = lifeChartController.GetUnrestrainedMania();
            moodlist.ItemTapped += MoodList_ItemTapped;

            MoodType currentMood;

            if (lifeChartController.GetMood() != null)
                Enum.TryParse(lifeChartController.GetMood(), out currentMood);
            else
                currentMood = MoodType.NONE;

            moodValue.Text = MoodTypeHelper.GetDescription(currentMood);
        }

        private void StemmingListSetup()
        {
            IStrings stringsNL = StringsNL.Instance;

            Mood manieErnstig = new Mood()
            {
                Title = stringsNL.MoodSerious,
                Value = MoodType.SERIOUS.ToString(),
                Description = stringsNL.MoodSeriousDescription,
                Color = Color.FromRgb(236, 64, 122)
            };

            Mood manieMatigHoog = new Mood()
            {
                Title = stringsNL.MoodModeratelyHigh,
                Value = MoodType.MODERATELY_HIGH.ToString(),
                Description = stringsNL.MoodModeratelyHighDescription,
                Color = Color.FromRgb(240, 98, 146)
            };

            Mood manieMatigLaag = new Mood()
            {
                Title = stringsNL.MoodModeratelyLow,
                Value = MoodType.MODERATELY_LOW.ToString(),
                Description = stringsNL.MoodModeratelyLowDescription,
                Color = Color.FromRgb(255, 183, 77)
            };

            Mood manieLicht = new Mood()
            {
                Title = stringsNL.MoodLight,
                Value = MoodType.LIGHT.ToString(),
                Description = stringsNL.MoodLightDescription,
                Color = Color.FromRgb(205, 220, 57)
            };

            Mood stabiel = new Mood()
            {
                Title = stringsNL.MoodStable,
                Value = MoodType.STABLE.ToString(),
                Description = stringsNL.MoodStableDescription,
                Color = Color.FromRgb(139, 195, 74)
            };

            Mood depressieLicht = new Mood()
            {
                Title = stringsNL.MoodLightDepressive,
                Value = MoodType.LIGHT_DEPRESSIVE.ToString(),
                Description = stringsNL.MoodLightDepressiveDescription,
                Color = Color.FromRgb(38, 166, 154)
            };

            Mood depressieMatigLaag = new Mood()
            {
                Title = stringsNL.MoodModeratelyLowDepressive,
                Value = MoodType.MODERATELY_LOW_DEPRESSIVE.ToString(),
                Description = stringsNL.MoodModeratelyLowDepressiveDescription,
                Color = Color.FromRgb(0, 188, 212)
            };

            Mood depressieMatigHoog = new Mood()
            {
                Title = stringsNL.MoodModeratelyHighDepressive,
                Value = MoodType.MODERATELY_HIGH.ToString(),
                Description = stringsNL.MoodModeratelyHighDepressiveDescription,
                Color = Color.FromRgb(140, 158, 255)
            };

            Mood depressieErnstig = new Mood()
            {
                Title = stringsNL.MoodSeriouslyDepressive,
                Value = MoodType.SERIOUSLY_DEPRESSIVE.ToString(),
                Description = stringsNL.MoodSeriouslyDepressiveDescription,
                Color = Color.FromRgb(179, 136, 255)
            };

            Mood depressie = new Mood()
            {
                Title = stringsNL.MoodDepressive,
                Value = MoodType.DEPRESSIVE.ToString(),
                Description = stringsNL.MoodDepressiveDescription,
                Color = Color.FromRgb(103, 58, 183)
            };

            List<Mood> moodChoices = new List<Mood>(){
                manieErnstig,
                manieMatigHoog,
                manieMatigLaag,
                manieLicht,
                stabiel,
                depressieLicht,
                depressieMatigLaag,
                depressieMatigHoog,
                depressieErnstig,
                depressie
            };

            moodlist.ItemsSource = moodChoices;
        }

        void MoodList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Mood item = (Mood)e.Item;
            lifeChartController.SetMood(item.Value);
            moodValue.Text = item.Title;
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }

        private void UnpleasantToggleTrigger(object sender, ToggledEventArgs e){
            unpleasant = e.Value;
            lifeChartController.SetUnrestrainedMania(unpleasant);
        }
    }
}