﻿using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenstruationPage : ContentPage
    {
        private ILCPageController LCPageController;
        private LifeChartController lifeChartController;

        public MenstruationPage(ILCPageController LCPageController)
        {
            InitializeComponent();
            this.LCPageController = LCPageController;

            lifeChartController = LifeChartController.GetInstance();

            saveButton.Clicked += SaveLifeChart;
            backButton.Clicked += LoadPreviousPage;

            if (lifeChartController.GetMenstruation())
                noButton.IsEnabled = false;
            else
                yesButton.IsEnabled = false;
        }

        void YesButtonClicked(object sender, System.EventArgs e)
        {
            lifeChartController.SetMenstruated(true);
            yesButton.IsEnabled = false;
            noButton.IsEnabled = true;
        }

        void NoButtonClicked(object sender, System.EventArgs e)
        {
            lifeChartController.SetMenstruated(false);
            yesButton.IsEnabled = true;
            noButton.IsEnabled = false;
        }

        private void SaveLifeChart(object sender, EventArgs e)
        {
            LCPageController.EndPage();
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }
    }
}