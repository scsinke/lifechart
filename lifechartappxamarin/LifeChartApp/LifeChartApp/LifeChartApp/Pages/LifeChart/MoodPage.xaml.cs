﻿using LifeChartApp.Model;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Interfaces;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoodPage : ContentPage
    {
        private LifeChartController lifeChartController;
        Person person;
        private double stepValue = 1.0;
        private ILCPageController LCPageController;

        public MoodPage(ILCPageController LCPageController)
        {
            InitializeComponent();
            lifeChartController = LifeChartController.GetInstance();
            this.LCPageController = LCPageController;
            person = lifeChartController.GetPerson();

            moodSlider.ValueChanged += OnSliderValueChanged;
            amountOfMoodChangesEntry.TextChanged += OnAmountOfMoodChangesChanged;

            nextButton.Clicked += LoadNextPage;
            backButton.Clicked += LoadPreviousPage;

            amountOfMoodChangesEntry.Text = lifeChartController.GetMoodSwings().ToString();
            moodSlider.Value = lifeChartController.GetMoodRating();
        }

        private void LoadNextPage(object sender, EventArgs e)
        {
            LCPageController.NextPage();
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            LCPageController.PreviousPage();
        }

        private void OnSliderValueChanged(object sender, ValueChangedEventArgs e)
        {
            double newStep = Math.Round(e.NewValue / stepValue);
            newStep = newStep * stepValue;
            moodSlider.Value = newStep;

            lifeChartController.SetMoodRating(Convert.ToInt32(newStep));
        }

        private void OnAmountOfMoodChangesChanged(object sender, TextChangedEventArgs e)
        {
            int.TryParse(e.NewTextValue, out int amountOfMoodChanges);

            lifeChartController.SetMoodSwings(amountOfMoodChanges);
        }
    }
}