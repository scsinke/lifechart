﻿using LifeChartApp.Interfaces;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages.LifeChartText
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LCYearOverview : ContentPage
	{
        private ILCTextOverviewController controller;

        public LCYearOverview (ILCTextOverviewController controller, List<int> yearList)
		{
            InitializeComponent();
            this.controller = controller;
		
            YearList.ItemsSource = yearList;
            YearList.ItemTapped += YearList_ItemTapped;
        }

        private void YearList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            controller.LoadNextPage(int.Parse(e.Item.ToString()));
        }
	}
}