﻿using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages.LifeChartText
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LCDayOverview : ContentPage
	{
        private ILCTextOverviewController controller;
        private List<CompleteLifeChart> completeLifeCharts;

        public LCDayOverview (ILCTextOverviewController controller, List<CompleteLifeChart> completeLifeCharts)
		{
			InitializeComponent ();
            this.controller = controller;
            this.completeLifeCharts = completeLifeCharts;

            DayList.ItemsSource = GetAvailableDays();
            DayList.ItemTapped += DayList_ItemTapped;
            backButton.Clicked += LoadPreviousPage;
        }

        private List<string> GetAvailableDays()
        {
            List<string> dayListToReturn = new List<string>();
            foreach (CompleteLifeChart lc in completeLifeCharts)
            {
                string monthString = " " + UpperCaseFirst(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(lc.LifeChart.Date.Month));
                string currentDay = lc.LifeChart.Date.Day.ToString() + monthString;
                if (!dayListToReturn.Any(day => day == currentDay))
                    dayListToReturn.Add(currentDay);
            }

            return dayListToReturn;
        }

        private void DayList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            int dayNumber = ExtractNumbers(e.Item.ToString());
            controller.LoadNextPage(dayNumber);
        }

        private int ExtractNumbers(string input)
        {
            string pattern = @"\d";

            StringBuilder output = new StringBuilder();

            foreach (Match m in Regex.Matches(input, pattern))
            {
                output.Append(m);
            }
            return int.Parse(output.ToString());
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            controller.LoadPreviousPage();
        }

        private string UpperCaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}