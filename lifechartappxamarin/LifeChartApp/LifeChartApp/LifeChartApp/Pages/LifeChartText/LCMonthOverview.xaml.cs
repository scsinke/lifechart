﻿using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages.LifeChartText
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LCMonthOverview : ContentPage
	{
        private ILCTextOverviewController controller;
        private List<CompleteLifeChart> completeLifeCharts;

        public LCMonthOverview (ILCTextOverviewController controller, List<CompleteLifeChart> completeLifeCharts, int Year)
		{
			InitializeComponent ();
            this.controller = controller;
            this.completeLifeCharts = completeLifeCharts.Where(lc => lc.LifeChart.Date.Year == Year).ToList();

            YearText.Text = Year.ToString();
            MonthList.ItemsSource = GetAvailableMonths();
            backButton.Clicked += LoadPreviousPage;
            MonthList.ItemTapped += MonthList_ItemTapped;
        }

        private List<string> GetAvailableMonths()
        {
            List<string> monthListToReturn = new List<string>();
            foreach (CompleteLifeChart lc in completeLifeCharts)
            {
                string currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(lc.LifeChart.Date.Month);
                if (!monthListToReturn.Any(month => month == UpperCaseFirst(currentMonth)))
                    monthListToReturn.Add(UpperCaseFirst(currentMonth));
            }

            return monthListToReturn;
        }

        private void MonthList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            string currentMonth = e.Item.ToString().ToLower();
            int month = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames.ToList().IndexOf(currentMonth) + 1;
            controller.LoadNextPage(month);
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            controller.LoadFirstPage();
        }

        private string UpperCaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}