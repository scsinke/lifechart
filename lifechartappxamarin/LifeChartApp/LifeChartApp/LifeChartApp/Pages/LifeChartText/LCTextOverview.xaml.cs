﻿using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using LifeChartApp.Utils;
using LifeChartApp.Constants;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Dao;

namespace LifeChartApp.Pages.LifeChartText
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LCTextOverview : ContentPage
	{
        private ILCTextOverviewController controller;
        private CompleteLifeChart completeLifeChart;

        public LCTextOverview (ILCTextOverviewController controller, CompleteLifeChart completeLifeChart)
		{
			InitializeComponent ();

            this.controller = controller;
            this.completeLifeChart = completeLifeChart;
            LifeChartData.BindingContext = completeLifeChart.LifeChart;

            InitText();
            InitMedicineList();
            InitLifeEventList();

            backButton.Clicked += LoadPreviousPage;
        }

        private void InitText()
        {
            LifeChart lifeChart = completeLifeChart.LifeChart;
            MoodType currentMood;
            Enum.TryParse(lifeChart.Mood, out currentMood);
            lcMood.Text = MoodTypeHelper.GetDescription(currentMood);

            IStrings stringsNL = StringsNL.Instance;

            if (lifeChart.UnrestrainedMania)
                UnrestrainedMania.Text = stringsNL.Yes;
            else
                UnrestrainedMania.Text = stringsNL.No;

            if (lifeChart.Menstruated)
                Menstruated.Text = stringsNL.Yes;
            else
                Menstruated.Text = stringsNL.No;

            if(lifeChart.Uploaded)
                Uploaded.Text = stringsNL.Yes;
            else
                Uploaded.Text = stringsNL.No;

            weightText.Text = lifeChart.Weight.ToString() + " " + stringsNL.Kilogram;
            sleepText.Text = lifeChart.Sleep.ToString() + " " + stringsNL.Hours;
            moodSwingText.Text = lifeChart.MoodSwings.ToString() + "x";
        }

        private void InitMedicineList()
        {
            IMedicineDao medicineDao = MedicineDao.Instance;

            medicineView.ItemsSource = completeLifeChart.Medicines;
        }

        private void InitLifeEventList()
        {
            ILifeEventDao lifeEventDao = LifeEventDao.Instance;

            lifeEventView.ItemsSource = completeLifeChart.LifeEvents;
        }

        private void LoadPreviousPage(object sender, EventArgs e)
        {
            controller.LoadPreviousPage();
        }

        private string UpperCaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}