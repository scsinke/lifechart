﻿using System;
using Xamarin.Forms;
using LifeChartApp.Model;
using LifeChartApp.Constants;
using LifeChartApp.Dao;
using System.Globalization;

namespace LifeChartApp.Pages
{
    public partial class ManagePersonalInfoPage : ContentPage
    {
        private ContentView placeHolder;
        private IPersonDao personDao;
        private ILifeChartDao lifeChartDao;
        private IMedicineDao medicineDao;
        private Person person;

        public ManagePersonalInfoPage(ContentView placeHolder, IPersonDao personDao, ILifeChartDao lifeChartDao, IMedicineDao medicineDao)
        {
            InitializeComponent();
            this.placeHolder = placeHolder;

            this.personDao = personDao;
            this.medicineDao = medicineDao;
            this.lifeChartDao = lifeChartDao;

            person = personDao.GetPerson(1);

            ConvertSexTypeEnum();
            DisplayPersonInfo();

            saveButton.Clicked += SaveButton_Clicked;
            cancelButton.Clicked += CancelButton_Clicked;
        }

        private void DisplayPersonInfo()
        {
            if (person != null)
            {
                if (person.FirstName != null)
                    firstnameEntry.Text = person.FirstName;

                if (person.LastName != null)
                    lastnameEntry.Text = person.LastName;

                if (person.Sex != null)
                {
                    Enum.TryParse(person.Sex, out SexType sex);
                    sexPicker.SelectedIndex = (int)sex;
                }
                if (person.Weight > 0)
                {
                    weightEntry.Text = person.Weight.ToString(new CultureInfo("en-US"));
                }
            }
        }

        private void ConvertSexTypeEnum()
        {
            var sexTypes = Enum.GetValues(typeof(SexType));

            sexPicker.BatchBegin();
            foreach (Enum value in sexTypes)
            {
                sexPicker.Items.Add(SexTypeHelper.GetDescription((SexType)value));
            }
            sexPicker.BatchCommit();
        }

        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            if (firstnameEntry != null)
                person.FirstName = firstnameEntry.Text;

            if (lastnameEntry != null)
                person.LastName = lastnameEntry.Text;
            float weight = GetWeightFloat(weightEntry.Text);
            if (weightEntry != null && weight > 0)
            {
                person.Weight = weight;
            }

            if (sexPicker.SelectedItem != null)
            {
                SexType sexType = (SexType)sexPicker.SelectedIndex;

                person.Sex = sexType.ToString();

                personDao.UpdatePerson(person);

                LeavePage();
            }
        }

        private float GetWeightFloat(string input)
        {
            float weight;
            if (input.Contains("."))
            {
                string[] weightWithoutDecimal = input.Split('.');
                string stringAfterDecimal = weightWithoutDecimal[1];
                if (!string.IsNullOrEmpty(stringAfterDecimal))
                {
                    double numberAfterDecimal = double.Parse(stringAfterDecimal) * Math.Pow(0.1F, stringAfterDecimal.Length);
                    weight = float.Parse(weightWithoutDecimal[0]) + float.Parse(numberAfterDecimal.ToString());
                }
                else
                    float.TryParse(input, out weight);
            }
            else
                float.TryParse(input, out weight);
            return weight;
        }

        private void CancelButton_Clicked(object sender, EventArgs e)
        {
            LeavePage();
        }

        private void LeavePage()
        {
            var page = new ProfilePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }
    }
}
