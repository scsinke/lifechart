﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LifeChartApp.Model;
using System.Collections.Generic;
using System.Linq;
using LifeChartApp.Constants;
using LifeChartApp.Dao;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddDefaultMedicinePage : ContentPage
    {
        private ContentView placeHolder;
        private IPersonDao personDao;
        private ILifeChartDao lifeChartDao;
        private IMedicineDao medicineDao;
        private MedicineController mcInstance;

        public AddDefaultMedicinePage(ContentView placeHolder, IPersonDao personDao, ILifeChartDao lifeChartDao, IMedicineDao medicineDao)
        {
            InitializeComponent();
            this.placeHolder = placeHolder;
            mcInstance = MedicineController.GetInstance();

            this.personDao = personDao;
            this.medicineDao = medicineDao;
            this.lifeChartDao = lifeChartDao;

            MedicineName.TextChanged += MedicineName_TextChanged;
            Dose.TextChanged += Dose_TextChanged;
            AmountPerDay.TextChanged += Amount_TextChanged;

            cancelButton.Clicked += CancelButton_Clicked;
            saveButton.Clicked += SaveButton_Clicked;
        }

        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            String medName = MedicineName.Text;
            int medDose = int.Parse(Dose.Text);
            int medAmount = int.Parse(AmountPerDay.Text);

            DefaultMedicine med = new DefaultMedicine()
            {
                DoseInMg = medDose,
                AmountPerDay = medAmount,
                MedicineName = medName
            };

            if (SaveMedList(med))
            {
                LeavePage();
            }
        }

        private bool SaveMedList(DefaultMedicine med)
        {
            IStrings stringsNL = StringsNL.Instance;

            if (med.DoseInMg == 0 || med.AmountPerDay == 0) {
                DisplayAlert(stringsNL.FailedToAdd, stringsNL.DoseOrIntakeCannotBeZero, stringsNL.Ok);
                return false;
            }

            List<DefaultMedicine> medList = mcInstance.GetAllDefaultMeds();

            DefaultMedicine matchingMed = medList.FirstOrDefault(findMed => findMed.MedicineName == med.MedicineName && findMed.DoseInMg == med.DoseInMg);

            if (matchingMed == null)
            {
                medicineDao.InsertDefaultMedicine(med);
                return true;
            }
            else
            {
                DisplayAlert(stringsNL.FailedToAdd, stringsNL.DuplicateMedFound, stringsNL.Ok);
                return false;
            }
        }

        private void Amount_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
        }

        private void Dose_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
        }

        private void MedicineName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
        }

        private void CancelButton_Clicked(object sender, System.EventArgs e)
        {
            LeavePage();
        }

        private void CheckInputFields()
        {
            if (!String.IsNullOrEmpty(MedicineName.Text) && !String.IsNullOrEmpty(Dose.Text)
                && !String.IsNullOrEmpty(AmountPerDay.Text))
                saveButton.IsEnabled = true;
            else
                saveButton.IsEnabled = false;
        }

        private void LeavePage()
        {
            var page = new ManageMedicinePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }
    }
}