﻿using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditDefaultMedicinePage : ContentPage
    {
        private ContentView placeHolder;
        private IPersonDao personDao;
        private ILifeChartDao lifeChartDao;
        private IMedicineDao medicineDao;
        private MedicineController medController;

        private DefaultMedicine medicine;
        private int medIndex;

        public EditDefaultMedicinePage(ContentView placeHolder, IPersonDao personDao, ILifeChartDao lifeChartDao, IMedicineDao medicineDao, DefaultMedicine med)
        {
            InitializeComponent();
            this.placeHolder = placeHolder;

            this.personDao = personDao;
            this.medicineDao = medicineDao;
            this.lifeChartDao = lifeChartDao;

            medController = MedicineController.GetInstance();
            List<DefaultMedicine> medicineList = medController.GetAllDefaultMeds();
            medIndex = medicineList.FindIndex(m => m.MedicineId == med.MedicineId);

            medicine = med;

            BindValuesTolabels();

            MedicineName.TextChanged += OnMedicineNameChanged;
            Dose.TextChanged += OnDoseChanged;
            Amount.TextChanged += OnAmountPerDayChanged;

            deleteButton.Clicked += OnDeleteButtonClickedAsync;
            editButton.Clicked += OnEditButtonClicked;
        }

        private void BindValuesTolabels()
        {
            if (!String.IsNullOrEmpty(medicine.MedicineName))
            {
                MedicineName.Text = medicine.MedicineName;
            }

            Dose.Text = medicine.DoseInMg.ToString();
            Amount.Text = medicine.AmountPerDay.ToString();
        }

        private void OnAmountPerDayChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            if (int.TryParse(e.NewTextValue, out int amountPerDay))
                medicine.AmountPerDay = amountPerDay;
        }

        private void OnDoseChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            if (int.TryParse(e.NewTextValue, out int dose))
                medicine.DoseInMg = dose;
        }

        private void OnMedicineNameChanged(object sender, TextChangedEventArgs e)
        {
            CheckInputFields();
            medicine.MedicineName = e.NewTextValue;
        }

        private void CheckInputFields()
        {
            if (!String.IsNullOrEmpty(MedicineName.Text) && !String.IsNullOrEmpty(Dose.Text)
                && !String.IsNullOrEmpty(Amount.Text))
                editButton.IsEnabled = true;
            else
                editButton.IsEnabled = false;
        }

        private async void OnDeleteButtonClickedAsync(object sender, EventArgs e)
        {
            IStrings stringsNL = StringsNL.Instance;

            var result = await DisplayAlert(stringsNL.WatchOut, stringsNL.DeleteMedMessage, stringsNL.Yes, stringsNL.No);

            if (result)
            {
                medicineDao.DeleteDefaultMedicine(medicine);
                LeavePage();
            }
        }

        private void OnEditButtonClicked(object sender, EventArgs e)
        {
            if (medicine.MedicineName != null && medicine.DoseInMg > 0 && medicine.AmountPerDay > 0)
            {
                List<DefaultMedicine> medicineList = medController.GetAllDefaultMeds();
                medicineList[medIndex] = medicine;

                int matchingMedicines = medicineList.Count(med => med.DoseInMg == medicine.DoseInMg && med.MedicineName == medicine.MedicineName);

                if (matchingMedicines <= 1)
                {
                    medController.SetCurrentDefaultMedicineList(medicineList);
                    medController.UpdateDefaultMedicineListToDb();
                    LeavePage();
                }
                else
                {
                    IStrings stringsNL = StringsNL.Instance;

                    DisplayAlert(stringsNL.WatchOut, stringsNL.DuplicateMedFound, stringsNL.Ok);
                }
            }
        }

        private void LeavePage()
        {
            var page = new ManageMedicinePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }
    }
}