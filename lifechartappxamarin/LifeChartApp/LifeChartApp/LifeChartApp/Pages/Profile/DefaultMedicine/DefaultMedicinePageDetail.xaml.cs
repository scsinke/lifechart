﻿using LifeChartApp.Constants;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DefaultMedicinePageDetail : ContentPage
	{
        private DefaultMedicine medicine;
        private MedicineController medicineController;

        private ILCPageController LCPageController;

        public DefaultMedicinePageDetail(ILCPageController LCPageController, DefaultMedicine med)
        {
            InitializeComponent();
            medicineController = MedicineController.GetInstance();

            this.LCPageController = LCPageController;
            this.medicine = med;

            editButton.Clicked += EditButton_Clicked;
            deleteButton.Clicked += DeleteButton_ClickedAsync;

            BindValuesTolabels();
        }

        private void BindValuesTolabels()
        {
            if (!String.IsNullOrEmpty(medicine.MedicineName))
            {
                MedicineName.Text = medicine.MedicineName;
            }

            Dose.Text = medicine.DoseInMg.ToString();
            Amount.Text = medicine.AmountPerDay.ToString();

            MedicineName.TextChanged += MedicineName_TextChanged;
            Dose.TextChanged += Dose_TextChanged;
            Amount.TextChanged += Amount_TextChanged;
        }

        private void Amount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.NewTextValue))
            {
                medicine.AmountPerDay = Int32.Parse(e.NewTextValue);
            }
        }

        private void Dose_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.NewTextValue))
            {
                medicine.DoseInMg = Int32.Parse(e.NewTextValue);
            }
        }

        private void MedicineName_TextChanged(object sender, TextChangedEventArgs e)
        {
            medicine.MedicineName = e.NewTextValue;
        }

        private void GoBackToMedicinePage()
        {
            LCPageController.LeaveAddItemPage();
        }

        private async void DeleteButton_ClickedAsync(object sender, EventArgs e)
        {
            IStrings stringsNL = StringsNL.Instance;

            var result = await DisplayAlert(stringsNL.WatchOut, stringsNL.DeleteMedMessage, stringsNL.Yes, stringsNL.No);

            if (result)
            {
                RemoveMedicine();
                GoBackToMedicinePage();
            }
        }

        private void RemoveMedicine()
        {
            List<DefaultMedicine> medlist = medicineController.GetCurrentDefaultMedicineList();
            DefaultMedicine matchingMed = medlist.FirstOrDefault(med => med.MedicineName == medicine.MedicineName);

            if (matchingMed != null)
            {
                medlist.Remove(matchingMed);
            }

            medicineController.SetCurrentDefaultMedicineList(medlist);
        }

        private void EditButton_Clicked(object sender, EventArgs e)
        {
            List<DefaultMedicine> medList = medicineController.GetCurrentDefaultMedicineList();
            
            DefaultMedicine matchingMed = medList.FirstOrDefault(findMed => findMed.MedicineName == medicine.MedicineName && findMed.DoseInMg == medicine.DoseInMg);

            if (matchingMed != null)
            {
                medList.Remove(matchingMed);
                medList.Add(medicine);

                medicineController.SetCurrentDefaultMedicineList(medList);
            }
            GoBackToMedicinePage();
        }
    }
}