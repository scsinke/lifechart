﻿using LifeChartApp.Dao;
using LifeChartApp.Model;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ManageMedicinePage : ContentPage
	{
        private ContentView placeHolder;
        private IPersonDao personDao;
        private ILifeChartDao lifeChartDao;
        private IMedicineDao medicineDao;

        public ManageMedicinePage (ContentView placeHolder, IPersonDao personDao, ILifeChartDao lifeChartDao, IMedicineDao medicineDao)
		{
			InitializeComponent ();
            this.placeHolder = placeHolder;

            this.personDao = personDao;
            this.medicineDao = medicineDao;
            this.lifeChartDao = lifeChartDao;

            AddMedicinesList.ItemsSource = medicineDao.GetAllDefaultMedicines();
            AddMedicinesList.ItemTapped += AddMedicinesList_ItemTapped;
            backButton.Clicked += BackButton_Clicked;
        }

        private void BackButton_Clicked(object sender, EventArgs e)
        {
            var newPage = new ProfilePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = newPage.Content;
        }

        private void AddMedicinesList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            DefaultMedicine med = (DefaultMedicine)e.Item;

            var page = new EditDefaultMedicinePage(placeHolder, personDao, lifeChartDao, medicineDao, med);
            placeHolder.Content = page.Content;
        }

        public void AddMedicineToList(object sender, EventArgs e)
        {
            var page = new AddDefaultMedicinePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }
    }
}