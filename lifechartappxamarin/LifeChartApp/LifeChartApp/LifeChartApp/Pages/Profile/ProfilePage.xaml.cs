﻿using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Model;
using LifeChartApp.Utils;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LifeChartApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProfilePage : ContentPage
	{
        private ContentView placeHolder;
        private IPersonDao personDao;
        private ILifeChartDao lifeChartDao;
        private IMedicineDao medicineDao;
        private Person person;

        public ProfilePage (ContentView placeHolder, IPersonDao personDao, ILifeChartDao lifeChartDao, IMedicineDao medicineDao)
		{
			InitializeComponent ();
            this.placeHolder = placeHolder;
            this.personDao = personDao;
            this.medicineDao = medicineDao;
            this.lifeChartDao = lifeChartDao;

            medicationButton.Clicked += OpenManageMedicinePage;
            personalInfoButton.Clicked += OpenManagePersonalInfoPage;
            scanQRCodeButton.Clicked += ScanQRCodeButton_ClickedAsync;

            person = personDao.GetPerson(1);
            if(person != null)
                userFirstName.Text = person.FirstName;
        }

        private async void ScanQRCodeButton_ClickedAsync(object sender, EventArgs e)
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan();

            if (result != null)
            {
                IStrings stringsNL = StringsNL.Instance;

                UploadResponseType uploadResponse = UploadLifeChartToAPIAsync(result.Text);

                switch (uploadResponse)
                {
                    case UploadResponseType.SUCCESS:
                        await DisplayAlert(stringsNL.UploadSuccess, stringsNL.LifeChartUploadedMessage, stringsNL.Ok);
                        break;
                    case UploadResponseType.FAILED:
                        await DisplayAlert(stringsNL.UploadFailed, stringsNL.LifeChartNotUploadMessage, stringsNL.Ok);
                        break;
                    case UploadResponseType.NO_CONTENT:
                        await DisplayAlert(stringsNL.NoLifeChartsToUpload, stringsNL.NoLifeChartsNeedToUpload, stringsNL.Ok);
                        break;
                    default:
                        break;
                }
            }
        }

        private UploadResponseType UploadLifeChartToAPIAsync(string scannedToken)
        {
            List<LifeChartContainer> lifeCharts = lifeChartDao.GetNotUploadedLifeChartContainers();

            if (lifeCharts.Count > 0)
            {
                RestFunctions restFunctions = new RestFunctions();

                if(restFunctions.UploadLifeCharts(scannedToken, lifeCharts))
                {
                    SetLifeChartToUploaded(lifeCharts);
                    return UploadResponseType.SUCCESS;
                }
                else
                {
                    return UploadResponseType.FAILED;
                }
            }

            return UploadResponseType.NO_CONTENT;
        }

        private void SetLifeChartToUploaded(List<LifeChartContainer> lifeCharts)
        {
            foreach (LifeChartContainer lc in lifeCharts)
            {
                lifeChartDao.UpdateUploadedLifeChart(lc.LifeChartId, true);
            }
        }

        private void OpenManageMedicinePage(object sender, EventArgs e)
        {
            var page = new ManageMedicinePage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }

        private void OpenManagePersonalInfoPage(object sender, EventArgs e)
        {
            var page = new ManagePersonalInfoPage(placeHolder, personDao, lifeChartDao, medicineDao);
            placeHolder.Content = page.Content;
        }
    }
}