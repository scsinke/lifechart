﻿using LifeChartApp.Constants;
using LifeChartApp.Model;
using LifeChartApp.Requests;
using LifeChartApp.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace LifeChartApp
{
    public class RestFunctions
    {
        private readonly HttpClient client;
        private readonly Api api;

        public RestFunctions()
        {
            client = new HttpClient
            {
                Timeout = new TimeSpan(0, 0, 5)
            };

            api = new Api();
        }

        private HttpResponseMessage Post(Uri uri, HttpContent content)
        {
            Task<HttpResponseMessage> res = client.PostAsync(uri, content);

            Task.WaitAny(res);
            if (!res.IsCanceled)
            {
                return res.Result;
            }

            return new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.RequestTimeout,
                ReasonPhrase = "Hardcoded request timeout"
            };
        }

        public bool UploadLifeCharts(string scannedToken, List<LifeChartContainer> lifeCharts)
        {
            SaveLifeChartRequest req = new SaveLifeChartRequest
            {
                LifeCharts = lifeCharts
            };
                
            JsonContent c = new JsonContent(req);

            Uri uri = new Uri(api.URI_SAVE_LIFECHART + "?token="+ scannedToken);

            HttpResponseMessage response = Post(uri, c);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        } 
    }
}