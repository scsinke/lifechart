﻿using LifeChartApp.Model;
using System.Collections.Generic;

namespace LifeChartApp.Dao
{
    public interface IMedicineDao
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="medicines"></param>
        /// <param name="lifeChartId"></param>
        void InsertMedicines(List<Medicine> medicines, int lifeChartId);

        /// <summary>
        /// Inserts a default medicine
        /// </summary>
        /// <param name="defaultMedicine"></param>
        /// <returns></returns>
        int InsertDefaultMedicine(DefaultMedicine defaultMedicine);

        /// <summary>
        /// Gets all default medicines
        /// </summary>
        /// <returns>List of DefaultMedicine</returns>
        List<DefaultMedicine> GetAllDefaultMedicines();

        /// <summary>
        /// Gets a DefaultMedicine by id
        /// </summary>
        /// <param name="medicineId"></param>
        /// <returns>Default medicine by medicineId</returns>
        DefaultMedicine GetDefaultMedicine(int medicineId);

        /// <summary>
        /// Updates a DefaultMedicine
        /// </summary>
        /// <param name="medicine"></param>
        void UpdateDefaultMedicine(DefaultMedicine medicine);

        /// <summary>
        /// Deletes a DefaultMedicine
        /// </summary>
        /// <param name="medicine"></param>
        void DeleteDefaultMedicine(DefaultMedicine medicine);

        /// <summary>
        /// Gets all medicines by lifeChartId
        /// </summary>
        /// <param name="lifeChartId"></param>
        /// <returns>List of medicines</returns>
        List<Medicine> GetMedicinesFromLifeChartId(int lifeChartId);
    }
}