﻿using LifeChartApp.Model;
using System;
using System.Collections.Generic;

namespace LifeChartApp.Dao
{
    public interface ILifeChartDao
    {
        /// <summary>
        /// Inserts a complete lifechart in database. Calls life event and medicine dao passing the inserted lifechart id.
        /// </summary>
        /// <param name="completeLifeChart"></param>
        /// <param name="personId"></param>
        /// <returns>inserted LifeChart id</returns>
        int InsertCompleteLifeChart(CompleteLifeChart completeLifeChart, int personId);

        /// <summary>
        /// Get lifechart by id
        /// </summary>
        /// <param name="lifeChartId"></param>
        /// <returns>Complete LifeChart with matching id</returns>
        CompleteLifeChart GetCompleteLifeChart(int lifeChartId);

        /// <summary>
        /// Get list of all complete life charts
        /// </summary>
        /// <returns>list of LifeChart</returns>
        List<CompleteLifeChart> GetAllCompleteLifeCharts();

        /// <summary>
        /// Updates a LifeChart
        /// </summary>
        /// <param name="lifechart"></param>
        void UpdateLifeChart(LifeChart lifechart);

        /// <summary>
        /// Deletes a LifeCharts
        /// </summary>
        /// <param name="lifeChartId"></param>
        void DeleteLifeChart(int lifeChartId);

        /// <summary>
        /// Gets the most recent LifeChart by date
        /// </summary>
        /// <returns>latest LifeChart</returns>
        LifeChart GetLatestLifeChart();

        /// <summary>
        /// Gets all LifeCharts that haven't been uploaded yet
        /// </summary>
        /// <returns>list of LifeChartContainer</returns>
        List<LifeChartContainer> GetNotUploadedLifeChartContainers();

        /// <summary>
        /// Updates uploaded Lifecharts to upload state
        /// </summary>
        /// <param name="lifeChartId"></param>
        /// <param name="uploaded"></param>
        void UpdateUploadedLifeChart(int lifeChartId, bool uploaded);

        /// Gets all lifecharts between 2 dates
        /// </summary>
        /// <param name="startDate">first date</param>
        /// <param name="endDate">second date</param>
        /// <returns>List of complete Life Charts between start and enddate</returns>
        List<CompleteLifeChart> GetCompleteLifeChartsBetweenDates(DateTime startDate, DateTime endDate);
    }
}