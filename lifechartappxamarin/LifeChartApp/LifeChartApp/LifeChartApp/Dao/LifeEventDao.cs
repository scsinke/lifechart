﻿using System.Collections.Generic;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using System.Linq;

namespace LifeChartApp.Dao
{
    public class LifeEventDao : DatabaseProperties, ILifeEventDao
    {
        private static readonly object Lock = new object();
        private static LifeEventDao _instance;
        public static LifeEventDao Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new LifeEventDao());
                }
            }
        }

        private LifeEventDao()
        {
            Connection.CreateTable<LifeEvent>();
        }

        public void DeleteLifeEvent(int lifeEventId)
        {
            LifeEvent lifeEvent = Connection.Table<LifeEvent>().Where(life => life.EventId == lifeEventId).FirstOrDefault();

            if (lifeEvent != null)
            {
                Connection.Delete(lifeEvent);
            }
        }

        public List<LifeEvent> GetAllLifeEvents()
        {
            return Connection.Table<LifeEvent>().ToList();
        }

        public LifeEvent GetLifeEvent(int lifeEventId)
        {
            return Connection.Table<LifeEvent>().Where(life => life.EventId == lifeEventId).FirstOrDefault();
        }

        public int InsertLifeEvent(LifeEvent lifeEvent)
        {
            Connection.Insert(lifeEvent);
            return lifeEvent.EventId;
        }

        public void UpdateLifeEvent(LifeEvent lifeEvent)
        {
            Connection.Update(lifeEvent);
        }

        public List<LifeEvent> GetLifeEventsFromLifeChartId(int lifeChartId)
        {
            return Connection.Table<LifeEvent>().Where(le => le.LifeChartId == lifeChartId).ToList();
        }

        public void InsertLifeEvents(List<LifeEvent> lifeEvents, int lifeChartId)
        {
            foreach (LifeEvent lifeEvent in lifeEvents)
            {
                lifeEvent.LifeChartId = lifeChartId;
                Connection.Insert(lifeEvent);
            }
        }
    }
}