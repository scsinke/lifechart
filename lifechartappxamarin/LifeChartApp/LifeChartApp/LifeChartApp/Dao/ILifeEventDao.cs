﻿using LifeChartApp.Model;
using System.Collections.Generic;

namespace LifeChartApp.Dao
{
    public interface ILifeEventDao
    {
        /// <summary>
        /// Inserts a LifeEvent
        /// </summary>
        /// <param name="lifeEvent"></param>
        /// <returns>Id of insterted LifeEvent</returns>
        int InsertLifeEvent(LifeEvent lifeEvent);

        /// <summary>
        /// Get LifeEvent by id
        /// </summary>
        /// <param name="lifeEventId"></param>
        /// <returns>LifeEvent</returns>
        LifeEvent GetLifeEvent(int lifeEventId);

        /// <summary>
        /// Gets all LifeEvents
        /// </summary>
        /// <returns>List of LifeEvent</returns>
        List<LifeEvent> GetAllLifeEvents();

        /// <summary>
        /// Updates a LifeEvent
        /// </summary>
        /// <param name="lifeEvent"></param>
        void UpdateLifeEvent(LifeEvent lifeEvent);

        /// <summary>
        /// Deletes a LifeEvent
        /// </summary>
        /// <param name="lifeEventId"></param>
        void DeleteLifeEvent(int lifeEventId);

        /// <summary>
        /// Get all LifeEvents from a lifeChartId
        /// </summary>
        /// <param name="lifeChartId"></param>
        /// <returns>list of LifeEvents</returns>
        List<LifeEvent> GetLifeEventsFromLifeChartId(int lifeChartId);

        /// <summary>
        /// Inserts a list of LifeEvents matching a lifeChartId
        /// </summary>
        /// <param name="lifeEvents"></param>
        /// <param name="lifeChartId"></param>
        void InsertLifeEvents(List<LifeEvent> lifeEvents, int lifeChartId);
    }
}
