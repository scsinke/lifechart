﻿using SQLite;
using System;
using System.IO;

namespace LifeChartApp.Dao
{
    public class DatabaseProperties
    {
        private const string FileName = "LifeChart.db";
        protected readonly SQLiteConnection Connection;

#if __IOS__
        private static readonly string DocumentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
        private static readonly string LibraryPath = Path.Combine (DocumentsPath, "..", "Library"); // Library folder
        private static readonly string dbPath = Path.Combine(LibraryPath, FileName);
#elif __ANDROID__
        protected static readonly string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Xamarin.Droid.Assets." + FileName);
#endif

        public DatabaseProperties()
        {
            Connection = new SQLiteConnection(dbPath);
        }

    }
}