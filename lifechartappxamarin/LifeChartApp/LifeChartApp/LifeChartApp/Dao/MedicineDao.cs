﻿using System.Collections.Generic;
using LifeChartApp.Model;
using System.Linq;

namespace LifeChartApp.Dao
{
    public class MedicineDao : DatabaseProperties, IMedicineDao
    {
        private static readonly object Lock = new object();
        private static MedicineDao _instance;
        public static MedicineDao Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new MedicineDao());
                }
            }
        }

        private MedicineDao()
        {
            Connection.CreateTable<Medicine>();
            Connection.CreateTable<DefaultMedicine>();
        }

        public void DeleteDefaultMedicine(DefaultMedicine medicine)
        {
            Connection.Delete(medicine);
        }

        public List<DefaultMedicine> GetAllDefaultMedicines()
        {
            List<DefaultMedicine> medList = Connection.Table<DefaultMedicine>().ToList();
            if (medList == null)
                medList = new List<DefaultMedicine>();

            return medList;
        }

        public DefaultMedicine GetDefaultMedicine(int medicineId)
        {
            return Connection.Table<DefaultMedicine>().Where(med => med.MedicineId == medicineId).FirstOrDefault();
        }

        public int InsertDefaultMedicine(DefaultMedicine defaultMedicine)
        {
            Connection.Insert(defaultMedicine);

            return defaultMedicine.MedicineId;
        }

        public void InsertMedicines(List<Medicine> medicines, int lifeChartId)
        {
            foreach (Medicine medicine in medicines)
            {
                medicine.LifeChartId = lifeChartId;

                InsertMedicine(medicine);
            }
        }

        private void InsertMedicine(Medicine medicine)
        {
            Connection.Insert(medicine);
        }

        public void UpdateDefaultMedicine(DefaultMedicine medicine)
        {
            Connection.Update(medicine);
        }

        public List<Medicine> GetMedicinesFromLifeChartId(int lifeChartId)
        {
            return Connection.Table<Medicine>().Where(med => med.LifeChartId == lifeChartId).ToList();
        }
    }
}