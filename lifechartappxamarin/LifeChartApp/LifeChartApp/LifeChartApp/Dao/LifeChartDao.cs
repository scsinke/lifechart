﻿using System.Collections.Generic;
using LifeChartApp.Model;
using System.Linq;
using System;

namespace LifeChartApp.Dao
{
    public class LifeChartDao : DatabaseProperties, ILifeChartDao
    {
        private readonly ILifeEventDao lifeEventDao;
        private readonly IMedicineDao medicineDao;

        private static readonly object Lock = new object();
        private static LifeChartDao _instance;
        public static LifeChartDao Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new LifeChartDao());
                }
            }
        }

        private LifeChartDao()
        {
            lifeEventDao = LifeEventDao.Instance;
            medicineDao = MedicineDao.Instance;

            Connection.CreateTable<LifeChart>();
        }

        public void DeleteLifeChart(int lifeChartId)
        {
            LifeChart lifeChart = Connection.Table<LifeChart>().Where(lc => lc.LifeChartId == lifeChartId).FirstOrDefault();

            if (lifeChart != null)
            {
                Connection.Delete(lifeChart);
            }
        }

        public List<CompleteLifeChart> GetAllCompleteLifeCharts()
        {
            List<LifeChart> lifeChartList = Connection.Table<LifeChart>().OrderByDescending(lc => lc.Date).ToList();
            List<CompleteLifeChart> completeLifeList = new List<CompleteLifeChart>();

            foreach(LifeChart lc in lifeChartList)
            {
                CompleteLifeChart cl = new CompleteLifeChart()
                {
                    LifeChart = lc,
                    Medicines = medicineDao.GetMedicinesFromLifeChartId(lc.LifeChartId),
                    LifeEvents = lifeEventDao.GetLifeEventsFromLifeChartId(lc.LifeChartId)
                };

                completeLifeList.Add(cl);
            }

            return completeLifeList;
        }

        public LifeChart GetLatestLifeChart()
        {
            return Connection.Table<LifeChart>().OrderByDescending(life => life.Date).FirstOrDefault();
        }

        public CompleteLifeChart GetCompleteLifeChart(int lifeChartId)
        {
            CompleteLifeChart completeLifeChart = new CompleteLifeChart()
            {
                LifeChart = Connection.Table<LifeChart>().Where(lc => lc.LifeChartId == lifeChartId).FirstOrDefault(),
                Medicines = medicineDao.GetMedicinesFromLifeChartId(lifeChartId),
                LifeEvents = lifeEventDao.GetLifeEventsFromLifeChartId(lifeChartId)
            };

            return completeLifeChart;
            
        }

        public List<LifeChartContainer> GetNotUploadedLifeChartContainers()
        {
            List<LifeChart> lifeCharts = Connection.Table<LifeChart>().Where(lc => !lc.Uploaded).ToList();

            List<LifeChartContainer> lifechartContainers = new List<LifeChartContainer>();

            foreach (LifeChart lc in lifeCharts)
            {
                lifechartContainers.Add(GenerateLifeChartContainer(lc));
            }

            return lifechartContainers;
        }

        private LifeChartContainer GenerateLifeChartContainer(LifeChart lc)
        {
            List<Medicine> medicineList = medicineDao.GetMedicinesFromLifeChartId(lc.LifeChartId);
            List<LifeEvent> lifeEventList = lifeEventDao.GetLifeEventsFromLifeChartId(lc.LifeChartId);

            string stringDate = lc.Date.ToString("yyyy-MM-dd");

            LifeChartContainer lifeChartContainer = new LifeChartContainer()
            {
                LifeChartId = lc.LifeChartId,
                Complaints = lc.Complaints,
                Menstruated = lc.Menstruated,
                Mood = lc.Mood,
                MoodRating = lc.MoodRating,
                MoodSwings = lc.MoodSwings,
                SeverityOfEpisode = lc.SeverityOfEpisode,
                Sleep = lc.Sleep,
                UnrestrainedMania = lc.UnrestrainedMania,
                Weight = lc.Weight,
                Date = stringDate,

                Medicines = medicineList,
                LifeEvents = lifeEventList
            };

            return lifeChartContainer;
        }

        public int InsertCompleteLifeChart(CompleteLifeChart completeLifeChart, int personId)
        {
            int insertResult = Connection.Insert(completeLifeChart.LifeChart);

            if (insertResult > 0) {
                completeLifeChart.LifeChart.PersonId = personId;

                if (completeLifeChart.LifeEvents != null)
                    lifeEventDao.InsertLifeEvents(completeLifeChart.LifeEvents, completeLifeChart.LifeChart.LifeChartId);

                if(completeLifeChart.Medicines != null)
                    medicineDao.InsertMedicines(completeLifeChart.Medicines, completeLifeChart.LifeChart.LifeChartId);

                return completeLifeChart.LifeChart.LifeChartId;
            }
            else{
                return -1;
            }
        }

        public void UpdateLifeChart(LifeChart lifechart)
        {
            Connection.Update(lifechart);
        }

        public void UpdateUploadedLifeChart(int lifeChartId, bool uploaded)
        {
            LifeChart lifeChart = Connection.Table<LifeChart>().Where(lc => lc.LifeChartId == lifeChartId).FirstOrDefault();

            if (lifeChart != null)
            {
                lifeChart.Uploaded = uploaded;
                Connection.Update(lifeChart);
            }
        }

        public List<CompleteLifeChart> GetCompleteLifeChartsBetweenDates(DateTime startDate, DateTime endDate)
        {
            List<CompleteLifeChart> lifeChartList = GetAllCompleteLifeCharts();

            return lifeChartList.Where(lc => lc.LifeChart.Date >= startDate && lc.LifeChart.Date <= endDate).ToList();
        }
    }
}