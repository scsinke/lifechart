﻿using LifeChartApp.Model;

namespace LifeChartApp.Dao
{
    public interface IPersonDao
    {
        /// <summary>
        /// Inserts a Person
        /// </summary>
        /// <param name="person"></param>
        /// <returns>inserted id</returns>
        int InsertPerson(Person person);

        /// <summary>
        /// Gets a person by personId
        /// </summary>
        /// <param name="personId"></param>
        /// <returns>Person</returns>
        Person GetPerson(int personId);

        /// <summary>
        /// Updates a person
        /// </summary>
        /// <param name="person"></param>
        void UpdatePerson(Person person);

        /// <summary>
        /// Deletes a person by personId
        /// </summary>
        /// <param name="personId"></param>
        void DeletePerson(int personId);
    }
}