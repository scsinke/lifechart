﻿using LifeChartApp.Interfaces;
using LifeChartApp.Model;

namespace LifeChartApp.Dao
{
    public class PersonDao : DatabaseProperties, IPersonDao
    {
        private static readonly object Lock = new object();
        private static PersonDao _instance;
        public static PersonDao Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new PersonDao());
                }
            }
        }

        private PersonDao()
        {
            Connection.CreateTable<Person>();
        }

        public int InsertPerson(Person person)
        {
            int newId = Connection.Table<Person>().Count();

            if (newId <= 1)
            {
                person.PersonId = newId;
                Connection.Insert(person);
                return person.PersonId;
            }
            else
            {
                return -1;
            }
        }

        public Person GetPerson(int personId)
        {
            return Connection.Table<Person>().Where(per => per.PersonId == personId).FirstOrDefault();
        }

        public void UpdatePerson(Person person)
        {
            Connection.Update(person);
        }

        public void DeletePerson(int personId)
        {
            Person person = Connection.Table<Person>().Where(per => per.PersonId == personId).FirstOrDefault();

            if (person != null)
            {
                Connection.Delete(person);
            }
        }
    }
}