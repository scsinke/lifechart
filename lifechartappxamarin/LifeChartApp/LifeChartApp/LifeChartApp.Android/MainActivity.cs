﻿using System;
using System.Globalization;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using ZXing.Mobile;
using Android.Content;
using LifechartApp.Broadcast;

namespace LifeChartApp.Droid
{
    [Activity(Label = "LifeChartApp", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            MobileBarcodeScanner.Initialize(Application);

            StartAlarmForNotifications();

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new LifeChartApp.App());
        }

        private void StartAlarmForNotifications()
        {
            AlarmManager manager = (AlarmManager)GetSystemService(AlarmService);
            Intent myIntent;
            PendingIntent pendingIntent;

            myIntent = new Intent(this, typeof(AlarmNotificationReceiver));
            pendingIntent = PendingIntent.GetBroadcast(this, 0, myIntent, 0);

            //Alarm beeing sent on 14:00 each day
            TimeSpan time = new TimeSpan(14, 00, 00);
            long timeInMillis = (long)time.TotalMilliseconds;

            manager.SetInexactRepeating(AlarmType.Rtc, timeInMillis, AlarmManager.IntervalDay, pendingIntent);
        }
    }
}