﻿using LifeChartApp;
using LifeChartApp.Constants;
using LifeChartApp.Dao;
using LifeChartApp.Model;
using Moq;
using NUnit.Framework;
using Xamarin.Forms;

namespace UnitTest
{
    [TestFixture]
    public class LCPageControllerTest
    {
        private LCPageController controller;
        private ILifeChartDao lifeChartDao;

        [SetUp]
        public void Init()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            lifeChartDao = LifeChartDao.Instance;
            Person person = new Person()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                PersonId = 1,
                Weight = 65,
                Sex = SexType.MALE.ToString()
            };
            Mock<IPersonDao> personDao = new Mock<IPersonDao>();
            personDao.Setup(p => p.GetPerson(1)).Returns(person);
            MainPage mainPage = new MainPage();
            controller = new LCPageController(mainPage, new ContentView(), lifeChartDao)
            {
                PersonDao = personDao.Object
            };
        }

        [TestCase]
        public void InitializePagesTest_ShouldMakePagesList()
        {
            //setup
            int amountOfPages = 9;
            //test

            //verify
            Assert.AreEqual(controller.Pages.Count, amountOfPages);
        }

        [TestCase]
        public void NextPageIndex_ShouldIncreasePageIndex()
        {
            //setup
            controller.NextPage(); //Makes the index 0
            int firstPageIndex = 0;
            int secondPageIndex = firstPageIndex + 1;
            //test
            controller.NextPage();
            //verify
            Assert.AreEqual(controller.CurrentPageIndex, secondPageIndex);
        }

        [TestCase]
        public void NextPageContentTest_MainContentViewShouldNotBeNull()
        {
            //setup
            int secondPageIndex = 0;
            //test
            controller.NextPage();
            //verify
            Assert.AreEqual(controller.MainContentView.Content, controller.Pages[secondPageIndex].Content);
        }

        [TestCase]
        public void PreviousPageIndexTest_ShouldMakeCurrentPageIndexMinusOne()
        {
            //setup
            controller.NextPage(); //Makes the index 0
            int firstPageIndex = 0;
            controller.NextPage();
            //test
            controller.PreviousPage();
            //verify
            Assert.AreEqual(controller.CurrentPageIndex, firstPageIndex);
        }

        [TestCase]
        public void PreviousPageContentTest_MainContentViewShouldEqualPageInThePageslistWithTheCorrespondingIndex()
        {
            //setup
            controller.NextPage(); //Makes the index 0
            int firstPageIndex = 0;
            controller.NextPage();
            //test
            controller.PreviousPage();
            //verify
            Assert.AreEqual(controller.MainContentView.Content, controller.Pages[firstPageIndex].Content);
        }

        [TestCase]
        public void EndPageTest_MainContentViewShouldNotBeNull()
        {
            //setup

            //test
            controller.EndPage();
            //verify
            Assert.IsNotNull(controller.MainContentView.Content);
        }

        [TestCase]
        public void LoadDefaultMedicinePageTest_MainContentViewShouldNotBeNull()
        {
            //setup
            
            DefaultMedicine dMed = new DefaultMedicine();
            //test
            controller.LoadDefaultMedicinePage(dMed);
            //verify
            Assert.IsNotNull(controller.MainContentView.Content);
        }

        [TestCase]
        public void LeaveAddObjectPageTest_MainContentViewShouldEqualPageContentOnCurrentPageIndex()
        {
            //setup
            int firstPageIndex = 0;
            DefaultMedicine dMed = new DefaultMedicine();
            controller.NextPage();
            controller.LoadDefaultMedicinePage(dMed);
            //test
            controller.LeaveAddItemPage();
            //verify
            Assert.AreEqual(controller.MainContentView.Content, controller.Pages[firstPageIndex].Content);
        }

        [TestCase]
        public void LoadAddMedicinePageTest_MainContentViewShouldNotBeNull()
        {
            //setup
            Medicine medMock = new Medicine();
            //test
            controller.LoadAddMedicinePage(medMock);
            //verify
            Assert.IsNotNull(controller.MainContentView.Content);
        }

        [TestCase]
        public void LoadAddMedicineToListPageTest_MainContentViewShouldNotBeNull()
        {
            //setup

            //test
            controller.LoadAddMedicineToListPage();
            //verify
            Assert.IsNotNull(controller.MainContentView.Content);
        }

        [TestCase]
        public void LoadAddLifeEventPageTest_MainContentViewShouldNotBeNull()
        {
            //setup

            //test
            controller.LoadAddLifeEventPage();
            //verify
            Assert.IsNotNull(controller.MainContentView.Content);
        }

        [TestCase]
        public void LoadLifeEventDetailsTest_MainContentViewShouldNotBeNull()
        {
            //setup
            LifeEvent lifeMock = new LifeEvent();

            //test
            controller.LoadLifeEventDetails(lifeMock);
            //verify
            Assert.IsNotNull(controller.MainContentView.Content);
        }

        [TestCase]
        public void LoadPageWithIndexTest_IndexesShouldMatch()
        {
            //setup
            int expectedIndex = 4;
            //test
            controller.LoadPageWithIndex(expectedIndex);
            //verify
            Assert.AreEqual(controller.CurrentPageIndex, expectedIndex);
        }
    }
}