﻿using LifeChartApp.Model;
using NUnit.Framework;

namespace UnitTest.Model
{
    [TestFixture]
    public class LifeChartControllerTest
    {
        private LifeChartController controller;

        [SetUp]
        public void Init()
        {
            controller = LifeChartController.GetInstance();
        }

        [TestCase]
        public void GetInstance()
        {
            //setup
            LifeChartController controllerLocal = LifeChartController.GetInstance();

            //test

            //verify
            Assert.IsInstanceOf(typeof(LifeChartController), controllerLocal);
        }
    }
}