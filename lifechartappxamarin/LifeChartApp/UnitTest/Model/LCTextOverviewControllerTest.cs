﻿using LifeChartApp.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace UnitTest
{
    [TestFixture]
    public class LCTextOverviewControllerTest
    {
        private LCTextOverviewController controller;
        private LCTextOverviewController emptyController;
        private LCTextOverviewController multipleYearController;
        private List<CompleteLifeChart> lifeCharts = new List<CompleteLifeChart>();

        [SetUp]
        public void Init()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            LifeChart lifeChart = new LifeChart()
            {
                LifeChartId = 1,
                Date = DateTime.Now
            };

            CompleteLifeChart completeLifeChart = new CompleteLifeChart()
            {
                LifeChart = lifeChart
            };

            lifeCharts = new List<CompleteLifeChart>()
            {
                completeLifeChart
            };

            controller = new LCTextOverviewController(new ContentView(), lifeCharts);
            emptyController = new LCTextOverviewController(new ContentView(), new List<CompleteLifeChart>());

            LifeChart lifeChart2 = new LifeChart()
            {
                LifeChartId = 1,
                Date = DateTime.Now.AddYears(-1)
            };

            CompleteLifeChart completeLifeChart2 = new CompleteLifeChart()
            {
                LifeChart = lifeChart2
            };

            List<CompleteLifeChart> multipleYearsLifeCharts = new List<CompleteLifeChart>()
            {
                completeLifeChart,
                completeLifeChart2
            };

            multipleYearController = new LCTextOverviewController(new ContentView(), multipleYearsLifeCharts);
        }

        [TestCase]
        public void LoadFirstPageIndexTest_ShouldSetIndexToOne()
        {
            //setup
            int expectedPageIndex = 1;
            //test
            controller.LoadFirstPage();
            //verify
            Assert.AreEqual(controller.PageIndex, expectedPageIndex);
        }

        [TestCase]
        public void LoadFirstPageContentTest_ShouldSetCurrentPage()
        {
            //setup

            //test
            controller.LoadFirstPage();
            //verify
            Assert.IsNotNull(controller.CurrentPage);
        }

        [TestCase]
        public void LoadNextPageIndexTest_IndexShouldIncreaseByOne() //Dependant of LoadFirstPageIndexTest_ShouldSetIndexToOne succeeding
        {
            //setup
            int monthNumber = DateTime.Now.Month;
            int expectedPageIndex = 2;
            //test
            controller.LoadFirstPage();
            controller.LoadNextPage(monthNumber);
            //verify
            Assert.AreEqual(controller.PageIndex, expectedPageIndex);
        }

        [TestCase]
        public void LoadPreviousPageIndexTest_IndexShouldDecreaseByOne() //Dependant of LoadNextPageIndexTest_IndexShouldIncreaseByOne() succeeding
        {
            //setup
            int monthNumber = DateTime.Now.Month;
            int expectedPageIndex = 1;
            //test
            controller.LoadFirstPage();
            controller.LoadNextPage(monthNumber);
            controller.LoadPreviousPage();
            //verify
            Assert.AreEqual(controller.PageIndex, expectedPageIndex);
        }

        [TestCase]
        public void LoadFirstPageIndexTestWithMultipleYears_ShouldSetIndexToZero()
        {
            //setup
            int expectedPageIndex = 0;
            //test
            multipleYearController.LoadFirstPage();
            //verify
            Assert.AreEqual(multipleYearController.PageIndex, expectedPageIndex);
        }

        [TestCase]
        public void LoadFirstPageIndexTestWithNoLifeCharts_()
        {
            //setup
            int expectedPageIndex = 1;
            //test
            emptyController.LoadFirstPage();
            //verify
            Assert.AreEqual(emptyController.PageIndex, expectedPageIndex);
        }

        [TestCase]
        public void LoadDayOverViewPageIndexTest_IndexShouldMatchThatOfDayOverview()
        {
            //setup
            int expectedPageIndex = 2;
            //test
            controller.LoadDayOverViewPage(lifeCharts);
            //verify
            Assert.AreEqual(controller.PageIndex, expectedPageIndex);
        }

        [TestCase]
        public void YearsShouldBeAddedToYearsListTestWhenTheFirstPageIsLoaded() {
            //setup
            int year1 = DateTime.Now.Year;
            int year2 = DateTime.Now.AddYears(-1).Year;

            List<int> expectedYears = new List<int>()
            {
                year1,
                year2
            };
            List<int> yearsInController = multipleYearController.Years;
            //test
            multipleYearController.LoadFirstPage();
            expectedYears.Sort();
            yearsInController.Sort();
            //verify
            Assert.AreEqual(expectedYears, yearsInController);
        }

        [TestCase]
        public void YearShouldBeUpdatedTestWhenTheFirstPageIsLoadedAndAYearIsSelected()
        {
            //setup
            int expectedYear = DateTime.Now.Year;
            //test
            multipleYearController.LoadFirstPage();
            multipleYearController.LoadNextPage(expectedYear);
            //verify
            Assert.AreEqual(multipleYearController.Year, expectedYear);
        }

        [TestCase]
        public void MonthShouldBeUpdatedTestWhenAMonthIsSelectedAndTheNextPageIsLoaded()
        {
            //setup
            int expectedMonth = 2;
            //test
            controller.LoadFirstPage();
            controller.LoadNextPage(expectedMonth);
            //verify
            Assert.AreEqual(controller.Month, expectedMonth);
        }
    }
}
