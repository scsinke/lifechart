﻿using LifeChartApp.Dao;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using NUnit.Framework;
using System.Collections.Generic;

namespace UnitTest.Dao
{
    [TestFixture]
    public class MedicineDaoTest : DatabaseProperties
    {
        private IMedicineDao medicineDao;

        [SetUp]
        public void DbSetup()
        {
            medicineDao = MedicineDao.Instance;
            Connection.DeleteAll<Medicine>();
            Connection.DeleteAll<DefaultMedicine>();
        }

        [TestCase]
        public void InsertDefaultMedicine_Valid()
        {
            //Setup
            DefaultMedicine dm = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName= "MedName"
            };

            //Test
            int insertedId = medicineDao.InsertDefaultMedicine(dm);

            //Verify
            Assert.AreEqual(1, insertedId);
        }

        [TestCase]
        public void InsertMedicines_SameSize()
        {
            List<Medicine> medList = new List<Medicine>();
            int lifeChartId = 1;

            //Setup
            Medicine med1 = new Medicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName1"
            };
            Medicine med2 = new Medicine()
            {
                AmountPerDay = 4,
                DoseInMg = 2,
                MedicineName = "MedName2"
            };
            medList.Add(med1);
            medList.Add(med2);

            //Test
            medicineDao.InsertMedicines(medList, lifeChartId);

            //Verify
            List<Medicine> getMedList = medicineDao.GetMedicinesFromLifeChartId(lifeChartId);

            Assert.IsTrue(medList.Count == getMedList.Count);
        }


        [TestCase]
        public void GetMedicinesFromLifeChartId_Valid()
        {
            List<Medicine> medList = new List<Medicine>();
            int lifeChartId = 1;

            //Setup
            Medicine med1 = new Medicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName1"
            };
            Medicine med2 = new Medicine()
            {
                AmountPerDay = 4,
                DoseInMg = 2,
                MedicineName = "MedName2"
            };
            medList.Add(med1);
            medList.Add(med2);

            //Test
            medicineDao.InsertMedicines(medList, lifeChartId);

            //Verify
            List<Medicine> getMedList = medicineDao.GetMedicinesFromLifeChartId(lifeChartId);

            Assert.IsTrue(medList.Count == getMedList.Count);
        }

        [TestCase]
        public void GetDefaultMedicine_Valid()
        {
            //Setup
            DefaultMedicine defaultMed = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName"
            };

            //Test
            medicineDao.InsertDefaultMedicine(defaultMed);
            DefaultMedicine dm = medicineDao.GetDefaultMedicine(1);

            //Verify
            Assert.IsTrue(dm.MedicineName == "MedName");
        }

        [TestCase]
        public void DeleteDefaultMed_Deleted()
        {
            //Setup
            DefaultMedicine defaultMed = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName"
            };

            //Test
            medicineDao.InsertDefaultMedicine(defaultMed);
            medicineDao.DeleteDefaultMedicine(defaultMed);

            DefaultMedicine dm = medicineDao.GetDefaultMedicine(1);

            //Verify
            Assert.IsNull(dm);
        }

        [TestCase]
        public void UpdateDefaultMedicine_Name_Valid()
        {
            //Setup
            string newMedName = "newName";
            DefaultMedicine dm = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName"
            };

            //Test
            int id = medicineDao.InsertDefaultMedicine(dm);

            dm.MedicineName = newMedName;
            medicineDao.UpdateDefaultMedicine(dm);

            DefaultMedicine defaultMedicine = medicineDao.GetDefaultMedicine(id);

            //Verify
            Assert.IsTrue(defaultMedicine.MedicineName == newMedName);
        }

        [TestCase]
        public void UpdateDefaultMedicine_Dose_Valid()
        {
            //Setup
            int newMedDose = 22;
            DefaultMedicine dm = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName"
            };

            //Test
            int id = medicineDao.InsertDefaultMedicine(dm);

            dm.DoseInMg = newMedDose;
            medicineDao.UpdateDefaultMedicine(dm);

            DefaultMedicine defaultMedicine = medicineDao.GetDefaultMedicine(id);

            //Verify
            Assert.IsTrue(defaultMedicine.DoseInMg == newMedDose);
        }

        [TestCase]
        public void GetAllDefaultMedicines_Filled()
        {
            //Setup
            DefaultMedicine defaultMed = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName"
            };
            DefaultMedicine defaultMed2 = new DefaultMedicine()
            {
                AmountPerDay = 5,
                DoseInMg = 1,
                MedicineName = "MedName2"
            };

            //Test
            medicineDao.InsertDefaultMedicine(defaultMed);
            medicineDao.InsertDefaultMedicine(defaultMed2);
            List<DefaultMedicine> medList = medicineDao.GetAllDefaultMedicines();

            //Verify
            Assert.IsTrue(medList.Count == 2);
        }

    }
}