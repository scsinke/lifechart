﻿using LifeChartApp.Dao;
using LifeChartApp.Model;
using NUnit.Framework;

namespace UnitTest.Dao
{
    [TestFixture]
    public class LifeEventDaoTest : DatabaseProperties
    {
        private ILifeEventDao lifeEventDao = LifeEventDao.Instance;

        [SetUp]
        public void DbSetup()
        {
            Connection.DeleteAll<LifeEvent>();
        }

        [TestCase]
        public void InsertLifeEvent_Valid()
        {
            LifeEvent le = new LifeEvent()
            {
                Event = "test",
                MoodInfluence = 4
            };

            int insertedId = lifeEventDao.InsertLifeEvent(le);

            Assert.Greater(insertedId, 0);
        }

        [TestCase]
        public void GetLifeEvent_Valid()
        {
            LifeEvent le = new LifeEvent()
            {
                Event = "test",
                MoodInfluence = 4
            };

            lifeEventDao.InsertLifeEvent(le);

            LifeEvent getLe = lifeEventDao.GetLifeEvent(1);

            Assert.AreEqual(1, getLe.EventId);
        }

        [TestCase]
        public void DeleteLifeEvent_Valid()
        {
            LifeEvent lifeEvent = new LifeEvent()
            {
                Event = "test",
                MoodInfluence = 4
            };

            lifeEventDao.InsertLifeEvent(lifeEvent);
            lifeEventDao.DeleteLifeEvent(1);

            LifeEvent le = lifeEventDao.GetLifeEvent(1);

            Assert.IsNull(le);
        }
    }
}