﻿using NUnit.Framework;
using LifeChartApp.Model;
using LifeChartApp.Interfaces;
using LifeChartApp.Dao;

namespace UnitTest.Dao
{
    [TestFixture]
    public class PersonDaoTest : DatabaseProperties
    {
        private IPersonDao personDao;

        [SetUp]
        public void DbSetup()
        {
            personDao = PersonDao.Instance;
            Connection.DeleteAll<Person>();
        }

        [TestCase]
        public void InsertPerson_Correct()
        {
            //Setup
            Person person = new Person()
            {
                FirstName = "Firstname",
                LastName = "Lastname",
                Sex = "MALE",
                Weight = 70
            };

            //Test
            int insertIndex = personDao.InsertPerson(person);

            //Verify
            Assert.Greater(insertIndex, 0);
        }

        [TestCase]
        public void GetPerson_ValidPerson()
        {
            //Setup
            Person person = new Person()
            {
                FirstName = "Firstname",
                LastName = "Lastname",
                Sex = "MALE",
                Weight = 70
            };

            //Test
            personDao.InsertPerson(person);
            person.PersonId = 1;

            Person getPerson = personDao.GetPerson(1);

            //Verify
            Assert.IsTrue(person.PersonId == getPerson.PersonId);
        }

        [TestCase]
        public void GetPerson_InvalidPerson()
        {
            //Setup

            //Test
            Person p = personDao.GetPerson(-2);

            //Verify
            Assert.IsNull(p);
        }

        [TestCase]
        public void UpdatePerson_Success()
        {
            //Setup
            Person person = new Person()
            {
                FirstName = "Firstname",
                LastName = "Lastname",
                Sex = "MALE",
                Weight = 70,
            };

            //Test
            int personId = personDao.InsertPerson(person);

            person.FirstName = "NewName";

            personDao.UpdatePerson(person);

            Person getPerson = personDao.GetPerson(personId);

            Assert.IsTrue("NewName" == getPerson.FirstName);
        }

        [TestCase]
        public void UpdatePerson_Invalid_PersonIdException()
        {
            //Setup
            Person person = new Person()
            {
                FirstName = "Firstname",
                LastName = "Lastname",
                Sex = "MALE",
                Weight = 70,
                PersonId = 1
            };

            //Test
            int personId = personDao.InsertPerson(person);

            person.FirstName = "NewName";
            person.PersonId = personId;

            personDao.UpdatePerson(person);
        }

        [TestCase]
        public void DeletePerson_Valid()
        {
            //Setup
            Person person = new Person()
            {
                FirstName = "Firstname",
                LastName = "Lastname",
                Sex = "MALE",
                Weight = 70,
            };

            //Test
            int id = personDao.InsertPerson(person);
            personDao.DeletePerson(id);

            Person p = personDao.GetPerson(id);

            Assert.IsNull(p);
        }

    }
}