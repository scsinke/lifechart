﻿using LifeChartApp.Dao;
using LifeChartApp.Interfaces;
using LifeChartApp.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace UnitTest.Dao
{
    public class LifeChartDaoTest : DatabaseProperties
    {
        private ILifeChartDao lifeChartDao;

        [SetUp]
        public void DbSetup()
        {
            lifeChartDao = LifeChartDao.Instance;
            Connection.DeleteAll<LifeChart>();
        }

        private LifeChart LifeChartSetup()
        {
            return new LifeChart()
            {
                Weight = 80,
                Menstruated = false,
                Mood = "Amavin",
                MoodRating = 3,
                MoodSwings = 2,
                Uploaded = false,
                SeverityOfEpisode = "STABLE",
                Sleep = 8,
                Date = DateTime.Now
            };
        }

        [TestCase]
        public void GetLifeChart_Id_Valid()
        {
            //Setup
            LifeChart lc = LifeChartSetup();
            List<Medicine> medList = new List<Medicine>();
            List<LifeEvent> lifeEventList = new List<LifeEvent>();

            CompleteLifeChart clc = new CompleteLifeChart()
            {
                LifeChart = lc,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            //Test
            int insertedId = lifeChartDao.InsertCompleteLifeChart(clc, 1);
            CompleteLifeChart lifeChart = lifeChartDao.GetCompleteLifeChart(insertedId);

            //Verify
            Assert.IsTrue(lifeChart.LifeChart.LifeChartId == insertedId);
        }

        [TestCase]
        public void InsertCompleteLifeChart_EmptyMedicineList_ShouldNotCrash()
        {
            //Setup
            LifeChart lc = LifeChartSetup();
            List<LifeEvent> lifeEventList = new List<LifeEvent>();

            CompleteLifeChart clc = new CompleteLifeChart()
            {
                LifeChart = lc,
                LifeEvents = lifeEventList
            };

            //Test
            int insertedId = lifeChartDao.InsertCompleteLifeChart(clc, 1);
            CompleteLifeChart lifeChart = lifeChartDao.GetCompleteLifeChart(insertedId);

            //Verify
            Assert.NotNull(lifeChart.LifeChart);
        }

        [TestCase]
        public void InsertCompleteLifeChart_EmptyLifeEventList_ShouldNotCrash()
        {
            //Setup
            LifeChart lc = LifeChartSetup();
            List<Medicine> medicineList = new List<Medicine>();

            CompleteLifeChart clc = new CompleteLifeChart()
            {
                LifeChart = lc,
                Medicines = medicineList
            };

            //Test
            int insertedId = lifeChartDao.InsertCompleteLifeChart(clc, 1);
            CompleteLifeChart lifeChart = lifeChartDao.GetCompleteLifeChart(insertedId);

            //Verify
            Assert.NotNull(lifeChart.LifeChart);
        }


        [TestCase]
        public void InsertCompleteLifeChart_Valid_NotNull()
        {
            //Setup
            LifeChart lc = LifeChartSetup();
            List<Medicine> medList = new List<Medicine>();
            List<LifeEvent> lifeEventList = new List<LifeEvent>();

            CompleteLifeChart clc = new CompleteLifeChart()
            {
                LifeChart = lc,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            //Test
            int insertedId = lifeChartDao.InsertCompleteLifeChart(clc, 1);
            CompleteLifeChart lifeChart = lifeChartDao.GetCompleteLifeChart(insertedId);

            //Verify
            Assert.NotNull(lifeChart.LifeChart);
        }


        [TestCase]
        public void DeleteLifeChart_Valid()
        {
            //Setup
            LifeChart lc = LifeChartSetup();
            List<Medicine> medList = new List<Medicine>();
            List<LifeEvent> lifeEventList = new List<LifeEvent>();

            CompleteLifeChart clc = new CompleteLifeChart()
            {
                LifeChart = lc,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            //Test
            int id = lifeChartDao.InsertCompleteLifeChart(clc, 1);
            lifeChartDao.DeleteLifeChart(id);

            CompleteLifeChart lifeChart = lifeChartDao.GetCompleteLifeChart(id);

            //Verify
            Assert.Null(lifeChart.LifeChart);
        }


        [TestCase]
        public void GetLatestLifeChart_Valid()
        {
            LifeChart lc1 = LifeChartSetup();
            List<Medicine> medList = new List<Medicine>();
            List<LifeEvent> lifeEventList = new List<LifeEvent>();

            CompleteLifeChart clc1 = new CompleteLifeChart()
            {
                LifeChart = lc1,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            LifeChart lc2 = LifeChartSetup();

            CompleteLifeChart clc2 = new CompleteLifeChart()
            {
                LifeChart = lc2,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            //Test
            lifeChartDao.InsertCompleteLifeChart(clc1, 1);
            lifeChartDao.InsertCompleteLifeChart(clc2, 1);

            //Verify
            LifeChart latestLifeChart = lifeChartDao.GetLatestLifeChart();

            Assert.IsTrue(lc2.Date == latestLifeChart.Date);
        }

        [TestCase]
        public void GetNotUploadedLifeChartContainers_ShouldReturnCorrectAmountOfLifeCharts()
        {
            LifeChart lc1 = LifeChartSetup();
            List<Medicine> medList = new List<Medicine>();
            List<LifeEvent> lifeEventList = new List<LifeEvent>();

            CompleteLifeChart clc1 = new CompleteLifeChart()
            {
                LifeChart = lc1,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            LifeChart lc2 = LifeChartSetup();
            lc2.Uploaded = true;

            CompleteLifeChart clc2 = new CompleteLifeChart()
            {
                LifeChart = lc2,
                Medicines = medList,
                LifeEvents = lifeEventList
            };

            //Test
            lifeChartDao.InsertCompleteLifeChart(clc1, 1);
            lifeChartDao.InsertCompleteLifeChart(clc2, 1);

            //Verify
            List<LifeChartContainer> lifeChartContainerList = lifeChartDao.GetNotUploadedLifeChartContainers();

            Assert.IsTrue(lifeChartContainerList.Count == 1);
        }
    }
}