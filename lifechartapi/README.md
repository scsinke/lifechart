# Pitest mutation test

## Installation
To use this test, your computer needs:

- [Maven](https://maven.apache.org/install.html)

### setup

Add pitest maven dependency
```bash
    <plugin>
        <groupId>org.pitest</groupId>
        <artifactId>pitest-maven</artifactId>
        <version>LATEST</version>
        <configuration>
            <sourceDataFormats>
                <sourceDataFormat>HTML</sourceDataFormat>
            </sourceDataFormats>
        </configuration>
    </plugin>
```

run `mvn org.pitest:pitest-maven:mutationCoverage` to run the plugin