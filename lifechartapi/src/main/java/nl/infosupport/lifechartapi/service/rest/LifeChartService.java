package nl.infosupport.lifechartapi.service.rest;

import nl.infosupport.lifechartapi.POJO.LifeChartsContainer;
import nl.infosupport.lifechartapi.logic.ILifeChartLogic;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NoContentException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/lifechart")
public class LifeChartService {

    private static final Logger LOGGER = Logger.getLogger(LifeChartService.class.getName());

    @Inject
    private ILifeChartLogic lifeChartLogic;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLifeCharts(@QueryParam("token") String token, LifeChartsContainer input) {
        try {
            lifeChartLogic.addLifeCharts(token, input);
            return Response.status(Status.OK).build();
        } catch (NonExistentTokenException e) {
            LOGGER.log(Level.SEVERE, "Token not authorized", e);
            return Response.status(Status.UNAUTHORIZED).build();
        } catch (NoContentException e) {
            LOGGER.log(Level.INFO, "No content was send", e);
            return Response.status(Status.NO_CONTENT).build();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed to insert into database", e);
            return Response.status(Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLifeChartsFromUser(@QueryParam("token") String token, @QueryParam("userId") int userId){
        try {
            LifeChartsContainer lifeChartsContainer = lifeChartLogic.getLifeChartsFromUser(userId, token);
            return Response.status(Status.OK).entity(lifeChartsContainer).build();
        }catch (NotAuthorizedException e){
            LOGGER.log(Level.SEVERE, "Token not authorized", e);
            return Response.status(Status.UNAUTHORIZED).build();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed to fetch LifeCharts from user", e);
            return Response.status(Status.BAD_REQUEST).build();
        } catch (NonExistentTokenException e) {
            LOGGER.log(Level.SEVERE, "Token does not exist", e);
            return Response.status(Status.BAD_REQUEST).build();
        }
    }
}
