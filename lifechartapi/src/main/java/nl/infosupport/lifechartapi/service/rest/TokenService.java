package nl.infosupport.lifechartapi.service.rest;

import nl.infosupport.lifechartapi.logic.IAuthorizationLogic;
import nl.infosupport.lifechartapi.logic.manager.IllegalUserIdException;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/token")
public class TokenService {

    private static final Logger LOGGER = Logger.getLogger(TokenService.class.getName());

    @Inject
    private IAuthorizationLogic authorizationLogic;

    /**
     * Request a token to call with the other endpoints in the api,
     *
     * @param id gets linked to the returned token, the id is used to identify the requests.
     * @return response 200 with token in plain text or response 400 when the user id is zero or negative
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response requestToken(@QueryParam("id") int id) {
        String token;
        try {
            token = authorizationLogic.generateToken(id);
        } catch (IllegalUserIdException e) {
            LOGGER.log(Level.WARNING, "User does not exist", e);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().entity(token).build();
    }
}
