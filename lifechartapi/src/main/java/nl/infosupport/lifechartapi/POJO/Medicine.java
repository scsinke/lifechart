package nl.infosupport.lifechartapi.POJO;

public class Medicine {
    private String name;
    private Integer doseInMg, amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDoseInMg() {
        return doseInMg;
    }

    public void setDoseInMg(Integer doseInMg) {
        this.doseInMg = doseInMg;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
