package nl.infosupport.lifechartapi.POJO;

import java.util.List;

public class LifeChartsContainer {
    private List<LifeChart> lifeCharts;

    public List<LifeChart> getLifeCharts() {
        return lifeCharts;
    }

    public void setLifeCharts(List<LifeChart> lifeCharts) {
        this.lifeCharts = lifeCharts;
    }
}
