package nl.infosupport.lifechartapi.POJO;

import java.util.List;

public class LifeChart {
    private String mood, severityOfEpisode, complaints;
    private Integer moodRating, moodSwings;
    private Double weight;
    private Double sleep;
    private Boolean menstruated, unrestrainedMania;
    private String date;

    private List<Medicine> medicines;
    private List<LifeEvent> lifeEvents;
    private int personId;

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getSeverityOfEpisode() {
        return severityOfEpisode;
    }

    public void setSeverityOfEpisode(String severityOfEpisode) {
        this.severityOfEpisode = severityOfEpisode;
    }

    public String getComplaints() {
        return complaints;
    }

    public void setComplaints(String complaints) {
        this.complaints = complaints;
    }

    public Integer getMoodRating() {
        return moodRating;
    }

    public void setMoodRating(Integer moodRating) {
        this.moodRating = moodRating;
    }

    public Integer getMoodSwings() {
        return moodSwings;
    }

    public void setMoodSwings(Integer moodSwings) {
        this.moodSwings = moodSwings;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getSleep() {
        return sleep;
    }

    public void setSleep(Double sleep) {
        this.sleep = sleep;
    }

    public Boolean getMenstruated() {
        return menstruated;
    }

    public void setMenstruated(Boolean menstruated) {
        this.menstruated = menstruated;
    }

    public Boolean getUnrestrainedMania() {
        return unrestrainedMania;
    }

    public void setUnrestrainedMania(Boolean unrestrainedMania) {
        this.unrestrainedMania = unrestrainedMania;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<Medicine> medicines) {
        this.medicines = medicines;
    }

    public List<LifeEvent> getLifeEvents() {
        return lifeEvents;
    }

    public void setLifeEvents(List<LifeEvent> lifeEvents) {
        this.lifeEvents = lifeEvents;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }
}
