package nl.infosupport.lifechartapi.POJO;

public class LifeEvent {
    private String name;
    private Integer moodInfluence;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMoodInfluence() {
        return moodInfluence;
    }

    public void setMoodInfluence(Integer moodInfluence) {
        this.moodInfluence = moodInfluence;
    }
}
