package nl.infosupport.lifechartapi.DAO.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseConnection {
    private static final Logger LOGGER = Logger.getLogger(DatabaseConnection.class.getName());
    private static DatabaseConnection instance;
    private Connection conn;

    public static DatabaseConnection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    public Connection create() {
        DatabaseProperties properties = DatabaseProperties.getInstance();
        try {
            if (conn == null) {
                Class.forName(properties.getDriver());
                conn = DriverManager.getConnection(properties.getConnection(), properties.getUser(), properties.getPassword());
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Initializing database connection problem", e);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Class could not be found", e);
        }
        return conn;
    }
}

