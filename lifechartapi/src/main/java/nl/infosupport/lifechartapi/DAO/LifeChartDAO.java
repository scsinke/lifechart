package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.DatabaseConnection;
import nl.infosupport.lifechartapi.DAO.util.Queries;
import nl.infosupport.lifechartapi.POJO.LifeChart;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LifeChartDAO extends Queries implements ILifeChartDAO {
    private Connection conn = DatabaseConnection.getInstance().create();
    private Queries queries;

    public LifeChartDAO() {
        this.queries = new Queries();
    }

    public Integer addLifeChart(Integer userId, LifeChart lifeChart) throws SQLException {
        PreparedStatement insertLifeChartStmt = conn.prepareStatement(queries.insertLifeChartQuery(), Statement.RETURN_GENERATED_KEYS);
        insertLifeChartStmt.setDouble(1, lifeChart.getWeight());
        insertLifeChartStmt.setDouble(2, lifeChart.getSleep());
        insertLifeChartStmt.setBoolean(3, lifeChart.getUnrestrainedMania());
        insertLifeChartStmt.setString(4, lifeChart.getSeverityOfEpisode());
        insertLifeChartStmt.setString(5, lifeChart.getComplaints());
        insertLifeChartStmt.setInt(6, lifeChart.getMoodSwings());
        insertLifeChartStmt.setBoolean(7, lifeChart.getMenstruated());
        insertLifeChartStmt.setString(8, lifeChart.getDate());
        insertLifeChartStmt.setInt(9, lifeChart.getMoodRating());
        insertLifeChartStmt.setInt(10, userId);
        insertLifeChartStmt.setString(11, lifeChart.getMood());

        insertLifeChartStmt.executeUpdate();

        ResultSet generatedKeys = insertLifeChartStmt.getGeneratedKeys();
        generatedKeys.first();

        return generatedKeys.getInt(1);
    }

    public LifeChart getLifeChartById(int lifeChartId) throws SQLException {
        LifeChart lifeChart = new LifeChart();
        PreparedStatement findLifeChartStmt = conn.prepareStatement(queries.getLifeChartByIdQuery());
        findLifeChartStmt.setInt(1, lifeChartId);

        ResultSet lifeChartResultSet = findLifeChartStmt.executeQuery();
        lifeChartResultSet.first();

        lifeChart.setWeight(lifeChartResultSet.getDouble("WEIGHT"));
        lifeChart.setSleep(lifeChartResultSet.getDouble("SLEEP"));
        lifeChart.setUnrestrainedMania(lifeChartResultSet.getBoolean("DESPITEMANIA"));
        lifeChart.setSeverityOfEpisode(lifeChartResultSet.getString("SEVERITYOFEPISODE"));
        lifeChart.setComplaints(lifeChartResultSet.getString("COMPLAINTS"));
        lifeChart.setMoodSwings(lifeChartResultSet.getInt("MOODSWINGS"));
        lifeChart.setMenstruated(lifeChartResultSet.getBoolean("MENSTRUATED"));
        lifeChart.setDate(lifeChartResultSet.getString("DATE"));
        lifeChart.setMoodRating(lifeChartResultSet.getInt("MOODRATING"));
        lifeChart.setPersonId(lifeChartResultSet.getInt("PERSONID"));
        lifeChart.setMood(lifeChartResultSet.getString("LONGTERMMOOD"));
        return lifeChart;
    }

    public List<Integer> getAllLifeChartIdsByClientId(int clientId) throws SQLException {
        List<Integer> lifeChartIds = new ArrayList<>();
        PreparedStatement findAllLifeChartsStmt = conn.prepareStatement(queries.getAllLifeChartIdsByClientIdQuery());
        findAllLifeChartsStmt.setInt(1, clientId);

        ResultSet resultSet = findAllLifeChartsStmt.executeQuery();
        while (resultSet.next()) {
            lifeChartIds.add(resultSet.getInt("LIFECHARTID"));
        }
        return lifeChartIds;
    }
}
