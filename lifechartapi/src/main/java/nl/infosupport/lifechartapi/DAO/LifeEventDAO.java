package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.DatabaseConnection;
import nl.infosupport.lifechartapi.DAO.util.Queries;
import nl.infosupport.lifechartapi.POJO.LifeEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LifeEventDAO implements ILifeEventDAO {
    private Connection conn = DatabaseConnection.getInstance().create();
    private Queries queries;

    public LifeEventDAO() {
        queries = new Queries();
    }

    public void addLifeEvents(Integer lifeChartId, List<LifeEvent> lifeEvents) throws SQLException {
        for (LifeEvent lifeEvent : lifeEvents) {
            insertLifeEvent(lifeChartId, lifeEvent);
        }
    }

    private boolean insertLifeEvent(Integer lifeChartId, LifeEvent lifeEvent) throws SQLException {
        PreparedStatement insertLifeEventStmt = conn.prepareStatement(queries.insertLifeEventQuery());
        insertLifeEventStmt.setString(1, lifeEvent.getName());
        insertLifeEventStmt.setInt(2, lifeEvent.getMoodInfluence());
        insertLifeEventStmt.setInt(3, lifeChartId);

        return insertLifeEventStmt.execute();
    }

    public List<LifeEvent> getLifeEventsByLifeChartId(int lifeChartId) throws SQLException {
        List<LifeEvent> lifeEventList = new ArrayList<>();
        PreparedStatement fetchLifeEventsStmt = conn.prepareStatement(queries.getLifeEventsByLifeChartIdQuery());
        fetchLifeEventsStmt.setInt(1, lifeChartId);

        ResultSet resultSet = fetchLifeEventsStmt.executeQuery();
        while (resultSet.next()) {
            LifeEvent lifeEvent = new LifeEvent();
            lifeEvent.setName(resultSet.getString(1));
            lifeEvent.setMoodInfluence(resultSet.getInt(2));

            lifeEventList.add(lifeEvent);
        }
        return lifeEventList;
    }
}
