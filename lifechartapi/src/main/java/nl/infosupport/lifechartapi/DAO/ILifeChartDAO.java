package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.POJO.LifeChart;

import java.sql.SQLException;
import java.util.List;

public interface ILifeChartDAO {
    /**
     * Adds LifeChart to Database and returns LifeChartId
     *
     * @param userId    Connect LifeChart to user
     * @param lifeChart LifeChart that user filled
     * @return lifeChartId  Id from added LifeChart
     * @throws SQLException
     */
    Integer addLifeChart(Integer userId, LifeChart lifeChart) throws SQLException;

    /**
     * Get the life chart with id {@param lifeChartId}.
     *
     * @param lifeChartId Id of a life chart
     * @return lifeChart
     * @throws SQLException
     */
    LifeChart getLifeChartById(int lifeChartId) throws SQLException;

    /**
     * Get all life charts where the client id is equal to {@param clientId}.
     *
     * @param clientId Id of a client
     * @return List of all id's of life charts which belong to the client
     * @throws SQLException
     */
    List<Integer> getAllLifeChartIdsByClientId(int clientId) throws SQLException;
}
