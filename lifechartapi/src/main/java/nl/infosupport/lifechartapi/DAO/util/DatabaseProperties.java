package nl.infosupport.lifechartapi.DAO.util;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseProperties {
    private static final Logger LOGGER = Logger.getLogger(DatabaseProperties.class.getName());
    private static DatabaseProperties instance;
    private static final String DATABASE_PROPERTIES = "database.properties";
    private Properties properties;

    private DatabaseProperties() {
        loadPropertyFile();
    }

    public static DatabaseProperties getInstance() {
        if (instance == null) {
            instance = new DatabaseProperties();
        }
        return instance;
    }

    private void loadPropertyFile() {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(DATABASE_PROPERTIES));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error reading property file: " + DATABASE_PROPERTIES, e);
        }
    }

    public String getDriver() {
        return properties.getProperty("driver");
    }

    public String getConnection() {
        return properties.getProperty("connection");
    }

    public String getUser() {
        return properties.getProperty("user");
    }

    public String getPassword() {
        return properties.getProperty("password");
    }
}
