package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.DatabaseConnection;
import nl.infosupport.lifechartapi.DAO.util.Queries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements IUserDAO {
    private Connection conn = DatabaseConnection.getInstance().create();
    private Queries queries;

    public UserDAO() {
        this.queries = new Queries();
    }

    public List<Integer> getPersonsByTherapistId(int therapistId) throws SQLException {
        List<Integer> clientIds = new ArrayList<>();

        PreparedStatement statement = conn.prepareStatement(queries.getAllClientsFromTherapist());
        statement.setInt(1, therapistId);

        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            clientIds.add(resultSet.getInt("PERSONID"));
        }
        return clientIds;
    }
}
