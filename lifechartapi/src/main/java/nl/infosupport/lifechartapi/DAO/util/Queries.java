package nl.infosupport.lifechartapi.DAO.util;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Queries {
    private static final Logger LOGGER = Logger.getLogger(Queries.class.getName());
    private Properties properties = new Properties();

    public Queries() {
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("queriesMySQL.properties"));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to load queries", e);
        }
    }

    public String getLifeChartByIdQuery() {
        return properties.getProperty("getLifeChartById");
    }

    public String getAllLifeChartIdsByClientIdQuery() {
        return properties.getProperty("getAllLifeChartIdsByClientId");
    }

    public String getMedicinesByLifeChartIdQuery() {
        return properties.getProperty("getMedicinesByLifeChartId");
    }

    public String addMedicinesToLifeChart() { return properties.getProperty("addMedicinesToLifeChart"); }

    public String getLifeEventsByLifeChartIdQuery() {
        return properties.getProperty("getLifeEventsByLifeChartId");
    }

    public String insertLifeChartQuery() { return properties.getProperty("insertLifeChart"); }

    public String insertLifeEventQuery() {
        return properties.getProperty("insertLifeEvent");
    }

    public String getAllClientsFromTherapist() { return properties.getProperty("getAllClientsFromTherapist"); }
}
