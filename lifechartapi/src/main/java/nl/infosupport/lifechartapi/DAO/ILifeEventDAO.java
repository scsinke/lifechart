package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.POJO.LifeEvent;

import java.sql.SQLException;
import java.util.List;

public interface ILifeEventDAO {
    /**
     * Adds a list of {@link LifeEvent} to Database and links them to LifeChart via lifeChartId
     *
     * @param lifeChartId lifeChartId, LifeEvent can be added to specific LifeChart
     * @param lifeEvents  List of LifeEvent to be added via DAO
     * @throws SQLException
     */
    void addLifeEvents(Integer lifeChartId, List<LifeEvent> lifeEvents) throws SQLException;

    /**
     * Get all {@link LifeEvent} that belong to the life chart with id {@param lifeChartId}.
     *
     * @param lifeChartId specifies from which life chart to fetch the life events
     * @return List of {@link LifeEvent}
     */
    List<LifeEvent> getLifeEventsByLifeChartId(int lifeChartId) throws SQLException;
}
