package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.POJO.Medicine;

import java.sql.SQLException;
import java.util.List;

public interface IMedicineDAO {
    /**
     * Adds medicine to Database and links them to LifeChart via lifeChartId
     *
     * @param lifeChartId lifeChartId, medicines can be added to lifeChart
     * @param medicines   List of Medicine objects
     * @throws SQLException
     */
    void addMedicines(Integer lifeChartId, List<Medicine> medicines) throws SQLException;

    /**
     * Get all {@link Medicine} that belong to the life chart with id {@param lifeChartId}.
     *
     * @param lifeChartId specifies from which life chart to fetch the life events
     * @return List of {@link Medicine}
     * @throws SQLException
     */
    List<Medicine> getMedicinesByLifeChartId(int lifeChartId) throws SQLException;
}
