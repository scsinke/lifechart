package nl.infosupport.lifechartapi.DAO;

import java.sql.SQLException;
import java.util.List;

public interface IUserDAO {
    /**
     *
     * @param therapistId           id of therapist
     * @return                      returns list of clients that have {@param therapistId} as therapist
     * @throws SQLException
     */
    List<Integer> getPersonsByTherapistId(int therapistId) throws SQLException;
}
