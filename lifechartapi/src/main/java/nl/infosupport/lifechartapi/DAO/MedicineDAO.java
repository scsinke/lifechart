package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.DatabaseConnection;
import nl.infosupport.lifechartapi.DAO.util.Queries;
import nl.infosupport.lifechartapi.POJO.Medicine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MedicineDAO implements IMedicineDAO {
    private Connection conn = DatabaseConnection.getInstance().create();
    private Queries queries;

    public MedicineDAO() {
        this.queries = new Queries();
    }

    public void addMedicines(Integer lifeChartId, List<Medicine> medicines) throws SQLException {

        for (Medicine medicine : medicines) {
            addMedicineToDatabase(lifeChartId, medicine);
        }
    }

    private boolean addMedicineToDatabase(Integer lifeChartId, Medicine medicine) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(queries.addMedicinesToLifeChart());

        statement.setInt(1, lifeChartId);
        statement.setString(2, medicine.getName());
        statement.setInt(3, medicine.getDoseInMg());
        statement.setInt(4, medicine.getAmount());

        return statement.execute();
    }

    public List<Medicine> getMedicinesByLifeChartId(int lifeChartId) throws SQLException {
        List<Medicine> medicines = new ArrayList<>();

        PreparedStatement statement = conn.prepareStatement(queries.getMedicinesByLifeChartIdQuery());
        statement.setInt(1, lifeChartId);

        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Medicine medicine = new Medicine();
            medicine.setName(resultSet.getString("NAME"));
            medicine.setDoseInMg(resultSet.getInt("DOSEINMG"));
            medicine.setAmount(resultSet.getInt("AMOUNTPERDAY"));

            medicines.add(medicine);
        }

        return medicines;
    }
}
