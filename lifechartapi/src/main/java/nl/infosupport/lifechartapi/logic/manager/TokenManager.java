package nl.infosupport.lifechartapi.logic.manager;

import java.util.HashMap;

public class TokenManager {
    private static TokenManager instance;
    private HashMap<String, Integer> tokenMap;

    private TokenManager() {
        tokenMap = new HashMap<>();
    }

    public static TokenManager getInstance() {
        if (instance == null) {
            instance = new TokenManager();
        }
        return instance;
    }

    public void addToken(int id, String token) throws IllegalUserIdException, DuplicateTokenException {
        checkIfUserIdIsValid(id);

        checkDuplicateToken(token);

        tokenMap.put(token, id);
    }

    private void checkIfUserIdIsValid(Integer id) throws IllegalUserIdException {
        if (id < 1){
            throw new IllegalUserIdException("Value of id cannot be zero or negative");
        }
    }

    private void checkDuplicateToken(String token) throws DuplicateTokenException {
        if (tokenMap.containsKey(token)){
            throw new DuplicateTokenException("TokenManager already contains the token: " + token);
        }
    }

    public void removeToken(String token) throws NonExistentTokenException {
        if (!tokenMap.containsKey(token)) {
            throw new NonExistentTokenException("TokenManager does not contain the token: " + token);
        }
        tokenMap.remove(token);
    }

    public int getUserIdByToken(String token) throws NonExistentTokenException {
        if (!tokenMap.containsKey(token)) {
            throw new NonExistentTokenException("TokenManager does not contain the token: " + token);
        }
        return tokenMap.get(token);
    }

}
