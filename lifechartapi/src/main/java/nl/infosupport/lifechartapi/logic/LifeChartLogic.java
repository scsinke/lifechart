package nl.infosupport.lifechartapi.logic;

import nl.infosupport.lifechartapi.DAO.ILifeChartDAO;
import nl.infosupport.lifechartapi.DAO.ILifeEventDAO;
import nl.infosupport.lifechartapi.DAO.IMedicineDAO;
import nl.infosupport.lifechartapi.POJO.LifeChart;
import nl.infosupport.lifechartapi.POJO.LifeChartsContainer;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.NoContentException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LifeChartLogic implements ILifeChartLogic {

    @Inject
    private ILifeChartDAO lifeChartDAO;
    @Inject
    private IMedicineDAO medicineDAO;
    @Inject
    private ILifeEventDAO lifeEventDAO;
    @Inject
    private IAuthorizationLogic authorizationLogic;

    public void addLifeCharts(String token, LifeChartsContainer input) throws NonExistentTokenException, NoContentException, SQLException {

        List<LifeChart> lifeCharts = input.getLifeCharts();

        checkIfListIsEmpty(lifeCharts);

        int userId = authorizationLogic.getUserIdByToken(token);

        for (LifeChart lifeChart : lifeCharts) {
            addLifeChart(userId, lifeChart);
        }
        authorizationLogic.removeToken(token);
    }

    private void checkIfListIsEmpty(List<LifeChart> lifeCharts) throws NoContentException {
        if (lifeCharts == null || lifeCharts.isEmpty()) {
            throw new NoContentException("No LifeCharts found!");
        }
    }

    private void addLifeChart(Integer userId, LifeChart lifeChart) throws SQLException {
        int lifeChartId = lifeChartDAO.addLifeChart(userId, lifeChart);
        medicineDAO.addMedicines(lifeChartId, lifeChart.getMedicines());
        lifeEventDAO.addLifeEvents(lifeChartId, lifeChart.getLifeEvents());
    }

    public LifeChartsContainer getLifeChartsFromUser(int clientId, String token) throws NonExistentTokenException, SQLException, NotAuthorizedException {
        int therapistId = authorizationLogic.getUserIdByToken(token);
        List<LifeChart> lifecharts = new ArrayList<>();
        LifeChartsContainer lifeChartsContainer = new LifeChartsContainer();

        authorizationLogic.isTherapistAuthorizedForClient(clientId, therapistId);

        List<Integer> lifeChartIds = lifeChartDAO.getAllLifeChartIdsByClientId(clientId);

        for (int lifeChartId : lifeChartIds) {
            LifeChart lc = getLifeChart(lifeChartId);
            lifecharts.add(lc);
        }
        lifeChartsContainer.setLifeCharts(lifecharts);
        return lifeChartsContainer;
    }


    private LifeChart getLifeChart(int lifeChartId) throws SQLException {
        LifeChart lc = lifeChartDAO.getLifeChartById(lifeChartId);
        lc.setMedicines(medicineDAO.getMedicinesByLifeChartId(lifeChartId));
        lc.setLifeEvents(lifeEventDAO.getLifeEventsByLifeChartId(lifeChartId));
        return lc;
    }

}
