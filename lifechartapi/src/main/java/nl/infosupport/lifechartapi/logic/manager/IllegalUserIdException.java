package nl.infosupport.lifechartapi.logic.manager;

public class IllegalUserIdException extends Exception {

    public IllegalUserIdException() {
    }

    public IllegalUserIdException(String message) {
        super(message);
    }
}
