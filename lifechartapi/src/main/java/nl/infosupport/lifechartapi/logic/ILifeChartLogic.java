package nl.infosupport.lifechartapi.logic;

import nl.infosupport.lifechartapi.POJO.LifeChartsContainer;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.NoContentException;
import java.sql.SQLException;

public interface ILifeChartLogic {
    /**
     * Splits LifeChart list and adds the LifeChart via DAO layer
     *
     * @param token
     * @param lifeCharts List of LifeCharts
     * @throws NoContentException           when provided List of LifeCharts is empty
     * @throws NonExistentTokenException    when token is not existent
     * @throws SQLException                 something went wrong with a database operation
     */
    void addLifeCharts(String token, LifeChartsContainer lifeCharts) throws NonExistentTokenException, SQLException, NoContentException;



    /**
     * @param userId                        identifier of a client
     * @param token                         doctor authentication
     * @throws NotAuthorizedException       when not authorized
     * @throws NonExistentTokenException    when token is not existent
     * @throws SQLException                 something went wrong with a database operation
     */
    LifeChartsContainer getLifeChartsFromUser(int userId, String token) throws NotAuthorizedException, NonExistentTokenException, SQLException;
}
