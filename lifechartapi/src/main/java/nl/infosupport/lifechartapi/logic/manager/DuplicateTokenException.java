package nl.infosupport.lifechartapi.logic.manager;

public class DuplicateTokenException extends Exception {

    public DuplicateTokenException(String message) {
        super(message);
    }
}
