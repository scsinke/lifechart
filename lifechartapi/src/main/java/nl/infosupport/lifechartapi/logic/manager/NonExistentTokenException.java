package nl.infosupport.lifechartapi.logic.manager;

public class NonExistentTokenException extends Exception {

    public NonExistentTokenException() {
    }

    public NonExistentTokenException(String message) {
        super(message);
    }
}
