package nl.infosupport.lifechartapi.logic;

import nl.infosupport.lifechartapi.DAO.IUserDAO;
import nl.infosupport.lifechartapi.logic.manager.DuplicateTokenException;
import nl.infosupport.lifechartapi.logic.manager.IllegalUserIdException;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;
import nl.infosupport.lifechartapi.logic.manager.TokenManager;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuthorizationLogic implements IAuthorizationLogic {

    @Inject
    private IUserDAO userDAO;

    private TokenManager tokenManager = TokenManager.getInstance();
    private static final Logger LOGGER = Logger.getLogger(AuthorizationLogic.class.getName());

    public String generateToken(int id) throws IllegalUserIdException {
        UUID uuid = UUID.randomUUID();
        String token = uuid.toString();

        try {
            tokenManager.addToken(id, token);
        } catch (DuplicateTokenException e) {
            LOGGER.log(Level.INFO, "Duplicate token generated", e);
            generateToken(id);
        }
        return token;
    }

    public void removeToken(String token) throws NonExistentTokenException {
        tokenManager.removeToken(token);
    }

    public void isTherapistAuthorizedForClient(int userId, int therapistId) throws NotAuthorizedException, SQLException {
        List<Integer> persons = userDAO.getPersonsByTherapistId(therapistId);
        if (!persons.contains(userId)) {
            throw new NotAuthorizedException("Therapist is not authorized for client");
        }
    }

    public Integer getUserIdByToken(String token) throws NonExistentTokenException {
        return tokenManager.getUserIdByToken(token);
    }
}
