package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.Queries;
import nl.infosupport.lifechartapi.POJO.Medicine;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class MedicineDAOTest {
    @Mock
    private Queries queries;
    @InjectMocks
    private MedicineDAO medicineDAO;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setup() {
        when(queries.getMedicinesByLifeChartIdQuery()).thenReturn("SELECT * FROM MEDICINE WHERE LIFECHARTID = ?");
        when(queries.addMedicinesToLifeChart()).thenReturn("INSERT INTO MEDICINE (LIFECHARTID, NAME, DOSEINMG, AMOUNTPERDAY) VALUES(?,?,?,?)");
    }

    @Test
    public void MedicineTest() throws SQLException {
//        setup
        int lifeChartId = 1;
        String medicineName = "Paracetamol";
        int dose = 200;
        int amount = 2;

        Medicine medicineInput = new Medicine();
        medicineInput.setName(medicineName);
        medicineInput.setDoseInMg(dose);
        medicineInput.setAmount(amount);

        List<Medicine> medicinesListInput = new ArrayList<>();
        medicinesListInput.add(medicineInput);

//        Test
        medicineDAO.addMedicines(lifeChartId, medicinesListInput);
        List<Medicine> medicineListOutput = medicineDAO.getMedicinesByLifeChartId(1);

        Medicine medicine = medicineListOutput.get(0);
        //Verify
        assertTrue(medicine.getName().equals(medicineName));
        assertTrue(medicine.getDoseInMg().equals(dose));
        assertTrue(medicine.getAmount().equals(amount));

    }

    @Test(expected = SQLException.class)
    public void failedToAddMedicineToDatabaseTest() throws SQLException {
//        setup
        int lifeChartId = -1;
        String medicineName = "Paracetamol";
        int dose = 200;
        int amount = 2;

        Medicine medicineInput = new Medicine();
        medicineInput.setName(medicineName);
        medicineInput.setDoseInMg(dose);
        medicineInput.setAmount(amount);

        List<Medicine> medicinesListInput = new ArrayList<>();
        medicinesListInput.add(medicineInput);

//        Test
        medicineDAO.addMedicines(lifeChartId, medicinesListInput);
    }
}
