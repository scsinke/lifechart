package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.Queries;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class UserDAOTest {

    @Mock
    private Queries queries;

    @InjectMocks
    private UserDAO userDAO;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setup() {
        when(queries.getAllClientsFromTherapist()).thenReturn("SELECT PERSONID FROM TREATMENT WHERE THERAPISTID = ?");
    }

    @Test
    public void getPersonsByTherapistIdShouldReturnOneClientWithIdOne() throws SQLException {
        //setup
        int therapistId = 1;

        //test
        List<Integer> clientIds = userDAO.getPersonsByTherapistId(therapistId);

        //verify
        assertTrue(clientIds.get(0).equals(1));
    }

    @Test
    public void getPersonsByTherapistIdShouldFailWhenTherapistDoesNotExist() throws SQLException {
        //setup
        int therapistId = -1;

        //test
        List<Integer> clientIds = userDAO.getPersonsByTherapistId(therapistId);

        //verify
        assertTrue(clientIds.isEmpty());
    }

    @Test
    public void getPersonsByTherapistIdShouldReturnMultipleClientIds() throws SQLException {
        //setup
        int therapistId = 2;

        //test
        List<Integer> clientIds = userDAO.getPersonsByTherapistId(therapistId);

        //verify
        assertTrue(clientIds.get(0).equals(1));
        assertTrue(clientIds.get(1).equals(2));
    }
}
