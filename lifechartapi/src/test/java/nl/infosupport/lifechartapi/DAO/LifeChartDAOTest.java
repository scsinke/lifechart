package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.Queries;
import nl.infosupport.lifechartapi.POJO.LifeChart;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.SQLException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class LifeChartDAOTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private Queries queries;
    @InjectMocks
    private LifeChartDAO lifeChartDAO;

    @Before
    public void setup() {
        when(queries.getLifeChartByIdQuery()).thenReturn("SELECT * FROM LIFECHART WHERE LIFECHARTID = ?");
        when(queries.insertLifeChartQuery()).thenReturn("INSERT INTO LIFECHART(WEIGHT, SLEEP, DESPITEMANIA, SEVERITYOFEPISODE, COMPLAINTS, MOODSWINGS, MENSTRUATED, DATE, MOODRATING, PERSONID, LONGTERMMOOD) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        when(queries.getAllLifeChartIdsByClientIdQuery()).thenReturn("SELECT LIFECHARTID FROM LIFECHART WHERE PERSONID = ?;");
    }

    @Test
    public void findLifeChart() throws SQLException {
        // Test
        LifeChart lifeChart = lifeChartDAO.getLifeChartById(1);

        // Verify
        assertTrue(lifeChart.getWeight().equals(80.6));
        assertTrue(lifeChart.getSleep().equals(6.5));
        assertTrue(lifeChart.getUnrestrainedMania().equals(true));
        assertTrue(lifeChart.getSeverityOfEpisode().equals("Episode"));
        assertTrue(lifeChart.getComplaints().equals("Complaints"));
        assertTrue(lifeChart.getMoodSwings().equals(2));
        assertTrue(lifeChart.getMenstruated().equals(true));
        assertTrue(lifeChart.getDate().equals("2012-01-11"));
        assertTrue(lifeChart.getMoodRating().equals(2));
        assertTrue(lifeChart.getPersonId() == 2);
    }

    @Test
    public void addLifeChart_ShouldInsertLifeChartInDb() throws SQLException {
        // Setup
        double weight = 80.0;
        double sleep = 7.5;
        boolean unrestrainedMania = true;
        String episode = "Gemiddeld";
        String complaints = "Geen klachten";
        int moodSwings = 2;
        boolean menstruated = true;
        String date = "2017-12-19";
        int moodRating = 4;

        LifeChart lifeChart = new LifeChart();
        lifeChart.setWeight(weight);
        lifeChart.setSleep(sleep);
        lifeChart.setUnrestrainedMania(unrestrainedMania);
        lifeChart.setSeverityOfEpisode(episode);
        lifeChart.setComplaints(complaints);
        lifeChart.setMoodSwings(moodSwings);
        lifeChart.setMenstruated(menstruated);
        lifeChart.setDate(date);
        lifeChart.setMoodRating(moodRating);

        // Test
        lifeChartDAO.addLifeChart(3, lifeChart);

        // Verify
        LifeChart retrievedLifeChart = lifeChartDAO.getLifeChartById(lifeChartDAO.getAllLifeChartIdsByClientId(3).get(0));
        assertThat(retrievedLifeChart.getWeight(), is(weight));
        assertThat(retrievedLifeChart.getSleep(), is(sleep));
        assertThat(retrievedLifeChart.getUnrestrainedMania(), is(unrestrainedMania));
        assertThat(retrievedLifeChart.getSeverityOfEpisode(), is(episode));
        assertThat(retrievedLifeChart.getComplaints(), is(complaints));
        assertThat(retrievedLifeChart.getMoodSwings(), is(moodSwings));
        assertThat(retrievedLifeChart.getMenstruated(), is(menstruated));
        assertThat(retrievedLifeChart.getDate(), is(date));
        assertThat(retrievedLifeChart.getMoodRating(), is(moodRating));
    }
}