package nl.infosupport.lifechartapi.DAO.util;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class QueriesTest {

    private Queries queries = new Queries();

    @Test
    public void getLifeChartByIdQuery() {
        String databaseQuery = "SELECT * FROM LIFECHART WHERE LIFECHARTID = ?;";
        String query = queries.getLifeChartByIdQuery();

        assertTrue(query.equals(databaseQuery));
    }

    @Test
    public void getAllLifeChartIdsByClientIdQuery() {
        String databaseQuery = "SELECT LIFECHARTID FROM LIFECHART WHERE PERSONID = ?;";
        String query = queries.getAllLifeChartIdsByClientIdQuery();

        assertTrue(query.equals(databaseQuery));
    }

    @Test
    public void getMedicinesByLifeChartIdQuery() {
        String databaseQuery = "SELECT * FROM MEDICINE WHERE LIFECHARTID = ?;";
        String query = queries.getMedicinesByLifeChartIdQuery();

        assertTrue(query.equals(databaseQuery));
    }

    @Test
    public void addMedicinesToLifeChart() {
        String databaseQuery = "INSERT INTO MEDICINE (LIFECHARTID, NAME, DOSEINMG, AMOUNTPERDAY) VALUES (?, ?, ?, ?);";
        String query = queries.addMedicinesToLifeChart();

        assertTrue(query.equals(databaseQuery));
    }

    @Test
    public void getLifeEventsByLifeChartIdQuery() {
        String databaseQuery = "SELECT * FROM LIFEEVENT WHERE LIFECHARTID = ?;";
        String query = queries.getLifeEventsByLifeChartIdQuery();

        assertTrue(query.equals(databaseQuery));

    }

    @Test
    public void insertLifeChartQuery() {
        String databaseQuery = "INSERT INTO LIFECHART (WEIGHT, SLEEP, DESPITEMANIA, SEVERITYOFEPISODE, COMPLAINTS, MOODSWINGS, MENSTRUATED, DATE, MOODRATING, PERSONID, LONGTERMMOOD) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        String query = queries.insertLifeChartQuery();

        assertTrue(query.equals(databaseQuery));
    }

    @Test
    public void insertLifeEventQuery() {
        String databaseQuery = "INSERT INTO LIFEEVENT (NAME, MOODINFLUENCE, LIFECHARTID) VALUES (?, ?, ?);";
        String query = queries.insertLifeEventQuery();

        assertTrue(query.equals(databaseQuery));
    }

    @Test
    public void getAllClientsFromTherapist() {
        String databaseQuery = "SELECT PERSONID FROM TREATMENT WHERE THERAPISTID = ?;";
        String query = queries.getAllClientsFromTherapist();

        assertTrue(query.equals(databaseQuery));
    }



//

//

//

//

//

//

}
