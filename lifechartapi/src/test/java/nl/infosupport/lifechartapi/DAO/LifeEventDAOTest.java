package nl.infosupport.lifechartapi.DAO;

import nl.infosupport.lifechartapi.DAO.util.Queries;
import nl.infosupport.lifechartapi.POJO.LifeEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class LifeEventDAOTest {
    @Mock
    private Queries queries;

    @InjectMocks
    private LifeEventDAO lifeEventDAO;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(queries.insertLifeEventQuery()).thenReturn("INSERT INTO LIFEEVENT(NAME, MOODINFLUENCE, LIFECHARTID) VALUES(?, ?, ?);");
        when(queries.getLifeEventsByLifeChartIdQuery()).thenReturn("SELECT * FROM LIFEEVENT WHERE LIFECHARTID = ?;");
    }

    @Test
    public void addLifeEvents_ShouldInsertRightAmountOfLifeEventsInDb() throws SQLException {
        // Setup
        int lifeChartId = 4;
        int nOfLifeEventToInsert = 10;
        List<LifeEvent> lifeEventList = new ArrayList<>();
        for (int i = 0; i < nOfLifeEventToInsert; i++) {
            LifeEvent lifeEvent = new LifeEvent();
            lifeEvent.setName("Verjaardag");
            lifeEvent.setMoodInfluence(3);
            lifeEventList.add(lifeEvent);
        }

        // Test
        lifeEventDAO.addLifeEvents(lifeChartId, lifeEventList);

        // Verify
        assertThat(lifeEventDAO.getLifeEventsByLifeChartId(lifeChartId).size(), is(nOfLifeEventToInsert));
    }

    @Test(expected = SQLException.class)
    public void addLifeEvents_ShouldThrowSQLExceptionWhenLifeChartDoesNotExist() throws SQLException {
        // Setup
        int lifeChartId = -1;
        List<LifeEvent> lifeEventList = new ArrayList<>();
        LifeEvent lifeEvent = new LifeEvent();
        lifeEvent.setName("name");
        lifeEvent.setMoodInfluence(0);
        lifeEventList.add(lifeEvent);

        // Test
        lifeEventDAO.addLifeEvents(lifeChartId, lifeEventList);
    }

    @Test
    public void getLifeEventsByLifeChartId_ShouldReturnAListWithASizeOfTwo() throws SQLException {
        // Test
        List<LifeEvent> lifeEventList = lifeEventDAO.getLifeEventsByLifeChartId(1);

        // Verify
        assertThat(lifeEventList.size(), is(2));
    }

    @Test
    public void getLifeEventsByLifeChartId_ShouldReturnLifeEventsEqualToTestData() throws SQLException {
        // Test
        List<LifeEvent> lifeEventList = lifeEventDAO.getLifeEventsByLifeChartId(1);

        // Verify
        assertThat(lifeEventList.get(0).getName(), is("Verjaardag"));
        assertThat(lifeEventList.get(0).getMoodInfluence(), is(4));
        assertThat(lifeEventList.get(1).getName(), is("Werken"));
        assertThat(lifeEventList.get(1).getMoodInfluence(), is(-3));
    }
}