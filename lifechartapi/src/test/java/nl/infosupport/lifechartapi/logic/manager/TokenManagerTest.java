package nl.infosupport.lifechartapi.logic.manager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class TokenManagerTest {
    @Mock
    private HashMap<String, Integer> tokenMap;
    @InjectMocks
    private TokenManager tokenManager;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addToken_ShouldAddToken() throws NonExistentTokenException, IllegalUserIdException, DuplicateTokenException {
        // Setup
        int id = 1;
        String token = "1234-1234-1234";
        when(tokenMap.containsKey(token)).thenReturn(false);

        // Test
        tokenManager.addToken(id, token);

        // Verify
        verify(tokenMap).put(token, id);
    }

    @Test(expected = DuplicateTokenException.class)
    public void addToken_ShouldThrowDuplicateTokenExceptionWhenAddingTwoIdenticalTokens() throws DuplicateTokenException, IllegalUserIdException {
        // Setup
        int id = 1;
        String token = "1234-1234-1234";
        when(tokenMap.containsKey(token)).thenReturn(true);

        // Test
        tokenManager.addToken(id, token);
    }

    @Test(expected = IllegalUserIdException.class)
    public void addToken_ShouldThrowIllegalArgumentExceptionWhenIdIsSmallerThanOne() throws DuplicateTokenException, IllegalUserIdException {
        // Setup
        int id = 0;
        String token = "1234-1234-1234";

        // Test
        for (int i = id; i > -100; i--) {
            tokenManager.addToken(id, token);
        }
    }

    @Test
    public void removeToken_ShouldRemoveToken() throws NonExistentTokenException {
        // Setup
        String token = "1234-1234-1234";
        when(tokenMap.containsKey(token)).thenReturn(true);

        // Test
        tokenManager.removeToken(token);

        // Verify
        verify(tokenMap).remove(token);
    }

    @Test(expected = NonExistentTokenException.class)
    public void removeToken_ShouldThrowNonExistentTokenExceptionWhenTokenDoesNotExist() throws NonExistentTokenException {
        // Setup
        String token = "nonexistingtoken";

        // Test
        tokenManager.removeToken(token);
    }

    @Test
    public void getIdByToken_ShouldReturnIdLinkedToToken() throws DuplicateTokenException, NonExistentTokenException, IllegalUserIdException {
        // Setup
        int id = 1;
        String token = "1234-1234-1234";
        when(tokenMap.containsKey(token)).thenReturn(true);
        doReturn(id).when(tokenMap).get(token);

        // Test
        int returnedId = tokenManager.getUserIdByToken(token);

        // Verify
        assertThat(returnedId, is(id));
    }

    @Test(expected = NonExistentTokenException.class)
    public void getIdByToken_ShouldThrowNonExistentTokenExceptionWhenGettingIdByNonExistingToken() throws NonExistentTokenException {
        // Setup
        String token = "nonexistingtoken";

        // Test
        int id = tokenManager.getUserIdByToken(token);
    }
}