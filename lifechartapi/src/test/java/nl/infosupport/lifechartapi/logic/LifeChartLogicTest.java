package nl.infosupport.lifechartapi.logic;

import nl.infosupport.lifechartapi.DAO.ILifeChartDAO;
import nl.infosupport.lifechartapi.DAO.ILifeEventDAO;
import nl.infosupport.lifechartapi.DAO.IMedicineDAO;
import nl.infosupport.lifechartapi.POJO.LifeChart;
import nl.infosupport.lifechartapi.POJO.LifeChartsContainer;
import nl.infosupport.lifechartapi.POJO.LifeEvent;
import nl.infosupport.lifechartapi.POJO.Medicine;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.NoContentException;
import java.sql.SQLException;
import javax.ws.rs.NotAuthorizedException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

public class LifeChartLogicTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    private String token = "1234-1234-1234";
    private Integer lifeChartId = 1;
    private Integer userId = 1;
    private Integer therapistId = 2;

    private LifeChartsContainer lifeChartsContainer;
    private LifeChart lifeChart;
    private List<Medicine> medicines;
    private List<LifeEvent> lifeEvents;

    @Mock
    private ILifeChartDAO lifeChartDAOMock;
    @Mock
    private IMedicineDAO medicineDAOMock;
    @Mock
    private ILifeEventDAO lifeEventDAOMock;
    @Mock
    private IAuthorizationLogic authorizationLogicMock;
    @Mock
    private LifeChartsContainer lifeChartsContainerMock;

    @InjectMocks
    private LifeChartLogic lifeChartLogic;
    
    @Mock
    private LifeChart mockedLifeChart;


    @Before
    public void setup() throws NonExistentTokenException, SQLException {
        containerSetup();

        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(1);
        when(lifeChartDAOMock.addLifeChart(1, lifeChart)).thenReturn(lifeChartId);
    }

    private void containerSetup() {
        mockMedicines();
        mockLifeEvents();
        mockLifeChart();
        mockContainer();
    }

    private void mockLifeEvents() {
        LifeEvent lifeEvent = new LifeEvent();
        lifeEvent.setName("Birthday");
        lifeEvent.setMoodInfluence(4);

        lifeEvents = new ArrayList<>();
        lifeEvents.add(lifeEvent);
    }

    private void mockMedicines() {
        Medicine medicine = new Medicine();
        medicine.setName("Paracetamol");
        medicine.setDoseInMg(200);
        medicine.setAmount(2);

        medicines = new ArrayList<>();
        medicines.add(medicine);
    }

    private void mockLifeChart() {
        lifeChart = new LifeChart();
        lifeChart.setDate("yyyy-MM-dd");
        lifeChart.setSleep(8.0);
        lifeChart.setWeight(80.0);
        lifeChart.setMood("Depressive");
        lifeChart.setMoodRating(-4);
        lifeChart.setMoodSwings(2);
        lifeChart.setComplaints("No complaints");
        lifeChart.setSeverityOfEpisode("No episode");
        lifeChart.setMedicines(medicines);
        lifeChart.setLifeEvents(lifeEvents);
    }

    private void mockContainer() {
        List<LifeChart> lifeCharts = new ArrayList<>();
        lifeCharts.add(lifeChart);

        lifeChartsContainer = new LifeChartsContainer();
        lifeChartsContainer.setLifeCharts(lifeCharts);
    }

    @Test
    public void LifeChartLogicShouldCallGetUserIdByToken() throws NonExistentTokenException, NoContentException, SQLException {
        lifeChartLogic.addLifeCharts(token, lifeChartsContainer);

        verify(authorizationLogicMock).getUserIdByToken(token);
    }

    @Test(expected = NonExistentTokenException.class)
    public void LifeChartLogicGetUserIdByTokenShouldThrowNonExistentTokenException() throws NonExistentTokenException, NoContentException, SQLException {

        when(authorizationLogicMock.getUserIdByToken(token)).thenThrow(new NonExistentTokenException());

        lifeChartLogic.addLifeCharts(token, lifeChartsContainer);
    }

    @Test
    public void LifeChartLogicShouldCallAddLifeChartOnDAO() throws NonExistentTokenException, NoContentException, SQLException {
        int userId = 1;

        lifeChartLogic.addLifeCharts(token, lifeChartsContainer);

        verify(lifeChartDAOMock).addLifeChart(userId, lifeChart);
    }

    @Test
    public void LifeChartLogicShouldCallAddMedicinesOnDAO() throws NonExistentTokenException, NoContentException, SQLException {

        lifeChartLogic.addLifeCharts(token, lifeChartsContainer);

        verify(medicineDAOMock).addMedicines(lifeChartId, medicines);
    }

    @Test
    public void LifeChartLogicShouldCallAddLifeEventsOnDAO() throws NonExistentTokenException, NoContentException, SQLException {
        lifeChartLogic.addLifeCharts(token, lifeChartsContainer);

        verify(lifeEventDAOMock).addLifeEvents(lifeChartId, lifeEvents);
    }

    @Test
    public void addLifeCharts_ShouldCallRemoveTokenWhenLifeChartExist() throws NonExistentTokenException, SQLException, NoContentException {
        // Test
        lifeChartLogic.addLifeCharts(token, lifeChartsContainer);

        // Verify
        verify(authorizationLogicMock).removeToken(token);
    }

    @Test(expected = NoContentException.class)
    public void LifeChartLogicThrowsListIsEmptyException() throws NonExistentTokenException, NoContentException, SQLException {
        lifeChartLogic.addLifeCharts(token, lifeChartsContainerMock);
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallGetTherapistIdByToken() throws NonExistentTokenException, SQLException {
        // Setup
        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);
        // Test
        lifeChartLogic.getLifeChartsFromUser(userId,token);
        // Verify
        verify(authorizationLogicMock).getUserIdByToken(token);
    }

    @Test(expected = NonExistentTokenException.class)
    public void getLifeChartsFromUser_ShouldRethrowNonExistentTokenException() throws NonExistentTokenException, SQLException {
        // Setup
        when(authorizationLogicMock.getUserIdByToken(token)).thenThrow(new NonExistentTokenException());
        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallIsTherapistAuthorizedForClientHappyFlow() throws NonExistentTokenException, SQLException {
        // Setup
        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);
        // Test
        lifeChartLogic.getLifeChartsFromUser(userId,token);
        // Verify
        verify(authorizationLogicMock).isTherapistAuthorizedForClient(userId,therapistId);
    }

    @Test (expected = NonExistentTokenException.class)
    public void getLifeChartsFromUser_ShouldThrowNonExistendTokenExceptionWhenTherapistTokenIsNotValid() throws NonExistentTokenException, SQLException {
        // Setup
        when(authorizationLogicMock.getUserIdByToken(token)).thenThrow(new NonExistentTokenException());
        // Test
        lifeChartLogic.getLifeChartsFromUser(userId,token);
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallGetAllLifeChartIdByClientIdHappyFlow() throws NonExistentTokenException, SQLException {
        // Setup
        int clientId = 1;

        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);

        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
        // Verify
        verify(lifeChartDAOMock).getAllLifeChartIdsByClientId(clientId);
    }

    @Test (expected = NonExistentTokenException.class)
    public void getLifeChartsFromUser_ShouldThrowNonExistingTokenExceptionWhenTherapistTokenIsNotValid() throws NonExistentTokenException, SQLException {
        // Setup
        int clientId =1;
        when(authorizationLogicMock.getUserIdByToken(token)).thenThrow(new NonExistentTokenException());
        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
        // Verify
        verify(lifeChartDAOMock, never()).getAllLifeChartIdsByClientId(clientId);
    }

    @Test(expected = NotAuthorizedException.class)
    public void getLifeChartsFromUser_ShouldNotCallGetAllLifeChartIdByClientIdWhenTherapistIsNotAuthorized() throws NonExistentTokenException, SQLException {
        // Setup
        int clientId =1;
        when(authorizationLogicMock.getUserIdByToken(token)).thenThrow(NotAuthorizedException.class);
        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
        // Verify
        verify(lifeChartDAOMock, never()).getAllLifeChartIdsByClientId(clientId);
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallGetLifeChartByIdWhenLifeChartIdsBiggerThanZero() throws NonExistentTokenException, SQLException {
        // Setup
        List<Integer> lifeChartIds = new ArrayList<>();
        int lifeChartId = 1;
        int clientId =1;

        lifeChartIds.add(lifeChartId);

        when(lifeChartDAOMock.getAllLifeChartIdsByClientId(clientId)).thenReturn(lifeChartIds);
        when(lifeChartDAOMock.getLifeChartById(lifeChartId)).thenReturn(mockedLifeChart);
        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);

        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
        // Verify
        verify(lifeChartDAOMock).getLifeChartById(lifeChartId);
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallGetLifeChartByIdMultipleTimesRelativeToTheAmountOfLifeChartIds() throws NonExistentTokenException, SQLException {
        // Setup
        List<Integer> lifeChartIds = new ArrayList<>();

        int lifeChartId = 1;
        int numberOfLifeChartIds = 2;
        int clientId =1;

        for (int i = 0; i < numberOfLifeChartIds; i++) {
            lifeChartIds.add(i);
        }

        when(lifeChartDAOMock.getAllLifeChartIdsByClientId(clientId)).thenReturn(lifeChartIds);
        when(lifeChartDAOMock.getLifeChartById(0)).thenReturn(mockedLifeChart);
        when(lifeChartDAOMock.getLifeChartById(lifeChartId)).thenReturn(mockedLifeChart);
        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);

        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
        // Verify
        verify(lifeChartDAOMock, times(numberOfLifeChartIds)).getLifeChartById(anyInt());
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallGetMedicineByLifeChartIdWhenLifeChartIdsBiggerThanZero() throws NonExistentTokenException, SQLException {
        // Setup
        List<Integer> lifeChartIds = new ArrayList<>();

        int lifeChartId = 1;
        int clientId =1;
        lifeChartIds.add(lifeChartId);

        when(lifeChartDAOMock.getAllLifeChartIdsByClientId(clientId)).thenReturn(lifeChartIds);
        when(lifeChartDAOMock.getLifeChartById(lifeChartId)).thenReturn(mockedLifeChart);
        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);

        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);
        // Verify
        verify(medicineDAOMock).getMedicinesByLifeChartId(lifeChartId);
    }

    @Test
    public void getLifeChartsFromUser_ShouldCallGetLifeEventsByLifeChartIdWhenLifeChartIdsBiggerThanZero() throws NonExistentTokenException, SQLException {
        // Setup
        List<Integer> lifeChartIds = new ArrayList<>();

        int lifeChartId = 1;
        int clientId =1;
        lifeChartIds.add(lifeChartId);

        when(lifeChartDAOMock.getAllLifeChartIdsByClientId(clientId)).thenReturn(lifeChartIds);
        when(lifeChartDAOMock.getLifeChartById(lifeChartId)).thenReturn(mockedLifeChart);
        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);

        // Test
        lifeChartLogic.getLifeChartsFromUser(userId, token);

        // Verify
        verify(lifeEventDAOMock).getLifeEventsByLifeChartId(lifeChartId);
    }

    @Test
    public void TestGetLifeChartsFromUserMedicineDAOCalled() throws SQLException, NonExistentTokenException {
        //Setup
        int clientId = 1;
        List<Integer> lifechartIdList = new ArrayList<>();
        lifechartIdList.add(1);

        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);
        when(lifeChartDAOMock.getAllLifeChartIdsByClientId(clientId)).thenReturn(lifechartIdList);
        when(lifeChartDAOMock.getLifeChartById(lifeChartId)).thenReturn(mockedLifeChart);
        when(medicineDAOMock.getMedicinesByLifeChartId(lifeChartId)).thenReturn(null);
        when(lifeEventDAOMock.getLifeEventsByLifeChartId(lifeChartId)).thenReturn(null);

        //Test
        lifeChartLogic.getLifeChartsFromUser(clientId, token);

        //Verify
        verify(medicineDAOMock).getMedicinesByLifeChartId(lifeChartId);
    }

    @Test
    public void TestGetLifeChartsFromUserLifeEventDAOCalled() throws SQLException, NonExistentTokenException {
        //Setup
        int clientId = 1;
        List<Integer> lifechartIdList = new ArrayList<>();
        lifechartIdList.add(1);

        when(authorizationLogicMock.getUserIdByToken(token)).thenReturn(therapistId);
        when(lifeChartDAOMock.getAllLifeChartIdsByClientId(clientId)).thenReturn(lifechartIdList);
        when(lifeChartDAOMock.getLifeChartById(lifeChartId)).thenReturn(mockedLifeChart);
        when(medicineDAOMock.getMedicinesByLifeChartId(lifeChartId)).thenReturn(null);
        when(lifeEventDAOMock.getLifeEventsByLifeChartId(lifeChartId)).thenReturn(null);


        //Test
        lifeChartLogic.getLifeChartsFromUser(clientId, token);

        //Verify
        verify(lifeEventDAOMock).getLifeEventsByLifeChartId(lifeChartId);
    }

}
