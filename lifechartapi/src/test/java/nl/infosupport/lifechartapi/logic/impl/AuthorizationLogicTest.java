package nl.infosupport.lifechartapi.logic.impl;

import nl.infosupport.lifechartapi.logic.AuthorizationLogic;
import nl.infosupport.lifechartapi.DAO.IUserDAO;
import nl.infosupport.lifechartapi.logic.manager.DuplicateTokenException;
import nl.infosupport.lifechartapi.logic.manager.IllegalUserIdException;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;
import nl.infosupport.lifechartapi.logic.manager.TokenManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.NotAuthorizedException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class AuthorizationLogicTest {
    @Mock
    private TokenManager tokenManager;
    @Mock
    private IUserDAO userDAO;
    @InjectMocks
    private AuthorizationLogic authorizationLogic;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void generateToken_ShouldReturn36CharString() throws IllegalUserIdException {
        // Setup
        int id = 1;

        // Test
        String token = authorizationLogic.generateToken(id);

        // Verify
        assertThat(token.length(), is(36));
    }

    @Test(expected = IllegalUserIdException.class)
    public void generateToken_ShouldThrowIllegalUserIdExceptionWhenIdIsSmallerThanOne() throws IllegalUserIdException, DuplicateTokenException {
        // Setup
        int id = 0;
        doThrow(new IllegalUserIdException()).when(tokenManager).addToken(eq(id), anyString());

        // Test
        authorizationLogic.generateToken(id);
    }

    @Test
    public void removeToken_ShouldCallRemoveToken() throws NonExistentTokenException {
        // Setup
        String token = "1234-1234-1234";

        // Test
        authorizationLogic.removeToken(token);

        // Verify
        verify(tokenManager).removeToken(token);
    }

    @Test(expected = NonExistentTokenException.class)
    public void removeToken_ShouldThrowNonExistentTokenExceptionWhenTokenDoesNotExist() throws NonExistentTokenException {
        // Setup
        String token = "nonexistingtoken";
        doThrow(new NonExistentTokenException()).when(tokenManager).removeToken(token);

        // Test
        authorizationLogic.removeToken(token);
    }

    @Test
    public void getUserIdByToken_ShouldReturnUserId() throws NonExistentTokenException {
        // Setup
        int id = 1;
        String token = "1234-1234-1234";
        when(tokenManager.getUserIdByToken(token)).thenReturn(id);

        // Test
        int returnedId = authorizationLogic.getUserIdByToken(token);

        // Verify
        assertThat(returnedId, is(id));
    }

    @Test(expected = NonExistentTokenException.class)
    public void getUserIdByToken_ShouldThrowNonExistentTokenExceptionWhenGettingIdByNonExistingToken() throws NonExistentTokenException {
        // Setup
        String token = "nonexistingtoken";
        when(tokenManager.getUserIdByToken(token)).thenThrow(new NonExistentTokenException());

        // Test
        authorizationLogic.getUserIdByToken(token);
    }

    @Test
    public void getTherapistIdByToken_shouldCallTokenManager() throws NonExistentTokenException {
        // Setup
        String token = "1234-1234-1234";

        // Test
        authorizationLogic.getUserIdByToken(token);

        // Verify
        verify(tokenManager).getUserIdByToken(token);
    }

    @Test
    public void getPersonsByTherapistId_shouldCallUserDAO() throws SQLException {
        // Setup
        int userId = 1;
        int therapistId=1;

        List<Integer> persons = new ArrayList<>();
        persons.add(userId);
        // Test
        when(userDAO.getPersonsByTherapistId(therapistId)).thenReturn(persons);
        authorizationLogic.isTherapistAuthorizedForClient(userId,therapistId);
        // Verify
        verify(userDAO).getPersonsByTherapistId(therapistId);
    }

    @Test(expected = NotAuthorizedException.class)
    public void getPersonsByTherapistId_InvalidShouldThrowNotAuthorizedException() throws SQLException {
        // Setup
        int userId =1;
        int therapistId = 1;
        List<Integer> persons = new ArrayList<>();
        persons.add(2);
        when(userDAO.getPersonsByTherapistId(therapistId)).thenReturn(persons);
        // Test
        authorizationLogic.isTherapistAuthorizedForClient(userId,therapistId);
        // Verify

    }

    @Test
    public void getPersonsByTherapistId_validShouldBeHappy() throws SQLException {
        // Setup
        int userId = 1;
        int therapistId =1;
        List<Integer> persons = new ArrayList<>();
        persons.add(1);
        when(userDAO.getPersonsByTherapistId(therapistId)).thenReturn(persons);
        // Test
        authorizationLogic.isTherapistAuthorizedForClient(userId,therapistId);
        // Verify

    }
}