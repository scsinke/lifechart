package nl.infosupport.lifechartapi.POJO;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LifeEventTest {
    private LifeEvent lifeEvent;

    @Before
    public void setUp() {
        lifeEvent = new LifeEvent();
    }

    @Test
    public void Name() {
        String test = "Peter";
        lifeEvent.setName(test);

        assertEquals(test, lifeEvent.getName());
    }

    @Test
    public void MoodInfluence() {
        Integer test = 2;
        lifeEvent.setMoodInfluence(test);

        assertEquals(test, lifeEvent.getMoodInfluence());
    }
}