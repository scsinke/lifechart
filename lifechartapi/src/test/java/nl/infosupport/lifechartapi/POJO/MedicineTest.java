package nl.infosupport.lifechartapi.POJO;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MedicineTest {
    private Medicine medicine;

    @Before
    public void setUp() {
        medicine = new Medicine();
    }

    @Test
    public void Name() {
        String test = "Paracetamol";
        medicine.setName(test);

        assertEquals(test, medicine.getName());
    }

    @Test
    public void Dose() {
        Integer test = 200;
        medicine.setDoseInMg(test);

        assertEquals(test, medicine.getDoseInMg());
    }

    @Test
    public void Amount() {
        Integer test = 2;
        medicine.setAmount(test);

        assertEquals(test, medicine.getAmount());
    }
}