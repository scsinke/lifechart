package nl.infosupport.lifechartapi.POJO;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LifeChartTest {
    LifeChart lifeChart;

    @Before
    public void setUp() {
        lifeChart = new LifeChart();
    }

    @Test
    public void Mood() {
        String test = "Test";
        lifeChart.setMood(test);

        assertEquals(test, lifeChart.getMood());
    }

    @Test
    public void ServerityOfEpisode() {
        String test = "Test";
        lifeChart.setSeverityOfEpisode(test);

        assertEquals(test, lifeChart.getSeverityOfEpisode());
    }

    @Test
    public void Complaints() {
        String test = "Test";
        lifeChart.setComplaints(test);

        assertEquals(test, lifeChart.getComplaints());
    }

    @Test
    public void MoodRating() {
        Integer test = 3;
        lifeChart.setMoodRating(test);

        assertEquals(test, lifeChart.getMoodRating());
    }

    @Test
    public void MoodSwings() {
        Integer test = 2;
        lifeChart.setMoodSwings(test);

        assertEquals(test, lifeChart.getMoodSwings());
    }

    @Test
    public void Weight() {
        Double test = 80.0;
        lifeChart.setWeight(test);

        assertEquals(test, lifeChart.getWeight());
    }

    @Test
    public void Sleep() {
        Double test = 8.0;
        lifeChart.setSleep(test);

        assertEquals(test, lifeChart.getSleep());
    }

    @Test
    public void Menstruated() {
        Boolean test = true;
        lifeChart.setMenstruated(test);

        assertEquals(test, lifeChart.getMenstruated());
    }

    @Test
    public void UnrestrainedMania() {
        Boolean test = false;
        lifeChart.setUnrestrainedMania(test);

        assertEquals(test, lifeChart.getUnrestrainedMania());
    }

    @Test
    public void Date() {
        String test = "yyyy-MM-dd";
        lifeChart.setDate(test);

        assertEquals(test, lifeChart.getDate());
    }

    @Test
    public void Medicines() {
        String test = "Paracetamol";
        List<Medicine> medicines = new ArrayList<>();
        Medicine medicine = new Medicine();
        medicine.setName(test);
        medicines.add(medicine);

        lifeChart.setMedicines(medicines);

        assertEquals(test, lifeChart.getMedicines().get(0).getName());
    }

    @Test
    public void LifeEvents() {
        String test = "Birthday";
        List<LifeEvent> lifeEvents = new ArrayList<>();

        LifeEvent lifeEvent = new LifeEvent();
        lifeEvent.setName(test);
        lifeEvents.add(lifeEvent);

        assertEquals(test, lifeEvents.get(0).getName());
    }
}