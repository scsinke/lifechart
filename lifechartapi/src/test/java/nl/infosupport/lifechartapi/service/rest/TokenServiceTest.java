package nl.infosupport.lifechartapi.service.rest;

import nl.infosupport.lifechartapi.logic.IAuthorizationLogic;
import nl.infosupport.lifechartapi.logic.manager.IllegalUserIdException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.valid4j.matchers.http.HttpResponseMatchers.hasStatus;

public class TokenServiceTest {
    @Mock
    private IAuthorizationLogic authorizationLogic;
    @InjectMocks
    private TokenService tokenService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void requestToken_ShouldReturnStatusOKWhenValidId() throws IllegalUserIdException {
        // Setup
        int id = 1;
        String token = "1234-1234-1234";
        when(authorizationLogic.generateToken(id)).thenReturn(token);

        // Test
        Response response = tokenService.requestToken(id);

        // Verify
        assertThat(response, hasStatus(Response.Status.OK));
    }

    @Test
    public void requestToken_ShouldHaveBadRequestStatusWhenIdIsZero() throws IllegalUserIdException {
        // Setup
        int id = 0;
        when(authorizationLogic.generateToken(id)).thenThrow(new IllegalUserIdException("Value of id cannot be zero"));

        // Test
        Response response = tokenService.requestToken(id);

        // Verify
        assertThat(response, hasStatus(Response.Status.BAD_REQUEST));
    }

    @Test
    public void requestToken_ShouldHaveBadRequestStatusWhenIdNegative() throws IllegalUserIdException {
        // Setup
        int id = -1;
        when(authorizationLogic.generateToken(id)).thenThrow(new IllegalUserIdException("Value of id cannot be negative"));

        // Test
        Response response = tokenService.requestToken(id);

        // Verify
        assertThat(response, hasStatus(Response.Status.BAD_REQUEST));
    }
}