package nl.infosupport.lifechartapi.service.rest;

import nl.infosupport.lifechartapi.POJO.LifeChart;
import nl.infosupport.lifechartapi.POJO.LifeChartsContainer;
import nl.infosupport.lifechartapi.logic.ILifeChartLogic;
import nl.infosupport.lifechartapi.logic.manager.NonExistentTokenException;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.NoContentException;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.valid4j.matchers.http.HttpResponseMatchers.hasStatus;

/**
 * Unit test for LifeChartController.
 */
public class LifeChartServiceTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private ILifeChartLogic lifeChartLogic;
    @InjectMocks
    private LifeChartService lifeChartService;

    private String token = "1234-1234-1234";
    private LifeChartsContainer lifeChartsContainer = new LifeChartsContainer();

    @Test
    public void TestLifeChartControllerAddLifeChartVerifyCall() {

        Response response = lifeChartService.addLifeCharts(token, lifeChartsContainer);

        assertThat(response, hasStatus(Response.Status.OK));
    }

    @Test
    public void TestLifeChartControllerAddLifeChartUNAUTORIZED() throws NonExistentTokenException, NoContentException, SQLException {

        doThrow(new NonExistentTokenException()).when(lifeChartLogic).addLifeCharts(token, lifeChartsContainer);

        Response response = lifeChartService.addLifeCharts(token, lifeChartsContainer);

        assertThat(response, hasStatus(Response.Status.UNAUTHORIZED));
    }

    @Test
    public void TestLifeChartControllerAddLifeChartEmptyListNO_CONTENT() throws NonExistentTokenException, NoContentException, SQLException {

        doThrow(new NoContentException("No List")).when(lifeChartLogic).addLifeCharts(token, lifeChartsContainer);

        Response response = lifeChartService.addLifeCharts(token, lifeChartsContainer);

        assertThat(response, hasStatus(Response.Status.NO_CONTENT));
    }

    @Test
    public void TestLifeChartControllerAddLifeChartSQLException() throws NonExistentTokenException, SQLException, NoContentException {

        doThrow(new SQLException()).when(lifeChartLogic).addLifeCharts(token, lifeChartsContainer);

        Response response = lifeChartService.addLifeCharts(token, lifeChartsContainer);

        assertThat(response, hasStatus(Response.Status.BAD_REQUEST));
    }


    @Test
    public void TestGetLifeChartsFromUserCalledLogic() throws NonExistentTokenException, SQLException {
        //setup
        int userId = 1;

        //test
        lifeChartService.getLifeChartsFromUser(token, userId);

        //verify
        verify(lifeChartLogic).getLifeChartsFromUser(userId,token);
    }

    @Test
    public void TestValidation() throws NonExistentTokenException {
        //setup
        int userId =1;
        //test
        Response response = lifeChartService.getLifeChartsFromUser(token, userId);
        //verify
        assertThat(response, hasStatus(Response.Status.OK));
    }

    @Test
    public void testInvalideAuthorazation() throws NonExistentTokenException, SQLException {
        //setup
        int userId = 1;
        doThrow(new NotAuthorizedException("user is not authorized to perform this action")).when(lifeChartLogic).getLifeChartsFromUser(userId,token);
        //test
        Response response = lifeChartService.getLifeChartsFromUser(token, userId);
        //verify
        assertThat(response,hasStatus(Response.Status.UNAUTHORIZED));
    }

    @Test
    public void TestGetLifeChartsFromUserSQLException() throws NonExistentTokenException, SQLException {
        //setup
        int userId = 1;
        doThrow(new SQLException()).when(lifeChartLogic).getLifeChartsFromUser(userId,token);
        //test
        Response response = lifeChartService.getLifeChartsFromUser(token, userId);
        //verify
        assertThat(response,hasStatus(Response.Status.BAD_REQUEST));
    }

    @Test
    public void TestGetLifeChartsFromUserNonExistentTokenException() throws NonExistentTokenException, SQLException {
        //setup
        int userId = 1;
        doThrow(new NonExistentTokenException()).when(lifeChartLogic).getLifeChartsFromUser(userId,token);
        //test
        Response response = lifeChartService.getLifeChartsFromUser(token, userId);
        //verify
        assertThat(response,hasStatus(Response.Status.BAD_REQUEST));
    }
}
