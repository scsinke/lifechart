/*==============================================================*/
/* Table: PERSON */
/*==============================================================*/
CREATE TABLE PERSON
(
  SEX      ENUM ('MALE', 'FEMALE') NOT NULL,
  NAME     VARCHAR(50),
  PERSONID INT                     NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (PERSONID)
);

/*==============================================================*/
/* Table: LIFECHART */
/*==============================================================*/
CREATE TABLE LIFECHART
(
  WEIGHT            FLOAT,
  SLEEP             FLOAT NOT NULL,
  DESPITEMANIA      BOOL,
  SEVERITYOFEPISODE VARCHAR(1000),
  COMPLAINTS        VARCHAR(1000),
  MOODSWINGS        INT,
  MENSTRUATED       BOOL,
  DATE              DATE  NOT NULL,
  MOODRATING        INT,
  LIFECHARTID       INT   NOT NULL AUTO_INCREMENT,
  PERSONID          INT   NOT NULL,
  LONGTERMMOOD      ENUM ('NONE', 'SERIOUS', 'MODERATELY_HIGH', 'MODERATELY_LOW', 'LIGHT', 'STABLE', 'LIGHT_DEPRESSIVE', 'MODERATELY_LOW_DEPRESSIVE', 'MODERATELTY_HIGH_DEPRESSIVE', 'SERIOUSLY_DEPRESSIVE', 'DEPRESSIVE'),
  PRIMARY KEY (LIFECHARTID),
  UNIQUE (PERSONID, DATE), /*a person can only insert 1 LifeChart a day*/
  FOREIGN KEY (PERSONID) REFERENCES PERSON (PERSONID)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

/*==============================================================*/
/* Table: LIFEEVENT */
/*==============================================================*/
CREATE TABLE LIFEEVENT
(
  NAME          VARCHAR(50) NOT NULL,
  MOODINFLUENCE INT         NOT NULL,
  LIFEEVENTID   INT         NOT NULL AUTO_INCREMENT,
  LIFECHARTID   INT         NOT NULL,
  PRIMARY KEY (LIFEEVENTID),
  FOREIGN KEY (LIFECHARTID) REFERENCES LIFECHART (LIFECHARTID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

/*==============================================================*/
/* Table: MEDICINE */
/*==============================================================*/
CREATE TABLE MEDICINE
(
  NAME         VARCHAR(50) NOT NULL,
  DOSEINMG     INT         NOT NULL,
  AMOUNTPERDAY INT,
  MEDICINEID   INT         NOT NULL AUTO_INCREMENT,
  LIFECHARTID  INT         NOT NULL ,
  PRIMARY KEY (MEDICINEID),
  FOREIGN KEY (LIFECHARTID) REFERENCES LIFECHART (LIFECHARTID)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

/*==============================================================*/
/* Table: THERAPIST */
/*==============================================================*/
CREATE TABLE THERAPIST
(
  THERAPISTID INT         NOT NULL  AUTO_INCREMENT,
  NAME        VARCHAR(50) NOT NULL,
  PRIMARY KEY (THERAPISTID)
);

/*==============================================================*/
/* Table: TREATMENT */
/*==============================================================*/
CREATE TABLE TREATMENT
(
  PERSONID    INT         NOT NULL,
  THERAPISTID INT         NOT NULL,
  FOREIGN KEY (PERSONID) REFERENCES PERSON (PERSONID)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  FOREIGN KEY (THERAPISTID) REFERENCES THERAPIST (THERAPISTID)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);