#Foundation LifeChart frontend

##Installation

To use this frontend, your computers needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)

###Setup

Install the Foundation CLI with this command:

```bash
npm install foundation-cli --global
```

If you get a EACCESS warning:

```bash
sudo npm install foundation-cli --global
```

Download the frontend with git:
```bash
git clone {repository}
```

Then open the folder in your command line, and install the needed dependencies:

```bash
cd projectname
npm install
bower install
```

Finally, run `foundation watch` to run the sass compiler, It will re-run every time you save a Sass file.